
function login(){

  window.location.href = base_url+"welcome/login";
}


//Validação simples
 // $("#registerUsuario").validate();

 $("#registerUsuario").validate({
    rules: {
      nome: "required",
      telefone: "required"
    }
});


 $("#recuperarSenha").validate({
    rules: {
      email: "required",
    }
});


$("#recuperarSenhaNova").validate({
    rules: {
      password: "required",
      password2: {
      equalTo: "#password"
      }
    },

    messages: {
        password2: {
            equalTo: "As senhas inseridas não conferem, tente novamente."
        }
    }
})