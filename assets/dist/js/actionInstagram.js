$("#formInstaCut").validate({
    rules: {
      servicoinsta: "required",
      idinsta: "required",
      quantidade: "required",
    }

});


$('#servicoinsta').change(function(){
   
   var servico = $('#servicoinsta').val();

   $.ajax({
	    method: "POST",
	    url: base_url+"instagram/selecionarServicoTable/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#instatable').html(response);
    
    });


	$.ajax({
	    method: "POST",
	    url: base_url+"instagram/selecionarServicoMini/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#instaMini').html(response);
	    $('#quantidade').val('');
	    $('#valorInsta').val('');
    
    });
   
});


$('#quantidade').blur(function(){
   
   var quantidade = $('#quantidade').val();
   var valor =  parseFloat($('.spanValor').html().replace(',','.'));
   
   calculo = valor/1000;
   calculo = quantidade*calculo;

   if (isNaN(calculo)) {
   	$('#valorInsta').val('0,00');
   }else{
   	$('#valorInsta').val(parseFloat(calculo.toFixed(2)).toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2
    }));
   }
   

});
