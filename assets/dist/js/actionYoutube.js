$("#formTubeCut").validate({
    rules: {
      servicotube: "required",
      idtube: "required",
      quantidade: "required",
    }

});


$('#servicotube').change(function(){
   
   var servico = $('#servicotube').val();

   $.ajax({
	    method: "POST",
	    url: base_url+"youtube/selecionarServicoTable/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#tubetable').html(response);
    
    });


	$.ajax({
	    method: "POST",
	    url: base_url+"youtube/selecionarServicoMini/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#tubeMini').html(response);
	    $('#quantidade').val('');
	    $('#valorTube').val('');
    
    });
   
});


$('#quantidade').blur(function(){
   
   var quantidade = $('#quantidade').val();
   var valor =  parseFloat($('.spanValor').html().replace(',','.'));
   
   calculo = valor/1000;
   calculo = quantidade*calculo;

   if (isNaN(calculo)) {
   	$('#valorTube').val('0,00');
   }else{
   	$('#valorTube').val(parseFloat(calculo.toFixed(2)).toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2
    }));
   }
   

});
