$("#formFaceCut").validate({
    rules: {
      servicoface: "required",
      idface: "required",
      quantidade: "required",
    }

});


$('#servicoface').change(function(){
   
   var servico = $('#servicoface').val();

   $.ajax({
      method: "POST",
      url: base_url+"facebook/selecionarServicoTable/",
      dataType: "html",
      data: { servico: servico}
      })
      .done(function( response ) {

      $('#facetable').html(response);
    
    });


  $.ajax({
      method: "POST",
      url: base_url+"facebook/selecionarServicoMini/",
      dataType: "html",
      data: { servico: servico}
      })
      .done(function( response ) {

      $('#faceMini').html(response);
      $('#quantidade').val('');
      $('#valorFace').val('');
    
    });
   
});


// Math.floor(calculo * 100) / 100)



$('#quantidade').blur(function(){
   
   var quantidade = $('#quantidade').val();
   var valor =  parseFloat($('.spanValor').html().replace(',','.'));
   
   calculo = valor/1000;
   calculo = quantidade*calculo;

   if (isNaN(calculo)) {
    $('#valorFace').val('0,00');
   }else{
    $('#valorFace').val(parseFloat(calculo.toFixed(2)).toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2
    }));
   }
   

});
