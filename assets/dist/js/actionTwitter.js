$("#formTwCut").validate({
    rules: {
      servicotw: "required",
      idtw: "required",
      quantidade: "required",
    }

});


$('#servicotw').change(function(){
   
   var servico = $('#servicotw').val();

   $.ajax({
	    method: "POST",
	    url: base_url+"twitter/selecionarServicoTable/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#twtable').html(response);
    
    });


	$.ajax({
	    method: "POST",
	    url: base_url+"twitter/selecionarServicoMini/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#twMini').html(response);
	    $('#quantidade').val('');
	    $('#valorTw').val('');
    
    });
   
});


$('#quantidade').blur(function(){
   
   var quantidade = $('#quantidade').val();
   var valor =  parseFloat($('.spanValor').html().replace(',','.'));
   
   calculo = valor/1000;
   calculo = quantidade*calculo;

   if (isNaN(calculo)) {
   	$('#valorTw').val('0,00');
   }else{
   	$('#valorTw').val(parseFloat(calculo.toFixed(2)).toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2
    }));
   }
   

});
