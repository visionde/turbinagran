$("#formTikCut").validate({
    rules: {
      servicotik: "required",
      idtik: "required",
      quantidade: "required",
    }

});


$('#servicotik').change(function(){
   
   var servico = $('#servicotik').val();

   $.ajax({
	    method: "POST",
	    url: base_url+"tiktok/selecionarServicoTable/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#tiktable').html(response);
    
    });


	$.ajax({
	    method: "POST",
	    url: base_url+"tiktok/selecionarServicoMini/",
	    dataType: "html",
	    data: { servico: servico}
	    })
	    .done(function( response ) {

	    $('#tikMini').html(response);
	    $('#quantidade').val('');
	    $('#valorTik').val('');
    
    });
   
});


$('#quantidade').blur(function(){
   
   var quantidade = $('#quantidade').val();
   var valor =  parseFloat($('.spanValor').html().replace(',','.'));
   
   calculo = valor/1000;
   calculo = quantidade*calculo;

   if (isNaN(calculo)) {
   	$('#valorTik').val('0,00');
   }else{
   	$('#valorTik').val(parseFloat(calculo.toFixed(2)).toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2
    }));
   }
   

});
