<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->library('session');
		
	}

	public function index()
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dadosView['meio'] = 'curso/curso';
		$this->load->view('tema/layout',$dadosView);	
		
	}

}