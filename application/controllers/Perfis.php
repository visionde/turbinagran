<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfis extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Perfis_model');
		$this->load->model('Saldo_model');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');	


		$saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));
		$sessao = array('saldo'          => str_replace('.', ',', $saldo[0]->saldo_valor));
		$this->session->set_userdata($sessao);
		
	}

    public function index()
	{

		$dadosView['meio'] = 'perfis/perfis';
		$dadosView['dados'] = $this->Perfis_model->buscarDadosGeral();
		$this->load->view('tema/layout',$dadosView);	
		
	}

	public function ajustarPerfil()
	{

        $dados = array(                                           
            $this->input->post('idCampo') => $this->input->post('valor'),                                  
        );

        $id = $this->input->post('idUsuario');   
        
        $dadosView['dados'] = $this->Perfis_model->ajustarPerfil($dados, $id);
		
	}

	public function ajustarSaldo()
	{

        $dados = array(                                           
            $this->input->post('idCampo') => $this->input->post('valor'),                                  
        );

        $id = $this->input->post('idUsuario');   
        
        $dadosView['dados'] = $this->Perfis_model->ajustarSaldo($dados, $id);
		
	}

   public function excluir()
	{  

	    $id = $this->uri->segment(3);
	    $status = $this->uri->segment(4);
	    $usuario_visivel = 0 ; 
	              
	    $resultado = $this->Perfis_model->excluir($usuario_visivel,$id);	

		if($resultado){
			$this->session->set_flashdata('success','Usuário excluído com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir os dados!');
		}

		redirect('perfis/'.$status,'refresh');  
	}


}