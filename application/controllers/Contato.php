<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->library('session');
		
	}

	public function index()
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dadosView['meio'] = 'contato/contato';
		$this->load->view('tema/layout',$dadosView);	
		
	}


	public function contatoMenu()
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to('turbinagran@gmail.com'); 

	        $this->email->subject("Email de Contato");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá Suporte o usuário ".$this->input->post('nome')." entrou em contato,</span>
					                            <span style='display:block'>Pedindo a seguinda ajuda:</span>
					                        </p>
					                    </td>
					                </tr>
					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p style='margin-bottom:0px'>".$this->input->post('message').".</p>
					                    </td>
					                </tr>
					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Entrar em contato atraves:</span>
					                            <span style='display:block'>Email: ".$this->input->post('email')."</span>
					                            <span style='display:block'>Telefone: ".$this->input->post('tel')."</span>
					                        </p>
					                    </td>
					                </tr>
					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

	        if($this->email->send()) {
	            $this->session->set_flashdata('success','Email enviado com sucesso aguarde o retorno do SUPORTE!');
			    $this->load->view('register/registerConf');   
	        } else {
	            $this->session->set_flashdata('erro','Ocorreu um erro tente novamente!');
			    $this->load->view('register/registerErro');
	        }  
        
       
	}



    public function contatoWelcome()
	{

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to('turbinagran@gmail.com'); 

	        $this->email->subject("Email de Contato");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá Suporte o usuário ".$this->input->post('name')." entrou em contato,</span>
					                            <span style='display:block'>Pedindo a seguinda ajuda:</span>
					                        </p>
					                    </td>
					                </tr>
					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p style='margin-bottom:0px'>".$this->input->post('message').".</p>
					                    </td>
					                </tr>
					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Entrar em contato atraves:</span>
					                            <span style='display:block'>Email: ".$this->input->post('email')."</span>
					                            <span style='display:block'>Telefone: ".$this->input->post('tel')."</span>
					                        </p>
					                    </td>
					                </tr>
					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

	        if($this->email->send()) {
	            $this->session->set_flashdata('success','Email enviado com sucesso aguarde o retorno do SUPORTE!');
			    $this->load->view('register/registerConf');   
	        } else {
	            $this->session->set_flashdata('erro','Ocorreu um erro tente novamente!');
			    $this->load->view('register/registerErro');
	        } 
        
       
	}




}