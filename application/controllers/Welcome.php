<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Welcome_model');
		$this->load->model('Saldo_model');
		$this->load->model('Ordem_model');
		$this->load->model('Register_model');
		$this->load->model('Parametros_model');
		$this->load->model('MercadoPago_model');
		$this->load->model('Indicacao_model');
		$this->load->library('session');	
	}

	/**
	 * [index description]Redireciona para o login do sistema, este é o controller default e metodo padrão
	 * @return [type] [description]
	 */


    public function index()
	{

		if(!$this->session->userdata('usuario_id')){
			
			$this->load->view('welcome');

		} else {
			 	
			redirect(base_url() . "welcome/dashboard");

		}
		

	}	

	// /**
	//  * [processarLogin description] Método responsável para processar os dadoso de usuário e senha do sistema
	//  * @return [type] [description]
	//  */
	public function processarLogin($usuario = NULL, $senha = NULL, $perfil = NULL)
	{

		if (!empty($usuario) AND !empty($senha) AND !empty($perfil)) {

		   $dados['usuario'] = base64_decode($usuario);
           $dados['senha'] = $senha;
           $dados['perfil'] = $perfil;

		}else{

		   $dados['usuario'] = $this->input->post('usuario');
           $dados['senha'] = sha1(md5(strtolower($this->input->post('senha'))));
           $dados['perfil'] = $this->input->post('perfil');

		}

		if ($this->Register_model->validarUsuario($dados['usuario']) == false) {

			$this->session->set_flashdata('erro','Usuario sem cadastro no nosso sistema!');
			$this->load->view('register/registerErro');
        }else{


        $resultado = $this->Welcome_model->processarLogin($dados);
        

        if ($resultado) {

           $saldo = $this->Saldo_model->consultaSaldo($resultado[0]->usuario_id);
		   $bonus = $this->Indicacao_model->consultaBonus($resultado[0]->usuario_id);
           $mercado = $this->MercadoPago_model->consultarMercado($resultado[0]->usuario_id);
	       foreach ($mercado as $v) { $this-> notification($v->mercadopago_payment_id, $v->mercadopago_id, $v->historico_saldo_valor);}

			$sessao = array(
				            'usuario_id'     => $resultado[0]->usuario_id,
							'usuario_nome'   => $resultado[0]->usuario_nome,
							'perfil_nome'    => $resultado[0]->perfil_nome,
							'perfil_id'      => $resultado[0]->perfil_id,
							'saldo'          => str_replace('.', ',', $saldo[0]->saldo_valor),
							'bonus'          => empty($bonus[0]->total) ? '0,00' : str_replace('.', ',', $bonus[0]->total),
							);

	
		   $this->session->set_userdata($sessao);
           redirect('welcome/dashboard');

		
		}else{

			$this->session->set_flashdata('erro','Login ou senha inválidos!');	
			$this->load->view('register/registerErro');			
        }

      }
		
	}


	public function login()
	{
		
		$this->load->view('login-register');

	}

	public function recoverPassword()
	{
		
		$this->load->view('recoverPassword');

	}

	public function register()
	{

		$dados['perfil'] = $this->input->post('perfilCadastro');
		$dados['codigo'] = $this->input->get('ref');
		
		$this->load->view('register', $dados);

	}


	public function newPassword($email = null, $token = null)
	{

		if ($token == base64_encode(date('dmY'))) {

			$this->load->view('newPassword');

		}else{

			$this->session->set_flashdata('erro','Token Invalido!');
			$this->load->view('register/registerErro');

		}
		

	}

   public function dashboard()
	{	
	    if($this->session->userdata('usuario_id')==null) redirect('welcome');

	    $saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));
		$bonus = $this->Indicacao_model->consultaBonus($this->session->userdata('usuario_id'));

	    $dadosView['saldo'] =  str_replace('.', ',', $saldo[0]->saldo_valor);
		$dadosView['bonus'] =  empty($bonus[0]->total) ? '0,00' : str_replace('.', ',', $bonus[0]->total);
	    $dadosView['ordem'] = $this->Ordem_model->consultaOrdem($this->session->userdata('usuario_id'));
		$dadosView['meio'] = 'dashboard/dashboard';
		$this->load->view('tema/layout',$dadosView);	
	}	

    /**
	 * [logout description]Ação de Logout do sistema
	 * @return [type] [description]
	 */
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('welcome');
	}

	public function notification($cod, $idMercado, $valor)
	{

		$configMercado = $this->Parametros_model->buscarPararametroIdD('3');

		$config['use_access_token'] = $configMercado[0]->parametro_token;
		$config['client_secret'] = $configMercado[0]->parametro_url;
		$config['client_id'] =  $configMercado[0]->parametro_usuario;
		$config['sandbox_mode'] = true;
		
	    $this->load->library('Mercadopago', $config); 

	    $token_access = ["access_token" => $this->mercadopago->get_access_token()];

	    $payment_info = $this->mercadopago->get("/collections/notifications/" . $cod , $token_access, false);

	    $statusMercado = $payment_info["response"]["collection"]["status"];

        $status = array(
            'approved'     => "CONCLUIDO",
            'pending'      => "PENDENTE",
            'in_process'   => "ANALISE",
            'rejected'     => "REJEITADO",
            'refunded'     => "DEVOLVIDO",
            'cancelled'    => "CANCELADO",
            'in_mediation' => "MEDIANÇÃO"
         );

        foreach ($status as $key => $value) {

        	if ($statusMercado == $key) {

        		$this->MercadoPago_model->atualizarHistoricoMercadoPago($value, $idMercado);

        		if ($value == 'CONCLUIDO') {
        			$this->MercadoPago_model->atualizarSaldo($this->session->userdata('usuario_id'), $valor);
					$this->Indicacao_model->inserirValor($this->session->userdata('usuario_id'), $valor);
        		}

        	}
        	
        }
        
	 }



}
