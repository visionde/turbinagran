<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contas extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Comprovante_model');
		$this->load->model('Indicacao_model');
		$this->load->model('Saldo_model');
		$this->load->model('Parametros_model');
		$this->load->model('MercadoPago_model');
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$mercado = $this->MercadoPago_model->consultarMercado($this->session->userdata('usuario_id'));
	    foreach ($mercado as $v) { $this-> notification($v->mercadopago_payment_id, $v->mercadopago_id, $v->historico_saldo_valor);}

	    $saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));
		$sessao = array('saldo'          => str_replace('.', ',', $saldo[0]->saldo_valor));
		$this->session->set_userdata($sessao);
	}

	public function index()
	{
		$dadosView['meio'] = 'contas/contas';
		$this->load->view('tema/layout',$dadosView);			
	}

	public function cadastrarComprovante()
	{
	

        $arrDadosArquivo["type"] = $_FILES["fileComprovante"]["type"];

        $separador = explode('/', $arrDadosArquivo["type"]);
        $separador = $separador[1];

        if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg' ) and ($separador <> 'PDF') and ($separador <> 'pdf')) {
           
             $this->session->set_flashdata('erro','Extenção do arquivo invalida. Tente Novamente!');
		     redirect('contas');
         }

	    $arquivo = date('dmYhis') . '.' .$separador;
		
	     $configuracao = array(
	         'upload_path'   => './assets/arquivos/comprovantes',
	         'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG|pdf|PDF',
	         'file_name'     => $arquivo,
	         'max_size'      => '0'
	     );      
	     $this->load->library('upload');
	     $this->upload->initialize($configuracao);
	     if ($this->upload->do_upload('fileComprovante')){
	         $arquivoValid = true;
	     }else{
	     	 $this->session->set_flashdata('erro', $this->upload->display_errors());
		     redirect('contas');
		 }

		  if($arquivoValid == true) // se o arquivo foi despejado corretamente
          {

				$dados = array(
								'usuario_id'             => $this->session->userdata('usuario_id'),
								'comprovante'            => $arquivo
								
					);


				$resultado = $this->Comprovante_model->cadastrarComprovante($dados);
				
				if($resultado){
					$this->enviarEmailComprovanteAdmin($this->session->userdata('usuario_id'));
					$this->session->set_flashdata('success','comprovante anexado com sucesso!');
				}else{
					$this->session->set_flashdata('erro','Erro ao inserir comprovante!');
				}

				redirect('contas'); 
		   }else{

		   	   $this->session->set_flashdata('erro','Erro ao inserir comprovante!');
		   }

	}


	public function gestao()
	{

		if($this->session->userdata('perfil_id')!='1') redirect('welcome');


		$dadosView['dados'] = $this->Comprovante_model->buscarDados(); 
		$dadosView['meio'] = 'contas/gestao_saldo';
		$this->load->view('tema/layout',$dadosView);	
		
	}


	public function ComprovanteStatusInvalidar()
	{  
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

	    $id = $this->uri->segment(3);

	    $status = $this->uri->segment(4);

	    $idUsuario = $this->uri->segment(5);		      
	              
	    $resultado = $this->Comprovante_model->ComprovanteStatusInvalidar($status, $id);	

		if($resultado){
			$this->enviarEmailComprovanteInvalidar($idUsuario);
			$this->session->set_flashdata('success','Status alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao alterar os dados!');
		}

		redirect('contas/gestao','refresh');  
	}


    public function ComprovanteStatusValidar()
	{  
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

	    $id = $this->input->post('modal_comprovante');
	    $idUsuario = $this->input->post('modal_usuario');
	    $valor = $this->input->post('modal_valor');
	    $status = '2';	      
	              
	    $resultado = $this->Comprovante_model->ComprovanteStatusValidar($status, $id, $valor, $idUsuario);	

		if($resultado){
			$this->Indicacao_model->inserirValor($idUsuario, $valor);
			$this->enviarEmailComprovanteValidar($idUsuario, $valor);
			$this->session->set_flashdata('success','Status alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao alterar os dados!');
		}

		redirect('contas/gestao','refresh');  
	}

	public function mercadoExe()
	{

		$configMercado = $this->Parametros_model->buscarPararametroIdD('3');

		$config['use_access_token'] = $configMercado[0]->parametro_token;
		$config['client_secret'] = $configMercado[0]->parametro_url;
		$config['client_id'] =  $configMercado[0]->parametro_usuario;
		$config['sandbox_mode'] = true;

		$valor = $this->input->post('valor');
		$this->session->set_userdata($sessao = array('valor' => $valor));
		
	    $this->load->library('Mercadopago', $config);   

	        $preference_data = array(

		       "items" => array(
		         array(
		           "id" => '1',
		           "title" => 'Adição de saldo no site: TurbinaGran - Painel SMM',
		           "currency_id" => "BRL",
		           "picture_url" => "",
		           "description" => 'Adição de saldo no site: TurbinaGran - Painel SMM',
		           "quantity" => 1,
		           // "unit_price" => 5
		           "unit_price" => (double)$valor
		         )
		       ),

		       "back_urls" => array(
		         "success" => base_url()."contas/pagamentoSucesso",
		         "failure" => base_url()."contas/pagamentoFalha",
		         "pending" => base_url()."contas/pagamentoPendente"
		       ),

		       "payment_methods" => array(
			        "excluded_payment_types" => array(
					    array("id" => "ticket")
					  ),
			    ),
		       "notification_url" => "",
		       "external_reference" =>'3'
		    );

		    $preference = $this->mercadopago->create_preference($preference_data);

		    $this->mercadopago->sandbox_mode(FALSE);

		    $link = $preference["response"]["init_point"];

		    if($link != false){
		      echo json_encode($link);
		    }else{
		      echo json_encode('error');
		    }

				
	}


   public function pagamentoFalha()
	{
         $this->session->set_flashdata('erro','Pagamento não Realizado!');
         redirect('contas','refresh'); 
	}


   public function pagamentoPendente()
	{

		if( isset($_GET['collection_id']) || isset($_GET['id']) ){

			if ($this->MercadoPago_model->validarPagamento($_GET['payment_id']) == false) {
				$dados = array(                                            
		            'mercadopago_referencia'   => $_GET['external_reference'], 
		            'mercadopago_payment_id'   => $_GET['payment_id'],                                 
		        );

		       // echo "<pre>"; var_dump($dados); exit();

		        $resultado = $this->MercadoPago_model->atualizarDadosMercadoPago($dados, 'PENDENTE', $this->session->userdata('valor'));

	            if($resultado){
					$this->session->set_flashdata('warning','Pagamento se encontra Pendente!');
					redirect('contas','refresh'); 
				}else{
					$this->session->set_flashdata('erro','Ocorreu um erro inesperado. Entra em contato com SUPORTE!');
					redirect('contas','refresh');  
				}
			}else{

				$this->session->set_flashdata('erro','Pagamento já se encontra no sistema. Dúvidas entra em contato com SUPORTE!');
				redirect('contas','refresh');  
			}

		 }else{

		 	$this->session->set_flashdata('erro','Ocorreu um erro inesperado. Entra em contato com SUPORTE');
            redirect('contas','refresh'); 
 
		 }
         
	}


	public function pagamentoSucesso()
	{

		if( isset($_GET['collection_id']) || isset($_GET['id']) ){

			if ($this->MercadoPago_model->validarPagamento($_GET['payment_id']) == false) {
				$dados = array(                                            
		            'mercadopago_referencia'   => $_GET['external_reference'], 
		            'mercadopago_payment_id'   => $_GET['payment_id'],                                 
		        );

		        $resultado = $this->MercadoPago_model->atualizarDadosMercadoPago($dados, 'CONCLUIDO', $this->session->userdata('valor'));

	            if($resultado){

					$this->Indicacao_model->inserirValor($this->session->userdata('usuario_id'), $this->session->userdata('valor'));

	            	if ($this->MercadoPago_model->atualizarSaldo($this->session->userdata('usuario_id'), $this->session->userdata('valor')) == false) {

	                    $this->session->set_flashdata('erro','Ocorreu um erro inesperado na operação de saldo. Entra em contato com SUPORTE!');
						redirect('contas','refresh');  
	            	}

					$this->session->set_flashdata('success','Pagamento realizado com sucesso!');
					redirect('contas','refresh'); 
				}else{
					$this->session->set_flashdata('erro','Ocorreu um erro inesperado. Entra em contato com SUPORTE!');
					redirect('contas','refresh');  
				}
			}else{

				$this->session->set_flashdata('erro','Pagamento já se encontra no sistema. Dúvidas entra em contato com SUPORTE!');
				redirect('contas','refresh');  
			}

		 }else{

		 	$this->session->set_flashdata('erro','Ocorreu um erro inesperado. Entra em contato com SUPORTE');
            redirect('contas','refresh'); 
 
		 }
         
	}


    public function notification($cod, $idMercado, $valor)
	{

		$configMercado = $this->Parametros_model->buscarPararametroIdD('3');

		$config['use_access_token'] = $configMercado[0]->parametro_token;
		$config['client_secret'] = $configMercado[0]->parametro_url;
		$config['client_id'] =  $configMercado[0]->parametro_usuario;
		$config['sandbox_mode'] = true;
		
	    $this->load->library('Mercadopago', $config); 

	    $token_access = ["access_token" => $this->mercadopago->get_access_token()];

	    $payment_info = $this->mercadopago->get("/collections/notifications/" . $cod , $token_access, false);

	    $statusMercado = $payment_info["response"]["collection"]["status"];

        $status = array(
            'approved'     => "CONCLUIDO",
            'pending'      => "PENDENTE",
            'in_process'   => "ANALISE",
            'rejected'     => "REJEITADO",
            'refunded'     => "DEVOLVIDO",
            'cancelled'    => "CANCELADO",
            'in_mediation' => "MEDIANÇÃO"
         );

        foreach ($status as $key => $value) {

        	if ($statusMercado == $key) {

        		$this->MercadoPago_model->atualizarHistoricoMercadoPago($value, $idMercado);

        		if ($value == 'CONCLUIDO') {

        			$this->MercadoPago_model->atualizarSaldo($this->session->userdata('usuario_id'), $valor);
        		}

        	}
        	
        }
        
	 }


	 public function enviarEmailComprovanteAdmin($id)
	 {

	 	    $dados = $this->Comprovante_model->buscarDadadosCliente($id);

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to('turbinagran@gmail.com'); 

	        $this->email->subject("Aviso de Comprovante Recarga TurbinaGran");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá Administradores,</span>
					                            <span style='display:block'>O usuário ".$dados[0]->usuario_nome." acaba de enviar um comprovante para análise.</span>
					                            <span style='display:block'>Por favor, analisar !!!</span>
					                        </p>
					                    </td>
					                </tr>

					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

            $this->email->send();

	 }

	public function enviarEmailComprovanteValidar($id, $valor)
	 {

	 	    $dados = $this->Comprovante_model->buscarDadadosCliente($id);

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to($dados[0]->usuario_email); //turbinagran@gmail.com

	        $this->email->subject("Saldo adicionado TurbinaGran");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá ".$dados[0]->usuario_nome.",</span>
					                            <span style='display:block'>Enviamos essa mensagem para notifica-lo de que foi adicionado R$ ".str_replace('.', ',', $valor)." ao seu Painel TurbinaGran.</span>
					                        </p><br>
					                        <p>
					                            <span style='display:block'>Qualquer dúvida que tiver, você pode entrar em contato conosco através deste email ou pelo WhatsApp <a class='nav-link' href='https://wa.me/5581981469522' target='_blank'>(81)98146-9522<a/></span>
					                        </p>
					                        
					                    </td>
					                </tr>

					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

            $this->email->send();

	 }

	 public function enviarEmailComprovanteInvalidar($id)
	 {

	 	    $dados = $this->Comprovante_model->buscarDadadosCliente($id);

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to($dados[0]->usuario_email); //turbinagran@gmail.com

	        $this->email->subject("Comprovante inválido TurbinaGran");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá ".$dados[0]->usuario_nome.",</span>
					                            <span style='display:block'>Informamos que o comprovante enviado por você foi considerado inválido por nossos analistas.</span>
					                            <span style='display:block'>Solicitamos o envio de um novo comprovante.</span>

					                        </p><br>
					                         <p>
					                            <span style='display:block'>Em caso de dúvidas, responda a este email.</span>
					                        </p>
					                    </td>
					                </tr>

					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

            $this->email->send();

	 }



}