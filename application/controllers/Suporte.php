<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suporte extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Suporte_model');
		$this->load->library('session');
		
	}

	public function index()
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dadosView['meio'] = 'suporte/ticket';
		$this->load->view('tema/layout',$dadosView);	
		
	}

	public function abrir()
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dados = array(        		 				 
				  'usuario_id'            => $this->session->userdata('usuario_id'),
				  'ticket_assunto'        => $this->input->post('assunto'),	
				  'ticket_messagem' 	  => $this->input->post('message'),
	    );

	    $id = $this->Suporte_model->adicionar($dados);

    	if($id){
			$this->session->set_flashdata('success','Ticket aberto com sucesso!');
			redirect('suporte/ticket/'.$id,'refresh'); 
		}else{
			$this->session->set_flashdata('erro','Erro ao adicionar os dados!');
			redirect('suporte','refresh'); 
		}
	}


	public function abertos()
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dadosView['dados'] = $this->Suporte_model->abertos($this->session->userdata('usuario_id'));

		$dadosView['meio'] = 'suporte/abertos';
		$this->load->view('tema/layout',$dadosView);	
		
	}


	public function ticket($id)
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dadosView['ticket'] = $this->Suporte_model->dadosticket($id);
		$dadosView['dadosTickts'] = $this->Suporte_model->dadosTicketChat($id);

		$dadosView['meio'] = 'suporte/chat';
		$this->load->view('tema/layout',$dadosView);	
		
	}


	public function fechados()
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dadosView['dados'] = $this->Suporte_model->fechados($this->session->userdata('usuario_id'));

		$dadosView['meio'] = 'suporte/fechados';
		$this->load->view('tema/layout',$dadosView);	
		
	}


    public function ticketCliente()
	{
		
		$dados = array(        		 				 
				  'ticket_id'            => $this->input->post('id'),
				  'chat_ticket_messagem' => $this->input->post('message'),
	    );

	    $this->Suporte_model->adicionarChat($dados);

	    $ticket = $this->Suporte_model->dadosticket($this->input->post('id'));
		$dadosTickts = $this->Suporte_model->dadosTicketChat($this->input->post('id'));


		    echo ' <div class="time-label">
                <span class="bg-red">Aberto '.date("d M Y", strtotime(str_replace('/', '-',$ticket[0]->ticket_data))).'</span>
              </div>
              <div>
                <i class="fas fa-clipboard-list bg-blue"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fas fa-clock"></i> '.date("H:i", strtotime(str_replace('/', '-',$ticket[0]->ticket_data))).'</span>
                  <h3 class="timeline-header"><a href="#">'.ucfirst($ticket[0]->ticket_assunto).'</a></h3>

                  <div class="timeline-body">
                    '.ucfirst($ticket[0]->ticket_messagem).'
                  </div>
                </div>
              </div>';

             

                  $date = 0; 
                  foreach ($dadosTickts as $k) { 

                      if ($date != date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data)))) { 

                           echo '<div class="time-label">
                               <span class="bg-green">'.date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                            </div>'; 
                       }

                       $date = date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data)));

                      if ($k->chat_ticket_suporte != NULL) {
                        
                        echo '<div>
                            <i class="fas fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="fas fa-clock"></i> '.date("H:i", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                              <h3 class="timeline-header"><a href="#">Support Team</a></h3>

                              <div class="timeline-body">
                                '.ucfirst($k->chat_ticket_messagem).'
                              </div>
                            </div>
                          </div>';

                      } else {
                        
                        echo '<div>
                                <i class="fas fa-user bg-green"></i>
                                <div class="timeline-item">
                                  <span class="time"><i class="fas fa-clock"></i>'.date("H:i", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                                  <h3 class="timeline-header"><a href="#">'.$k->usuario_nome.'</a></h3>
                                  <div class="timeline-body">
                                    '.ucfirst($k->chat_ticket_messagem).'
                                  </div>
                                </div>
                              </div>';

                      }
                     
                  }


            $ultimo = end($dadosTickts); 


             if($ultimo->ticket_status == '2'){

	             echo '<div class="time-label">
	               <span class="bg-red">Fechado</span>
	            </div>'; 

             }else{

                  echo '<div>
		                <i class="fas fa-clock bg-gray"></i>
		                <div class="timeline-item">
		                  <h3 class="timeline-header"><a href="#">'.$this->session->userdata('usuario_nome').'></a></h3>
		                  <div class="timeline-body">
		                      <div class="form-group">
		                        <label for="message">Mensagem</label>
		                        <textarea class="form-control" name="message" id="message" data="'.$this->uri->segment(3).'" status="'.$ultimo->chat_ticket_suporte.'" maxlength="512" class="input" placeholder="Descreva a sua resposta aqui"></textarea>
		                      </div>
		                  </div>
		                </div>
		              </div>';

             }

	}


    public function gestaoticket()
	{
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

		$dadosView['dados'] = $this->Suporte_model->geral();

		$dadosView['meio'] = 'suporte/gestao';
		$this->load->view('tema/layout',$dadosView);	
		
	}

	public function ticketAdmin($id)
	{
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$dadosView['ticket'] = $this->Suporte_model->dadosticket($id);
		$dadosView['dadosTickts'] = $this->Suporte_model->dadosTicketChat($id);

		$dadosView['meio'] = 'suporte/chatAdmin';
		$this->load->view('tema/layout',$dadosView);	
		
	}


   public function ticketAdminchat()
	{
		
		$dados = array(        		 				 
				  'ticket_id'            => $this->input->post('id'),
				  'chat_ticket_suporte'  => '1',
				  'chat_ticket_messagem' => $this->input->post('message'),
	    );

	    $this->Suporte_model->adicionarChat($dados);

	    $ticket = $this->Suporte_model->dadosticket($this->input->post('id'));
		$dadosTickts = $this->Suporte_model->dadosTicketChat($this->input->post('id'));


		//echo "<pre>"; var_dump( $ticket); exit();


		    echo ' <div class="time-label">
                <span class="bg-red">Aberto '.date("d M Y", strtotime(str_replace('/', '-',$ticket[0]->ticket_data))).'</span>
              </div>
              <div>
                <i class="fas fa-clipboard-list bg-blue"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fas fa-clock"></i> '.date("H:i", strtotime(str_replace('/', '-',$ticket[0]->ticket_data))).'</span>
                  <h3 class="timeline-header"><a href="#">'.ucfirst($ticket[0]->ticket_assunto).'</a></h3>

                  <div class="timeline-body">
                    '.ucfirst($ticket[0]->ticket_messagem).'
                  </div>
                </div>
              </div>';

             

                  $date = 0; 
                  foreach ($dadosTickts as $k) { 

                      if ($date != date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data)))) { 

                           echo '<div class="time-label">
                               <span class="bg-green">'.date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                            </div>'; 
                       }

                       $date = date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data)));

                      if ($k->chat_ticket_suporte != NULL) {
                        
                        echo '<div>
                            <i class="fas fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="fas fa-clock"></i> '.date("H:i", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                              <h3 class="timeline-header"><a href="#">Support Team</a></h3>

                              <div class="timeline-body">
                                '.ucfirst($k->chat_ticket_messagem).'
                              </div>
                            </div>
                          </div>';

                      } else {
                        
                        echo '<div>
                                <i class="fas fa-user bg-green"></i>
                                <div class="timeline-item">
                                  <span class="time"><i class="fas fa-clock"></i>'.date("H:i", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                                  <h3 class="timeline-header"><a href="#">'.$k->usuario_nome.'</a></h3>
                                  <div class="timeline-body">
                                    '.ucfirst($k->chat_ticket_messagem).'
                                  </div>
                                </div>
                              </div>';

                      }
                     
                  }


            $ultimo = end($dadosTickts); 


             if($ultimo->ticket_status == '2'){

	             echo '<div class="time-label">
	               <span class="bg-red">Fechado</span>
	            </div>'; 

             }else{

                  echo '<div>
		                <i class="fas fa-clock bg-gray"></i>
		                <div class="timeline-item">
		                  <h3 class="timeline-header"><a href="#">Support Team</a></h3>
		                  <div class="timeline-body">
		                      <div class="form-group">
		                        <label for="message">Mensagem</label>
		                        <textarea class="form-control" name="message" id="message" data="'.$this->input->post('id').'" status="'.$ultimo->chat_ticket_suporte.'" maxlength="512" class="input" placeholder="Descreva a sua resposta aqui"></textarea>
		                      </div>
		                  </div>
		                </div>
		              </div>';

             }

	}


   public function ticketFechar()
	{
		
		$dados = array(        		 				 
				  'ticket_status'  => '2',
	    );

	    $this->Suporte_model->ticketFechar($dados, $this->input->post('id'));

	    echo json_encode(array('result'=> true));

    }


}