<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Servico_model');
		$this->load->model('Parametros_model');
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');	
	}


	public function index()
	{

		$dadosView['meio'] = 'servico/servico';
        $dadosView['dados'] = $this->Servico_model->buscarDadosGeral();
		$this->load->view('tema/layout',$dadosView);	
		
	}

	public function gestao()
	{

		if($this->session->userdata('perfil_id')!='1') redirect('welcome');
        
        $dadosView['dados'] = $this->Servico_model->buscarDadosGeralGestao();
        $dadosView['parametro'] = $this->Parametros_model->parametroAtivo();
		$dadosView['meio'] = 'servico/gestao';
		$this->load->view('tema/layout',$dadosView);	
		
	}


    public function ajustarServico()
	{

		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

        $dados = array(                                           
            $this->input->post('idCampo') => $this->input->post('valor'),                                  
        );

        $id = $this->input->post('idServico');   
        
        $dadosView['dados'] = $this->Servico_model->ajustarServico($dados, $id);

		$dadosView['meio'] = 'servico/gestao';
		$this->load->view('tema/layout',$dadosView);	
		
	}

	public function addServico()
	{

		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

        $dados = array(                                           
            'redes_id'           => $this->input->post('modal_redes'),
            'parametro_id'       => $this->input->post('modal_serviço_sistema'),    
            'servico_nome'       => $this->input->post('modal_serviço'),
            'servico_observacao' => $this->input->post('modal_serviço_observacao'), 
            'externo_id'         => $this->input->post('modal_serviço_id_externo'),  
            'servico_minimo'     => $this->input->post('modal_serviço_minimo'),  
            'servico_maximo'     => $this->input->post('modal_serviço_maximo'),  
            'servico_preco '     => $this->input->post('modal_serviço_preço'),
            'servico_preco_rev ' => $this->input->post('modal_serviço_preço_rev'),                                 
        );


        $resultado = $this->Servico_model->adicionar($dados,'servicos');

        
		if($resultado){
			$this->session->set_flashdata('success','Serviço adicionado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao adicionar os dados!');
		}

		redirect('servico/gestao','refresh');  
		
	}

	public function addRedes()
	{
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

        $dados = array(                                           
            'redes_nome'       => $this->input->post('modal_redes'),                                  
        );

        
        $resultado = $this->Servico_model->adicionar($dados, 'redes');

        
		if($resultado){
			$this->session->set_flashdata('success','Rede Social adicionada com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao adicionar os dados!');
		}

		redirect('servico/gestao','refresh');  
		
	}


	public function excluir()
	{  
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

	    $id = $this->uri->segment(3);
	    $servico_visivel = 0 ; 
	              
	    $resultado = $this->Servico_model->excluir($servico_visivel,$id);	

		if($resultado){
			$this->session->set_flashdata('success','Serviço excluído com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir os dados!');
		}

		redirect('servico/gestao','refresh');  
	}

   public function status()
	{  
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

	    $id = $this->uri->segment(3);

	    $status = $this->uri->segment(4);

	    if ($status == '0') {
	    	$servico_status = 1 ; 
	    } else {
	    	$servico_status = 0 ; 
	    }
	      
	              
	    $resultado = $this->Servico_model->status($servico_status,$id);	

		if($resultado){
			$this->session->set_flashdata('success','Satatus do Serviço alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao alterar os dados!');
		}

		redirect('servico/gestao','refresh');  
	}

    public function excluirRede()
	{  
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

	    $id = $this->uri->segment(3);
	    $redes_visivel = 0 ; 
	              
	    $resultado = $this->Servico_model->excluirRede($redes_visivel,$id);	

		if($resultado){
			$this->session->set_flashdata('success','Rede Social excluída com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir os dados!');
		}

		redirect('servico/gestao','refresh');  
	}

   public function ajaxservico()
    {
      
    $id   = $this->input->post('id');
    $dados   = $this->Servico_model->buscarDadosServico($id);

    echo json_encode($dados);

    }

   public function editarServico()
	{

		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

        $dados = array(                                           
            'redes_id'           => $this->input->post('edi_modal_redes'),
            'parametro_id'       => $this->input->post('edi_modal_serviço_sistema'),    
            'servico_nome'       => $this->input->post('edi_modal_serviço'),
            'servico_observacao' => $this->input->post('edi_modal_serviço_observacao'), 
            'externo_id'         => $this->input->post('edi_modal_serviço_id_externo'),  
            'servico_minimo'     => $this->input->post('edi_modal_serviço_minimo'),  
            'servico_maximo'     => $this->input->post('edi_modal_serviço_maximo'),  
            'servico_preco '     => $this->input->post('edi_modal_serviço_preço'),
            'servico_preco_rev ' => $this->input->post('edi_modal_serviço_preço_rev'),
                                            
        );

        $id = $this->input->post('edi_modal_serviço_id');


        $resultado = $this->Servico_model->editar($dados, $id, 'servicos');

        
		if($resultado){
			$this->session->set_flashdata('success','Serviço alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao editar os dados!');
		}

		redirect('servico/gestao','refresh');  
		
	}

   public function gestaoManual()
	{

		if($this->session->userdata('perfil_id')!='1') redirect('welcome');
        
        $dadosView['dados'] = $this->Servico_model->buscarDadosGeralGestaoManual();
		$dadosView['meio'] = 'servico/gestaoManual';
		$this->load->view('tema/layout',$dadosView);	
		
	}

   public function ajustarOrdem()
	{

		if($this->session->userdata('perfil_id')!='1') redirect('welcome');

        $dados = array(                                           
            $this->input->post('idCampo') => $this->input->post('valor'),                                  
        );

        $id = $this->input->post('idOrdem');   
        
        $dadosView['dados'] = $this->Servico_model->ajustarOrdem($dados, $id);
		
	}






}
