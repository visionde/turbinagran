<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
        $this->load->model('Relatorio_model');
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');	
		
	}

	public function financeiro($page = 1, $status = NULL, $pagamento = NULL)
	{

         $status = isset($_GET['status']) ? $_GET['status'] : $status;
         $pagamento = isset($_GET['pagamento']) ? $_GET['pagamento'] : $pagamento;

	     $itens_per_page = 10;
	     $dadosView['dados']  = $this->Relatorio_model->financeiroListar($page, $itens_per_page, $status, $pagamento);
	     $dadosView['meio']     = 'relatorios/financeiro/financeiro';
	     $total = $this->Relatorio_model->financeiroContador($page, $status, $pagamento);
	     if ($total){
	         $dadosView['total_financeiro']   = $total[0]->total;
	     }else{
	         $dadosView['total_financeiro']   = null;
	     }
	     $dadosView['page']   = $page;
	     $dadosView['status']   = $status;
         $dadosView['pagamento']   = $pagamento;
		 $dadosView['itens_per_page'] = $itens_per_page;
		 $this->load->view('tema/layout',$dadosView);
		
	}


    public function financeiroExcel($status = NULL, $pagamento = NULL)
	{

        $status = isset($_GET['status']) ? $_GET['status'] : $status;
        $pagamento = isset($_GET['pagamento']) ? $_GET['pagamento'] : $pagamento;
	    $dados  = $this->Relatorio_model->financeiroExcel($status, $pagamento);
 
        $this->load->library('PHPExcel');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '#');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Cliente / Revendedor');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Forma de Pagamento');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Status');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Valor');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'Data');

		$linha=2;
		foreach ($dados as $item){

			$historico_saldo_data  = date(('d/m/Y'),strtotime($item->historico_saldo_data));

			if (empty($item->comprovante_id)) {
				if (empty($item->mercadopago_id)) {
					$formpg = 'INDICACÃO';
				} else {
					$formpg = 'MERCADO PAGO';
				}
			} else {
				$formpg = 'COMPROVANTE';
			}

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$linha, $item->historico_saldo_id);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$linha, $item->usuario_nome);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$linha, $formpg);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$linha, $item->historico_saldo_status);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$linha, str_replace('.', ',', $item->historico_saldo_valor));
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$linha, $historico_saldo_data);
		    $linha++;
		}

		//formata o cabeçalho
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="financeiro.xlsx"');
		header('Cache-Control: max-age=0');


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}


	public function usuario($page = 1, $status = NULL)
	{

         $status = isset($_GET['status']) ? $_GET['status'] : $status;

	     $itens_per_page = 10;
	     $dadosView['dados']  = $this->Relatorio_model->usuarioListar($page, $itens_per_page, $status);
	     $dadosView['meio']     = 'relatorios/usuario/usuario';
	     $total = $this->Relatorio_model->usuarioContador($page, $status);
	     if ($total){
	         $dadosView['total_usuario']   = $total[0]->total;
	     }else{
	         $dadosView['total_usuario']   = null;
	     }
	     $dadosView['page']   = $page;
	     $dadosView['status']   = $status;
		 $dadosView['itens_per_page'] = $itens_per_page;
		 $this->load->view('tema/layout',$dadosView);
		
	}

   public function usuarioExcel($status = NULL)
	{

        $status = isset($_GET['status']) ? $_GET['status'] : $status;
	    $dados  = $this->Relatorio_model->usuarioExcel($status);
 
        $this->load->library('PHPExcel');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '#');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Nome');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Email');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Perfil');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Saldo');

		$linha=2;
		foreach ($dados as $item){
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$linha, $item->usuario_id);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$linha, $item->usuario_nome);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$linha, $item->usuario_email);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$linha, $item->perfil_nome);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$linha, str_replace('.', ',', $item->saldo_valor));
		    $linha++;
		}

		//formata o cabeçalho
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="usuario.xlsx"');
		header('Cache-Control: max-age=0');


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}


	public function ordens($page = 1, $status = NULL, $usuario = NULL)
	{

         $status = isset($_GET['status']) ? $_GET['status'] : $status;
         $usuario = isset($_GET['usuario']) ? $_GET['usuario'] : $usuario;

	     $itens_per_page = 5;
	     $dadosView['dados']  = $this->Relatorio_model->ordensListar($page, $itens_per_page, $status, $usuario);
	     $dadosView['meio']     = 'relatorios/ordens/ordens';
	     $total = $this->Relatorio_model->ordensContador($page, $status, $usuario);
	     if ($total){
	         $dadosView['total_ordens']   = $total[0]->total;
	     }else{
	         $dadosView['total_ordens']   = null;
	     }
	     $dadosView['page']   = $page;
	     $dadosView['status']   = $status;
	     $dadosView['usuario']   = $usuario;
		 $dadosView['itens_per_page'] = $itens_per_page;
		 $this->load->view('tema/layout',$dadosView);
		
	}

   public function ordensExcel($status = NULL, $usuario = NULL)
	{

        $status = isset($_GET['status']) ? $_GET['status'] : $status;
        $usuario = isset($_GET['usuario']) ? $_GET['usuario'] : $usuario;

	    $dados  = $this->Relatorio_model->ordensExcel($status, $usuario);
 
        $this->load->library('PHPExcel');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Usuário');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Serviço');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Data');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'QTD/Valor');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Link');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'Status');

		$linha=2;
		foreach ($dados as $item){
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$linha, $item->usuario_nome);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$linha, $item->servico_nome);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$linha, date("d/m/Y H:i:s", strtotime(str_replace('/', '-',$item->ordem_data))));
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$linha, $item->ordem_quantidade.' / R$ '.str_replace('.', ',', $item->ordem_valor));
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$linha, $item->ordem_link);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$linha, $item->ordem_status);
		    $linha++;
		}

		//formata o cabeçalho
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="ordens.xlsx"');
		header('Cache-Control: max-age=0');


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}

	public function bonus($page = 1, $status = NULL)
	{

         $status = isset($_GET['status']) ? $_GET['status'] : $status;

	     $itens_per_page = 5;
	     $dadosView['dados']  = $this->Relatorio_model->bonusListar($page, $itens_per_page, $status);
	     $dadosView['meio']     = 'relatorios/bonus/bonus';
	     $total = $this->Relatorio_model->bonusContador($page, $status);
	     if ($total){
	         $dadosView['total_bonus']   = $total[0]->total;
	     }else{
	         $dadosView['total_bonus']   = null;
	     }
	     $dadosView['page']   = $page;
	     $dadosView['status']   = $status;
		 $dadosView['itens_per_page'] = $itens_per_page;
		 $this->load->view('tema/layout',$dadosView);
		
	}

	public function bonusExcel($status = NULL)
	{

        $status = isset($_GET['status']) ? $_GET['status'] : $status;
	    $dados  = $this->Relatorio_model->bonusExcel($status);
 
        $this->load->library('PHPExcel');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '#');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Usuário');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Usuário Indicado');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Status');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Valor Indicação');

		$linha=2;
		foreach ($dados as $item){
			if ($item->indicacao_status == '0') {

		       $label = 'TRANSFERIDO';

			}elseif ($item->indicacao_status == '2') {

			   $label = 'EM ANÁLISE';

			}else{

			   $label = 'ABERTO';

			}
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$linha, $item->indicacao_id);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$linha, $item->usuario_recebido);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$linha, $item->usuario_indicacao);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$linha, $label);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$linha, str_replace('.', ',', $item->indicacao_valor));
		    $linha++;
		}

		//formata o cabeçalho
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="bonus.xlsx"');
		header('Cache-Control: max-age=0');


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}

	public function bonuspix($page = 1, $status = NULL)
	{

         $status = isset($_GET['status']) ? $_GET['status'] : $status;

	     $itens_per_page = 5;
	     $dadosView['dados']  = $this->Relatorio_model->bonuspixListar($page, $itens_per_page, $status);
	     $dadosView['meio']     = 'relatorios/bonus/bonuspix';
	     $total = $this->Relatorio_model->bonuspixContador($page, $status);
	     if ($total){
	         $dadosView['total_bonuspix']   = $total[0]->total;
	     }else{
	         $dadosView['total_bonuspix']   = null;
	     }
	     $dadosView['page']   = $page;
	     $dadosView['status']   = $status;
		 $dadosView['itens_per_page'] = $itens_per_page;
		 $this->load->view('tema/layout',$dadosView);
		
	}

	public function bonuspixExcel($status = NULL)
	{

        $status = isset($_GET['status']) ? $_GET['status'] : $status;
	    $dados  = $this->Relatorio_model->bonuspixExcel($status);
 
        $this->load->library('PHPExcel');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '#');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Usuário');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Usuário Admin');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Status');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Pix');

		$linha=2;
		foreach ($dados as $item){
			if ($item->pix_status == '0') {

				$label = 'INVÁLIDO';

				}elseif ($item->pix_status == '1') {

				$label = 'APROVADO';

				}else{

				$label = 'AGUARDANDO APROVAÇÃO';

			}
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$linha, $item->pix_id);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$linha, $item->usuario_recebido);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$linha, $item->usuario_liberado);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$linha, $label);
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$linha, $item->pix_valor);
		    $linha++;
		}

		//formata o cabeçalho
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="bonuspix.xlsx"');
		header('Cache-Control: max-age=0');


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}


}