<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parametros extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Parametros_model');
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');
		if($this->session->userdata('perfil_id')!='1') redirect('welcome');	
	}


	public function index()
	{
		$dadosView['parametros'] = $this->Parametros_model->buscarDados();
		$dadosView['meio'] = 'parametros/parametro';
		$this->load->view('tema/layout',$dadosView);	
		
	}

    public function buscarPararametroIdD()
	{
		$result = $this->Parametros_model->buscarPararametroIdD($this->input->post('id'));

		if (isset($result)) {
		  
		    foreach ($result as $k) {

			  	if ($k->parametro_id == '3') {

			  	  echo "<div class='card card-info'><div class='card-header'>
		                <h3 class='card-title'>Configurações ".$k->parametro_nome."</h3>
		              </div>

		              <form method='post' class='send-order'  action='".base_url()."parametros/salvaConfigs'>
		                  <div class='card-body'>

		                    <label class='filed-form'><span class='filed'>Client ID:</span></label>
		                      <input type='text' name='usuario' id='usuario' value='".$k->parametro_usuario."' class='form-control' >
		                      <input type='hidden' name='id' id='id' value='".$k->parametro_id."' class='form-control' >

		                    <label class='filed-form'><span class='filed'>Access Token:</span></label>
		                      <input type='text' name='token' id='token' value='".$k->parametro_token."' class='form-control'>
		                    

		                    <label class='filed-form'><span class='filed'>Client Secret:</span></label>
		                      <input type='text' name='url' id='url' value='".$k->parametro_url."' class='form-control' >

		                  </div>

		                  <div class='card-footer'>
		                    <button type='submit' class='btn btn-primary'>Salvar Config</button>
		                  </div>

		              </form></div>";

			  	} else {  		

			  		echo "<div class='card card-info'><div class='card-header'>
		                <h3 class='card-title'>Configurações ".$k->parametro_nome."</h3>
		              </div>

		              <form method='post' class='send-order'  action='".base_url()."parametros/salvaConfigs'>
		                  <div class='card-body'>

		                    <label class='filed-form'><span class='filed'>Usuario:</span></label>
		                      <input type='text' name='usuario' id='usuario' value='".$k->parametro_usuario."' class='form-control' >
		                      <input type='hidden' name='id' id='id' value='".$k->parametro_id."' class='form-control' >

		                    <label class='filed-form'><span class='filed'>Token:</span></label>
		                      <input type='text' name='token' id='token' value='".$k->parametro_token."' class='form-control'>
		                    

		                    <label class='filed-form'><span class='filed'>URL:</span></label>
		                      <input type='text' name='url' id='url' value='".$k->parametro_url."' class='form-control' >

		                  </div>

		                  <div class='card-footer'>
		                    <button type='submit' class='btn btn-primary'>Salvar Config</button>
		                  </div>

		              </form></div>";
			  	}
			  	
		    }
		}
		
	}

    public function atualizarParametro()
	{
		$result = $this->Parametros_model->atualizarParametro($this->input->post('id'));
		
	}

	public function salvaConfigs()
	{
		
		  $dados = array(
	          'parametro_token'      => $this->input->post('token'),
	          'parametro_url '       => $this->input->post('url'),
	          'parametro_usuario'    => $this->input->post('usuario')
	      );

	   $resultado = $this->Parametros_model->salvaConfigs($dados, $this->input->post('id'));

	    if($resultado){
			$this->session->set_flashdata('success','Alteração feita com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro na alteração dos dados!');
		}

		redirect('parametros/index','refresh');  
		
	}
	

}