<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicacao extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Indicacao_model');
		$this->load->model('Saldo_model');
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

	    $saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));
		$bonus = $this->Indicacao_model->consultaBonus($this->session->userdata('usuario_id'));
		$sessao = array('saldo' => str_replace('.', ',', $saldo[0]->saldo_valor), 'bonus' => empty($bonus[0]->total) ? '0,00' : str_replace('.', ',', $bonus[0]->total));
		$this->session->set_userdata($sessao);
	}

	public function index()
	{
		$dadosView['indicacoes'] = $this->Indicacao_model->consultarIndicacao($this->session->userdata('usuario_id'));
		$dadosView['comissao'] = $this->Indicacao_model->consultaComissao();
		$dadosView['meio'] = 'indicacao/indicacao';
		$this->load->view('tema/layout',$dadosView);			
	}

	public function parametro()
	{
		$dadosView['comissao'] = $this->Indicacao_model->consultaComissao();
		$dadosView['meio'] = 'indicacao/parametro';
		$this->load->view('tema/layout',$dadosView);			
	}

	public function comissao()
	{
		$dados = array('indicacao_parametro'  => $this->input->post('comissao'));
     
            $resultado = $this->Indicacao_model->comissao($dados);

            if($resultado){
                $this->session->set_flashdata('success','Registro atualizado com sucesso!');
            }
      
		$dadosView['comissao'] = $this->Indicacao_model->consultaComissao();
		$dadosView['meio'] = 'indicacao/parametro';
		$this->load->view('tema/layout',$dadosView);			
	}

	public function tranformarSaldo()
	{
	
	    $id = $this->session->userdata('usuario_id');
	    $bonus = $this->session->userdata('bonus');

		if($bonus == '0,00'){
			$this->session->set_flashdata('erro','Saldo insuficientes!');
			redirect('indicacao','refresh'); 
		}

		$consultarExistePix = $this->Indicacao_model->consultarExistePix($id);

		if($consultarExistePix[0]->total > 0){
			$this->session->set_flashdata('warning','Existe um bônus em análise aguarde a resolução do mesmo ou entre em contato com o Suporte!');
			redirect('indicacao','refresh'); 
		}
	              
	    $tranformar = $this->Indicacao_model->tranformarSaldo($bonus,$id);	

		if($tranformar){
			$this->session->set_flashdata('success','Saldo adicionado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Algo aconteceu entre em contato com Suporte!');
		}

		redirect('indicacao','refresh'); 
		 
	}

	public function tranformarSaldoPix()
	{
	
	    $id = $this->session->userdata('usuario_id');
	    $bonus = $this->session->userdata('bonus');
		$pix = $this->input->post('modal_valor');

		if($bonus == '0,00'){
			$this->session->set_flashdata('erro','Saldo insuficientes!');
			redirect('indicacao','refresh'); 
		}

		$consultarExistePix = $this->Indicacao_model->consultarExistePix($id);
		

		if($consultarExistePix[0]->total > 0){
			$this->session->set_flashdata('warning','Existe um bônus em análise aguarde a resolução do mesmo ou entre em contato com o Suporte!');
			redirect('indicacao','refresh'); 
		}
	              
	    $tranformar = $this->Indicacao_model->tranformarSaldoPix($bonus,$id, $pix);	

		if($tranformar){
			$this->session->set_flashdata('success','Saldo enviado para análise com sucesso! Em até 10 dias úteis você receberá a confirmação da sua solicitação.');
		}else{
			$this->session->set_flashdata('erro','Algo aconteceu entre em contato com Suporte!');
		}

		redirect('indicacao','refresh'); 
		 
	}

	public function gestaopix()
	{
		$dadosView['dados'] = $this->Indicacao_model->buscarDadosPix(); 
		$dadosView['meio'] = 'indicacao/gestao_pix';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function InvalidarPix()
	{  
	    $id = $this->uri->segment(3);
		$idUsuario = $this->uri->segment(4);
	    $idUsuarioadmin = $this->session->userdata('usuario_id');	
	              
	    $resultado = $this->Indicacao_model->InvalidarPix($idUsuario, $idUsuarioadmin, $id);	

		if($resultado){
			$this->enviarEmailPixInvalidar($idUsuario);
			$this->session->set_flashdata('success','Status alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao alterar os dados!');
		}

		redirect('indicacao/gestaopix','refresh');  
	}

	public function enviarEmailPixInvalidar($id)
	 {

	 	    $dados = $this->Indicacao_model->buscarDadadosCliente($id);

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to($dados[0]->usuario_email); //turbinagran@gmail.com $dados[0]->usuario_email

	        $this->email->subject("Pix inválido TurbinaGran");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá ".$dados[0]->usuario_nome.",</span>
					                            <span style='display:block'>Informamos que o Pix solicitado por você foi considerado inválido por nossos analistas.</span>
					                            <span style='display:block'>Solicitamos o envio de um novo Pix.</span>

					                        </p><br>
					                         <p>
					                            <span style='display:block'>Em caso de dúvidas, responda a este email.</span>
					                        </p>
					                    </td>
					                </tr>

					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

            $this->email->send();


	 }


	 public function ValidarPix()
	 {  
		 $id = $this->uri->segment(3);
		 $idUsuario = $this->uri->segment(4);
		 $idUsuarioadmin = $this->session->userdata('usuario_id');	
				   
		 $resultado = $this->Indicacao_model->ValidarPix($idUsuario, $idUsuarioadmin, $id);	
 
		 if($resultado){
			 $this->enviarEmailPixValidar($idUsuario);
			 $this->session->set_flashdata('success','Status alterado com sucesso!');
		 }else{
			 $this->session->set_flashdata('erro','Erro ao alterar os dados!');
		 }
 
		 redirect('indicacao/gestaopix','refresh');  
	 }

	 public function enviarEmailPixValidar($id)
	 {

	 	    $dados = $this->Indicacao_model->buscarDadadosCliente($id);

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to($dados[0]->usuario_email); //turbinagran@gmail.com $dados[0]->usuario_email

	        $this->email->subject("Pix enviado TurbinaGran");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá ".$dados[0]->usuario_nome.",</span>
												<span style='display:block'>Enviamos essa mensagem para notifica-lo de que foi enviado o valor do bônus para o pix informado.</span>

					                        </p><br>
					                         <p>
					                            <span style='display:block'>Em caso de dúvidas, responda a este email.</span>
					                        </p>
					                    </td>
					                </tr>

					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

            $this->email->send();


	 }



}