<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Servico_model');
		$this->load->library('session');
		
	}


    public function index()
	{
		
		$dados =  $this->Servico_model->buscarOdensCron();

		foreach ($dados as $value) {

			if ($value->ordem_quantidade > 0) {

				switch ($value->ordem_quantidade) {
					case ($value->ordem_quantidade > 0 and $value->ordem_quantidade <= 1000):

					    $min = $this->calcula_minutos($value->ordem_data, date('Y-m-d H:i:s'));

					    if ($min >= 1560) {

					    	 $dados = array(                                           
					            'ordem_status' => 'CONCLUIDA',                                  
					         );
					         $id = $value->ordem_id;
					         $this->Servico_model->ajustarOrdem($dados, $id); 
										    	
					    }
						break;

					case ($value->ordem_quantidade >= 1001 and $value->ordem_quantidade <= 2000):

						$min = $this->calcula_minutos($value->ordem_data, date('Y-m-d H:i:s'));
						
						if ($min >= 2160) {

					    	 $dados = array(                                           
					            'ordem_status' => 'CONCLUIDA',                                  
					         );
					         $id = $value->ordem_id;
					         $this->Servico_model->ajustarOrdem($dados, $id); 
										    	
					    }
						break;

					case ($value->ordem_quantidade >= 2001 and $value->ordem_quantidade <= 4000):

						$min = $this->calcula_minutos($value->ordem_data, date('Y-m-d H:i:s'));

						if ($min >= 2880) {

					    	 $dados = array(                                           
					            'ordem_status' => 'CONCLUIDA',                                  
					         );
					         $id = $value->ordem_id;
					         $this->Servico_model->ajustarOrdem($dados, $id); 
										    	
					    }
						break;

					case ($value->ordem_quantidade >= 4001 and $value->ordem_quantidade <= 7000):
						
						$min = $this->calcula_minutos($value->ordem_data, date('Y-m-d H:i:s'));
						
						if ($min >= 4320) {

					    	 $dados = array(                                           
					            'ordem_status' => 'CONCLUIDA',                                  
					         );
					         $id = $value->ordem_id;
					         $this->Servico_model->ajustarOrdem($dados, $id); 
										    	
					    }
						break;

					case ($value->ordem_quantidade >= 7001 and $value->ordem_quantidade <= 10000):
						
						$min = $this->calcula_minutos($value->ordem_data, date('Y-m-d H:i:s'));
						
						if ($min >= 6000) {

					    	 $dados = array(                                           
					            'ordem_status' => 'CONCLUIDA',                                  
					         );
					         $id = $value->ordem_id;
					         $this->Servico_model->ajustarOrdem($dados, $id); 
										    	
					    }
						break;

					default:
						echo "error";
						break;
				}

	
			} 
			
			
		}

		
	}


    public function calcula_minutos($tempo_inicio, $tempo_fim) {
	     $minutos = 0;
	     $horas = 0;

	     $dateStart = new \DateTime($tempo_inicio);
	     $dateNow = new \DateTime($tempo_fim);
	     $dateDiff = $dateStart->diff($dateNow);
	  
	     $minutos = $dateDiff->i + (($dateDiff->h + ($dateDiff->days * 24)) * 60);
	     if ($minutos == 0) {
	        $minutos++; // incluo 1 minuto caso seja alguns segundos o comparativo
	     }
	     return $minutos;
    }

}