<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perguntas extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');
	}

	public function index()
	{

		$dadosView['meio'] = 'pergunta/pergunta';
		$this->load->view('tema/layout',$dadosView);	
		
	}

}