<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Register_model');
		$this->load->library('session');
		$this->load->library('email');
	}

	/**
	 * [index description]Redireciona para o login do sistema, este é o controller default e metodo padrão
	 * @return [type] [description]
	 */


	public function registerUsuario()
	{

    	if ($this->Register_model->validarUsuario($this->input->post('email')) == true) {

			$this->session->set_flashdata('erro','Esse Email ja foi cadastrado!');
			$this->load->view('register/registerErro');
        }else{
    	
	    	$dados = array(        		 				 
				  'usuario_nome'            => $this->input->post('nome'),
				  'usuario_email'           => $this->input->post('email'),	
				  'usuario_perfil' 		    => $this->input->post('perfil'),
				  'usuario_whatsapp' 		=> $this->input->post('whatsapp'),
				  'usuario_instagram' 		=> $this->input->post('instagram'),
				  'usuario_facebook' 		=> $this->input->post('facebook'),
				  'usuario_senha'           => sha1(md5(strtolower($this->input->post('password')))),		
				  'usuario_perfil' 		    => empty($this->input->post('perfil')) ? '3': $this->input->post('perfil'),
				  'usuario_indicacao'       => $this->input->post('codigo')

	    	);

	    	$email = base64_encode($this->input->post('email'));
	    	$senha = sha1(md5(strtolower($this->input->post('password'))));
	        $perfil = empty($this->input->post('perfil')) ? '3': $this->input->post('perfil');

	    	$resultado = $this->Register_model->adicionar($dados);

	    	if($resultado){
	    		$dados['email'] = $email;
	    		$dados['senha'] = $senha;
	    		$dados['perfil'] = $perfil;
	    		$this->load->view('register/registerConfirmation', $dados);
	    	}else{
	    		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
	    		$this->load->view('register/registerErro');
	    	}

       }
        
       
	}


   public function recuperarSenha()
	{

    	if ($this->Register_model->validarUsuario($this->input->post('email')) == true) {


    		$dados = $this->Register_model->buscarDadados($this->input->post('email'));

    		$token = base64_encode(date('dmY'));

    		$email = base64_encode($this->input->post('email'));

	        $this->email->set_newline("\r\n");
	        $this->email->from('contato@turbinagran.com.br', 'TurbinaGran');
	        $this->email->to($this->input->post('email')); 

	        $this->email->subject("Recuperar Senha");

	        $this->email->message("


	        	<center>
					<table style='max-width:620px;border-collapse:collapse;margin:0 auto 0 auto' width='620' cellspacing='0' cellpadding='0' border='0'>
					    <tbody>
					    <tr>
					        <td>
					            <table style='max-width:580px;padding-left:20px;padding-right:20px' width='580' cellspacing='0' cellpadding='0' border='0'>
					                <tbody>

					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p>
					                            <span style='display:block'>Olá ".$dados[0]->usuario_nome.",</span>
					                            <span style='display:block'>Por favor, clique no link a seguir para redefinir sua senha:</span>
					                        </p>
					                    </td>
					                </tr>
					                <tr style='background:#ffffff'>
					                    <td style='padding-top:10px' align='center'>
					                        <a href='".base_url()."welcome/newPassword/".$email."/".$token."' style='color:#000000;text-decoration:none' target='_blank'>
					                            <div style='border:1px solid #B92786;background-color:#B92786;width:300px;height:60px;border-radius:6px' align='center'>
					                                <table align='center'>
					                                    <tbody><tr>
					                                        <td style='color:#ffffff;font-size:18px;letter-spacing:2.25px;padding-top:18px;font-weight:bold;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' valign='middle' align='center'>
					                                            RECUPERAR SENHA 
					                                        </td>
					                                    </tr>
					                                </tbody></table>
					                            </div>
					                        </a>
					                    </td>
					                </tr>
					                <tr style='background:#ffffff'>
					                    <td border='0' style='padding-left:20px;padding-right:20px;padding-top:29.8px;font-size:22px;color:#000000;letter-spacing:0;line-height:37px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='left'>
					                        <p style='margin-bottom:0px'>Se você não requisitou a redefinição da senha, é possível que esta Conta TurbinaGran tenha sido acessada por alguém sem autorização para fazê-lo. Se você perceber problemas na Conta TurbinaGran entra em contato com nosso suporte.</p>
					                    </td>
					                </tr>
					                <tr>
					                    <td align='left'>
					                        <img style='background:#f6f6f6 ; width: auto; height: auto; border-radius: 20px;' src='http://turbinagran.com.br/assets/dist/img/topo_email.png' class='image rocket' alt=' />
					                    </td>
					                </tr>
					                <tr>
					                    <td style='margin-top:0;margin-bottom:0;color:#262626;font-size:17px;padding-top:30px;padding-bottom:70px;letter-spacing:0;line-height:35px;font-family:AvenirNext-Regular,Droid Sans monospace,Roboto,Arial,sans-serif' align='center'><center>
					                        <p>
					                            <span style='display:block'>© TurbinaGran Inc. 2020 &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' style='text-decoration:none;color:#0eadff' target='_blank'>Termos de Serviço</a></span>
					                            <span style='display:block'>contato@turbinagran.com.br</span>
					                        </p></center>
					                    </td>
					                </tr>
					                </tbody>
					            </table>
					        </td>
					    </tr>
					    </tbody>
					</table>
					</center>



	        ");

	        if($this->email->send()) {
	            $this->session->set_flashdata('success','Um link para recupeção de senha foi enviado para o seu Email!');
			    $this->load->view('register/registerConf');   
	        } else {
	            $this->session->set_flashdata('erro','Ocorreu um erro tente novamente!');
			    $this->load->view('register/registerErro');
	        }  

        }else{
    	

			$this->session->set_flashdata('erro','Email não cadastrado!');
			$this->load->view('register/registerErro');
	    	

       }
        
       
	}



   public function recuperarSenhaNova()
	{

    	if ($this->Register_model->validarUsuario(base64_decode($this->input->post('tipo'))) == true) {				 

		    $senha = sha1(md5(strtolower($this->input->post('password'))));		

	    	$resultado = $this->Register_model->alterar($senha, base64_decode($this->input->post('tipo')));

	    	$dadosP = $this->Register_model->buscarDadados(base64_decode($this->input->post('tipo')));


	        if($resultado){

	            $dados['email'] = base64_encode($dadosP[0]->usuario_email);
	    		$dados['senha'] = $dadosP[0]->usuario_senha;
	    		$dados['perfil'] = $dadosP[0]->usuario_perfil;
	    		$this->load->view('register/registerConfirmation', $dados);
	    	}else{
	    		$this->session->set_flashdata('erro','Ocorreu um erro tente novamente');
	    		$this->load->view('register/registerErro');
	    	}


        }else{
    	
	    	$this->session->set_flashdata('erro','Ocorreu um erro tente novamente!');
	        $this->load->view('register/registerErro');

       }
        
       
	}




}