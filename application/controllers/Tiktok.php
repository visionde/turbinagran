<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiktok extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Servico_model');
		$this->load->model('Saldo_model');
		$this->load->model('Parametros_model');
		$this->load->model('Ordem_model');
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

		$saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));
		$sessao = array('saldo'          => str_replace('.', ',', $saldo[0]->saldo_valor));
		$this->session->set_userdata($sessao);	
	}


	public function index()
	{
        $dadosView['tiktok'] = $this->Servico_model->buscarDados('3');
		$dadosView['meio'] = 'tiktok/tiktok';
		$this->load->view('tema/layout',$dadosView);		
	}

    public function selecionarServicoTable()
	{

		$id = $this->input->post('servico');

        $dados = $this->Servico_model->selecionarServico($id);



        if (isset($dados[0])) {


        	$preco = $this->session->userdata('perfil_id') == '2' ? $dados[0]->servico_preco_rev : $dados[0]->servico_preco;
        	
	      	echo "<tr>
	              <td><strong>Link:</strong><br>Clique em enviar apenas 1 (uma vez)</td>
	            <tr>
	              <td><strong>Minimo:</strong><br>".number_format($dados[0]->servico_minimo, 0, ' ', '.')."</td>
	            </tr>
	            <tr>
	              <td><strong>Máximo:</strong><br>".number_format($dados[0]->servico_maximo, 0, ' ', '.')."</td>
	            </tr>";

	          if ($this->session->userdata('perfil_id') == '1') {
	             
	           echo " <tr>
	              <td><strong>Valor p/ 1000 - Cliente:</strong><br><span class='spanValor'>".str_replace('.', ',', $dados[0]->servico_preco)."</span></td>
	            </tr>
	            <tr>
	              <td><strong>Valor p/ 1000 - Revendedor:</strong><br><span class='spanValor'>".str_replace('.', ',', $dados[0]->servico_preco_rev)."</span></td>
	            </tr>";
	          }else{

	          echo "<tr>
	              <td><strong>Valor p/ 1000:</strong><br><span class='spanValor'>". str_replace('.', ',', $preco)."</span></td>
	            </tr>";

	          }

	         echo "tr>
                  <td><strong>Observações:</strong><br>".$dados[0]->servico_observacao."</td>
                 </tr>";
        } else {
        	
	      	echo "<tr>
	              <td><strong>Link:</strong><br>Clique em enviar apenas 1 (uma vez)</td>
	            <tr>
	              <td><strong>Minimo:</strong><br></td>
	            </tr>
	            <tr>
	              <td><strong>Máximo:</strong><br></td>
	            </tr>
	            <tr>
	              <td><strong>Valor p/ 1000:</strong><br><span class='spanValor'></span></td>
	            </tr>
	            <tr>
                  <td><strong>Observações:</strong><br></td>
                </tr>";
        }

	}

   public function selecionarServicoMini()
	{

		$id   = $this->input->post('servico');

        $dados = $this->Servico_model->selecionarServico($id);


        if (isset($dados[0])) {
        	
	      echo " <span class='spin minimo'>mínimo ".number_format($dados[0]->servico_minimo, 0, ' ', '.')."</span>
                 <span class='spin maximo'>máxmio ".number_format($dados[0]->servico_maximo, 0, ' ', '.')."</span>";
        } else {
        	
	      echo " <span class='spin minimo'>mínimo</span>
                 <span class='spin maximo'>máxmio</span>";
        }
        
	}

    public function ordemServico()
	{

		$saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));
		$dados = $this->Servico_model->selecionarServico($this->input->post('servicotik'));
		$preco = $this->session->userdata('perfil_id') == '2' ? $dados[0]->servico_preco_rev : $dados[0]->servico_preco;
		

		$precoReal = $preco / 1000;
		$retirada = $this->input->post('quantidade') * $precoReal;
		$calculoFinal =  $saldo[0]->saldo_valor - $retirada;


		if ($calculoFinal < 0) {

			$this->session->set_flashdata('erro','Saldo Insufiente!');
         	redirect('tiktok');
			
		} else {
			

			if (isset($dados[0])) {

			    $this->form_validation->set_rules('quantidade', '...', 'callback_maximo['.$dados[0]->servico_maximo.']|callback_minimo['.$dados[0]->servico_minimo.']');

		    		if($this->form_validation->run() == FALSE)
					{
			        	$this->session->set_flashdata('erro',validation_errors());
			        	redirect('tiktok');

			        } else {

			        	if ($dados[0]->parametro_id == '2') {

			        		$saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));

			        		$preco = $this->session->userdata('perfil_id') == '2' ? $dados[0]->servico_preco_rev : $dados[0]->servico_preco;

			        		$precoReal = $preco / 1000;
							$retirada = $this->input->post('quantidade') * $precoReal;
							$calculoFinal =  $saldo[0]->saldo_valor - $retirada;

			        		if ($calculoFinal < 0) {

								$this->session->set_flashdata('erro','Saldo Insufiente!');
					         	redirect('tiktok');
								
							} 
			        		                        
				        	$parametro = $this->Parametros_model->dadadosParametros($dados[0]->parametro_id);
				            $api = $this->apitubo($parametro[0]->parametro_usuario, $parametro[0]->parametro_token, $dados[0]->externo_id, $this->input->post('quantidade'), $this->input->post('idtik'));

				            if ($api) {
		             	    	$dados = array(
		             	    	      'usuario_id'          => $this->session->userdata('usuario_id'),        		 				 
									  'servico_id'          => $dados[0]->servico_id,
									  'parametro_id'        => $dados[0]->parametro_id,
									  'ordem_quantidade'    => $this->input->post('quantidade'),	
									  'ordem_valor'         => $retirada,		
									  'redes_id' 		    => $dados[0]->redes_id,
									  'ordem_link'          => $this->input->post('idtik'),	
									  'ordem_status'        => 'PENDENTE',		
									  'ordem_externo_id' 	=> $api['id_pedido'],
						    	);

						    	$this->Ordem_model->adicionarOrdem($dados);
						    	$this->Saldo_model->retiradaSaldo($calculoFinal, $this->session->userdata('usuario_id'));
						    	$this->session->set_flashdata('success','Ordem adicionada com sucesso!');
						    	redirect('tiktok');

				            }else{
				            	redirect('tiktok');				            	
				            }

			        	}elseif ($dados[0]->parametro_id == '4') {

			                $saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));

			        		$preco = $this->session->userdata('perfil_id') == '2' ? $dados[0]->servico_preco_rev : $dados[0]->servico_preco;

			        		$precoReal = $preco / 1000;
							$retirada = $this->input->post('quantidade') * $precoReal;
							$calculoFinal =  $saldo[0]->saldo_valor - $retirada;

			        		if ($calculoFinal < 0) {

								$this->session->set_flashdata('erro','Saldo Insufiente!');
					         	redirect('tiktok');
								
							}

							$parametro = $this->Parametros_model->dadadosParametros($dados[0]->parametro_id);
				            $api = $this->apiUpredes($parametro[0]->parametro_url, $parametro[0]->parametro_token, $dados[0]->externo_id, $this->input->post('idtik'), $this->input->post('quantidade'));

				            if ($api) {
		             	    	$dados = array(
		             	    	      'usuario_id'          => $this->session->userdata('usuario_id'),        		 				 
									  'servico_id'          => $dados[0]->servico_id,
									  'parametro_id'        => $dados[0]->parametro_id,
									  'ordem_quantidade'    => $this->input->post('quantidade'),	
									  'ordem_valor'         => $retirada,		
									  'redes_id' 		    => $dados[0]->redes_id,
									  'ordem_link'          => $this->input->post('idtik'),	
									  'ordem_status'        => 'PENDENTE',		
									  'ordem_externo_id' 	=> $api->order,
						    	);

						    	$this->Ordem_model->adicionarOrdem($dados);
						    	$this->Saldo_model->retiradaSaldo($calculoFinal, $this->session->userdata('usuario_id'));
						    	$this->session->set_flashdata('success','Ordem adicionada com sucesso!');
						    	redirect('tiktok');

				            }else{
				            	redirect('tiktok');				            	
				            }

				        }elseif ($dados[0]->parametro_id == '5') {

				        	$saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));

			        		$preco = $this->session->userdata('perfil_id') == '2' ? $dados[0]->servico_preco_rev : $dados[0]->servico_preco;

			        		$precoReal = $preco / 1000;
							$retirada = $this->input->post('quantidade') * $precoReal;
							$calculoFinal =  $saldo[0]->saldo_valor - $retirada;

			        		if ($calculoFinal < 0) {

								$this->session->set_flashdata('erro','Saldo Insufiente!');
					         	redirect('tiktok');
								
							}

							$parametro = $this->Parametros_model->dadadosParametros($dados[0]->parametro_id);
				            $api = $this->apiImpulsioneme($parametro[0]->parametro_url, $parametro[0]->parametro_token, $dados[0]->externo_id, $this->input->post('idtik'), $this->input->post('quantidade'));

				            if ($api) {
		             	    	$dados = array(
		             	    	      'usuario_id'          => $this->session->userdata('usuario_id'),        		 				 
									  'servico_id'          => $dados[0]->servico_id,
									  'parametro_id'        => $dados[0]->parametro_id,
									  'ordem_quantidade'    => $this->input->post('quantidade'),	
									  'ordem_valor'         => $retirada,		
									  'redes_id' 		    => $dados[0]->redes_id,
									  'ordem_link'          => $this->input->post('idtik'),	
									  'ordem_status'        => 'PENDENTE',		
									  'ordem_externo_id' 	=> $api->order,
						    	);

						    	$this->Ordem_model->adicionarOrdem($dados);
						    	$this->Saldo_model->retiradaSaldo($calculoFinal, $this->session->userdata('usuario_id'));
						    	$this->session->set_flashdata('success','Ordem adicionada com sucesso!');
						    	redirect('tiktok');

				            }else{
				            	redirect('tiktok');				            	
				            }
				            
			        	} else {


			                if ($calculoFinal < 0) {

								$this->session->set_flashdata('erro','Saldo Insufiente!');
					         	redirect('tiktok');
								
							} 

			        		$result = $this->getFbId($this->input->post('idtik'));

			        		if ($result) {

		        			    $dados = array(
		        			          'usuario_id'          => $this->session->userdata('usuario_id'),        		 				 
									  'servico_id'          => $dados[0]->servico_id,
									  'parametro_id'        => $dados[0]->parametro_id,	
									  'ordem_quantidade'    => $this->input->post('quantidade'),
									  'ordem_valor'         => $retirada,		
									  'redes_id' 		    => $dados[0]->redes_id,
									  'ordem_link'          => $this->input->post('idtik'),	
									  'ordem_status'        => 'PENDENTE'
						    	);

						    	$this->Ordem_model->adicionarOrdem($dados);
						    	$this->Saldo_model->retiradaSaldo($calculoFinal, $this->session->userdata('usuario_id'));
						    	$this->session->set_flashdata('success','Ordem adicionada com sucesso!');
						    	redirect('tiktok');

			        		} else {

			        			$this->session->set_flashdata('erro','Link inválido, tente novamente!');
	         	                redirect('tiktok');
			        		}			        		
			        		    
			        	}
			        	
			        }

	        }else{

	         	$this->session->set_flashdata('erro','Ocorreu um erro, tente novamente!');
	         	redirect('tiktok');
	        }

		}
		
	}


   public function maximo($str, $max)
	{
	    if ( ! is_numeric($str))
	    {
	        return FALSE;
	    }
	    $this->form_validation->set_message('maximo', 'Limite de quantidade máxima ultrapassado.');
	    return $str <= $max;
	}

    public function minimo($str, $min)
	{
	    if ( ! is_numeric($str))
	    {
	        return FALSE;
	    }
	    $this->form_validation->set_message('minimo', 'Limite de quantidade mínimo ultrapassado.');
	    return $str >= $min;
	}


	private function apitubo($usuario, $token, $produto, $quantidade, $url) {

        ini_set("memory_limit", "-1");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.turbosocial.com.br/revenda/api.php?tipo=REALIZAR_PEDIDO&id_produto='.$produto.'&quantidade='.$quantidade.'&url='.$url.'&id_usuario='.$usuario.'&api_token='.$token.'' ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTREDIR => 3,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_REFERER => "{$_SERVER['SERVER_NAME']}"
            )
        );

        $response = json_decode(curl_exec($curl));
        $err      = curl_error($curl);  
        curl_close($curl);

        $response = get_object_vars($response);

		// $key = array_search('erro', $response);
		// if($key!==false){
		//     unset($response[$key]);
		// }

        if (isset($response['status'])) {

	        if ($response['status'] == 'erro') {

	        	if ($response['codigo'] == 'URL_INCORRETA') {
	        		$this->session->set_flashdata('erro','Erro TB !!! ('.$response['mensagem'].')');
	        		return false;
	        	} else {
	        	    $this->session->set_flashdata('erro','Erro TB !!! ('.$response['codigo'].'). Favor contatar o Suporte! ');
				    return false;
	        	}
	
	        } else {
	        	return $response;
	        }

        } else {
        	$this->session->set_flashdata('erro','Erro TB !!! (Falha na conexão). Tente Novamente Caso Persistir, Favor contatar o Suporte! ');
			return false;
        }
        
    }


    private function apiUpredes($url, $api, $produto, $link, $quantidade) {

    	$params=['key'=>$api, 'action'=>'add', 'service'=>$produto, 'link'=>$link, 'quantity'=>$quantidade];

        set_time_limit(0);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => ''.$url.'' ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTREDIR => 3,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_REFERER => "{$_SERVER['SERVER_NAME']}"
            )
        );

        $response = json_decode(curl_exec($curl));
        $err      = curl_error($curl);  
        curl_close($curl);

        if (isset($response->erro)) {
        	$this->session->set_flashdata('erro','Erro UP !!! ('.$response->menssagem.'). Favor contatar o Suporte! ');
			return false;
        } else {
        	return $response;
        }

    }


    private function apiImpulsioneme($url, $api, $produto, $link, $quantidade) {

		$this->load->library('GoogleTranslate');

		$source = 'en' ;
		$target = 'pt' ;

    	$params=['key'=>$api, 'action'=>'add', 'service'=>$produto, 'link'=>$link, 'quantity'=>$quantidade];

        set_time_limit(0);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => ''.$url.'' ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTREDIR => 3,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_REFERER => "{$_SERVER['SERVER_NAME']}"
            )
        );

        $response = json_decode(curl_exec($curl));
        $err      = curl_error($curl);  
        curl_close($curl);

        if (isset($response->error)) {
        	$this->session->set_flashdata('erro','Erro IM !!! ('.$this->googletranslate->translate($source, $target , $response->error).'). Favor contatar o Suporte! ');
			return false;
        } else {
        	return $response;
        }

    }



    function getFbId($url){
	    $fbId = false;
	    $info = parse_url($url);

	    if( isset($info['host']) && preg_match('/tiktok\.com/', $info['host']) ){
	        if(isset($info['query'])){
	            $qs = parse_str($info['query'], $params);
	            if( isset($params['id']) ){
	                //pega o id aqui $params['id']
	                $fbId = $params['id'];
	            }
	        }else if( isset($info['path']) ){
	            $path = str_replace('/', '', $info['path']);
	            //precisa ajustar essa expressao, não lembro exatamente se isso tudo é permitido ou falta algo
	            if(preg_match('/\.php/', $path)==0){
	                if( preg_match('/([a-zA-Z0-9]|\.|_)+/', $path)>0 ){
	                    $fbId = $path;
	                }
	            }
	        }
	    }
	    return $fbId;
	}





}
