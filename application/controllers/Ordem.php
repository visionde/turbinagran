<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordem extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Ordem_model');
		$this->load->model('Parametros_model');
        $this->load->model('Saldo_model');
		$this->load->library('session');
		if($this->session->userdata('usuario_id')==null) redirect('welcome');

        $saldo = $this->Saldo_model->consultaSaldo($this->session->userdata('usuario_id'));
        $sessao = array('saldo'          => str_replace('.', ',', $saldo[0]->saldo_valor));
        $this->session->set_userdata($sessao);
	}

	public function index()
	{
		$this->atualizarBanco();
		$dadosView['total'] = $this->Ordem_model->buscarDadosTotal($this->session->userdata('usuario_id'));
		$dadosView['pendentes'] = $this->Ordem_model->buscarDadosPendentes($this->session->userdata('usuario_id'));
		$dadosView['concluidas'] = $this->Ordem_model->buscarDadosConcluidas($this->session->userdata('usuario_id'));
		$dadosView['meio'] = 'ordem/ordem';
		$this->load->view('tema/layout',$dadosView);	
		
	}

    public function saldos()
    {
        $dadosView['creditos'] = $this->Ordem_model->buscarDadosCreditos($this->session->userdata('usuario_id'));
        $dadosView['debitos'] = $this->Ordem_model->buscarDadosDebitos($this->session->userdata('usuario_id'));
        $dadosView['meio'] = 'ordem/saldos';
        $this->load->view('tema/layout',$dadosView);    
        
    }


	public function atualizarBanco(){

		$dados = $this->Ordem_model->buscarDadosTotalAtualizar($this->session->userdata('usuario_id'));

		foreach ($dados as $k) {

			if ($k->parametros == '2') {
				$this->apitubo($k->ordem_externo_id);
			}elseif ($k->parametros == '4') {
                $this->apiUpredes($k->ordem_externo_id);
            }elseif ($k->parametros == '5') {
                $this->apiImpulsioneme($k->ordem_externo_id);
				
			}
			
		}
	}


	private function apitubo($id_externo) {

		$parametro = $this->Parametros_model->dadadosParametros('2');

        set_time_limit(0);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.turbosocial.com.br/revenda/api.php?tipo=STATUS_PEDIDO&id_pedido='.$id_externo.'&id_usuario='.$this->session->userdata('usuario_id').'&api_token='.$parametro[0]->parametro_token.'' ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTREDIR => 3,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_REFERER => "{$_SERVER['SERVER_NAME']}"
            )
        );

        $response = json_decode(curl_exec($curl));
        $err      = curl_error($curl);  
        curl_close($curl);


        if ($response->status == 'Completed') {

        	$this->Ordem_model->atualizarOrdem($status = 'CONCLUIDA', $id_externo);

        }elseif ($response->status == 'Refunded') {

            $result = $this->Ordem_model->consultaOrdemRefund($id_externo);
            $this->Ordem_model->atualizarOrdemRefund($status = 'REEMBOLSO', $id_externo);
            $this->Ordem_model->AtualizarRefund($result[0]->ordem_valor, $result[0]->usuario_id);
           
        } 

    }


    private function apiUpredes($id_externo) {

		$parametro = $this->Parametros_model->dadadosParametros('4');

    	$params=['key'=>$parametro[0]->parametro_token, 'action'=>'status', 'order'=>$id_externo];

        set_time_limit(0);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => ''.$parametro[0]->parametro_url.'' ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTREDIR => 3,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_REFERER => "{$_SERVER['SERVER_NAME']}"
            )
        );

        $response = json_decode(curl_exec($curl));
        $err      = curl_error($curl);  
        curl_close($curl);

        if (isset($response->status)) {
            if ($response->status == 'Concluido') {

                $this->Ordem_model->atualizarOrdem($status = 'CONCLUIDA', $id_externo);
            } 
        } 

    }


    private function apiImpulsioneme($id_externo) {

        $parametro = $this->Parametros_model->dadadosParametros('5');

        $params=['key'=>$parametro[0]->parametro_token, 'action'=>'status', 'order'=>$id_externo];

        set_time_limit(0);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => ''.$parametro[0]->parametro_url.'' ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTREDIR => 3,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_REFERER => "{$_SERVER['SERVER_NAME']}"
            )
        );

        $response = json_decode(curl_exec($curl));
        $err      = curl_error($curl);  
        curl_close($curl);

        if ($response->status == 'Completed') {

            $this->Ordem_model->atualizarOrdem($status = 'CONCLUIDA', $id_externo);
        } 

    }

}