<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio_model extends CI_Model {


######## RELATORIO FINANCEIRO ########	


 public function financeiroListar($page, $itens_per_page, $status, $pagamento)
	{
		$this->db->select("*");
		if ($status != '1' and $status != NULL) {
		  $this->db->where('historico_saldo_status', $status);
		}
		if ($pagamento != '1'  and $pagamento != NULL) {
			if ($pagamento == '2') {
				$this->db->where('`historico_saldo`.`comprovante_id` is NOT NULL', NULL, FALSE);
			} elseif($pagamento == '4'){
				$this->db->where('`historico_saldo`.`historico_saldo_obsevacao` ', 'Saldo adicionado via Bônus');
			} else {
				$this->db->where('`historico_saldo`.`mercadopago_id` is NOT NULL', NULL, FALSE);
			}
		}
		$this->db->join('usuario','usuario.usuario_id = historico_saldo.usuario_id ');
		$this->db->join('comprovante','comprovante.comprovante_id = historico_saldo.comprovante_id ', 'left');
		$this->db->limit($itens_per_page, (($page - 1) * $itens_per_page));
	    return $this->db->get('historico_saldo')->result();
	}


 public function financeiroContador($page, $status, $pagamento)
	{
		$this->db->select("COUNT(`historico_saldo`.`historico_saldo_id`) AS total");
		if ($status != '1' and $status != NULL) {
		  $this->db->where('historico_saldo_status', $status);
		}
		if ($pagamento != '1'  and $pagamento != NULL) {
			if ($pagamento == '2') {
				$this->db->where('`historico_saldo`.`comprovante_id` is NOT NULL', NULL, FALSE);
			} elseif($pagamento == '4'){
				$this->db->where('`historico_saldo`.`historico_saldo_obsevacao` ', 'Saldo adicionado via Bônus');
			} else {
				$this->db->where('`historico_saldo`.`mercadopago_id` is NOT NULL', NULL, FALSE);
			}
		}
	  
		$this->db->join('usuario','usuario.usuario_id = historico_saldo.usuario_id ');
		$this->db->join('comprovante','comprovante.comprovante_id = historico_saldo.comprovante_id ', 'left');
	    return $this->db->get('historico_saldo')->result();
	}


 public function financeiroExcel($status, $pagamento)
	{
		$this->db->select("*");
		if ($status != '1' and $status != NULL) {
		  $this->db->where('historico_saldo_status', $status);
		}
		if ($pagamento != '1'  and $pagamento != NULL) {
			if ($pagamento == '2') {
				$this->db->where('`historico_saldo`.`comprovante_id` is NOT NULL', NULL, FALSE);
			} elseif($pagamento == '4'){
				$this->db->where('`historico_saldo`.`historico_saldo_obsevacao` ', 'Saldo adicionado via Bônus');
			} else {
				$this->db->where('`historico_saldo`.`mercadopago_id` is NOT NULL', NULL, FALSE);
			}
		}
		$this->db->join('usuario','usuario.usuario_id = historico_saldo.usuario_id ');
	    return $this->db->get('historico_saldo')->result();
	}

######## FIM RELATORIO FINANCEIRO ########	

######## RELATORIO USUARIO ########	

 public function usuarioListar($page, $itens_per_page, $status)
	{
		$this->db->select("*");
		if ($status != '1' and $status != NULL) {
		  if ($status == 'ZERADO') {
		  	 $this->db->where('saldo_valor', '0.00');
		  } else {
		  	$this->db->where('saldo_valor !=', '0.00');
		  }
		}
		$this->db->where('usuario_visivel', 1);
		$this->db->where('usuario_perfil !=', 1);
		$this->db->join('usuario','usuario.usuario_id = saldo.usuario_id ');
		$this->db->join('perfil','usuario.usuario_perfil = perfil.perfil_id ');
		$this->db->order_by('usuario_nome');
		$this->db->limit($itens_per_page, (($page - 1) * $itens_per_page));
	    return $this->db->get('saldo')->result();
	}


 public function usuarioContador($page, $status)
	{
		$this->db->select("COUNT(`usuario`.`usuario_id`) AS total");
		if ($status != '1' and $status != NULL) {
		  if ($status == 'ZERADO') {
		  	 $this->db->where('saldo_valor', '0.00');
		  } else {
		  	$this->db->where('saldo_valor !=', '0.00');
		  }
		}
		$this->db->where('usuario_visivel', 1);
		$this->db->where('usuario_perfil !=', 1);
		$this->db->join('usuario','usuario.usuario_id = saldo.usuario_id ');
		$this->db->join('perfil','usuario.usuario_perfil = perfil.perfil_id ');
		$this->db->order_by('usuario_nome');
	    return $this->db->get('saldo')->result();
	}

 public function usuarioExcel($status)
	{
		$this->db->select("*");
		if ($status != '1' and $status != NULL) {
		  if ($status == 'ZERADO') {
		  	 $this->db->where('saldo_valor', '0.00');
		  } else {
		  	$this->db->where('saldo_valor !=', '0.00');
		  }
		}
	    $this->db->where('usuario_visivel', 1);
		$this->db->where('usuario_perfil !=', 1);
		$this->db->join('usuario','usuario.usuario_id = saldo.usuario_id ');
		$this->db->join('perfil','usuario.usuario_perfil = perfil.perfil_id ');
		$this->db->order_by('usuario_nome');
	    return $this->db->get('saldo')->result();
	}

######## FIM RELATORIO USUARIO ########	

######## RELATORIO ORDENS ########	

 public function ordensListar($page, $itens_per_page, $status, $usuario)
	{
		if($status != '1' and $status != NULL){
		  $this->db->where('ordem_status', $status);	
		}

		if ($usuario != NULL) {
		  $this->db->like('usuario_nome', $usuario, 'after');
		} 
		
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		$this->db->join('usuario','usuario.usuario_id = ordemservico.usuario_id ');
		$this->db->order_by('usuario_nome');
		$this->db->limit($itens_per_page, (($page - 1) * $itens_per_page));
		return $this->db->get('ordemservico')->result();

	}

  public function ordensContador($page, $status, $usuario)
	{
		$this->db->select("COUNT(`usuario`.`usuario_id`) AS total");
		if($status != '1' and $status != NULL){
		  $this->db->where('ordem_status', $status);	
		}

		if ($usuario != NULL) {
		  $this->db->like('usuario_nome', $usuario, 'after');
		} 
		
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		$this->db->join('usuario','usuario.usuario_id = ordemservico.usuario_id ');
		$this->db->order_by('usuario_nome');
	    return $this->db->get('ordemservico')->result();
	}

  public function ordensExcel($status, $usuario)
	{
		$this->db->select("*");
		if($status != '1' and $status != NULL){
		  $this->db->where('ordem_status', $status);	
		}

		if ($usuario != NULL) {
		  $this->db->like('usuario_nome', $usuario, 'after');
		} 
	    $this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		$this->db->join('usuario','usuario.usuario_id = ordemservico.usuario_id ');
		$this->db->order_by('usuario_nome');
	    return $this->db->get('ordemservico')->result();
	}

######## FIM RELATORIO ORDENS ########	

######## RELATORIO BONUS ########	

 public function bonusListar($page, $itens_per_page, $status)
	{
		$this->db->select("`indicacao`.`indicacao_id`,
		`indicacao`.`indicacao_data`,
		`indicacao`.`indicacao_valor`,
		`indicacao`.`indicacao_status`,
		(SELECT u.`usuario_nome` FROM usuario u WHERE u.`usuario_id` = uf.`usuario_indicacao` LIMIT 1) AS usuario_recebido,
		uf.`usuario_nome` AS usuario_indicacao");

		if($status != 'todos' and $status != NULL){
		  $this->db->where('indicacao_status', $status);	
		}
		
		$this->db->join('`usuario` uf','uf.`usuario_id = `indicacao`.`usuario_indicacao` ');
		$this->db->limit($itens_per_page, (($page - 1) * $itens_per_page));
		return $this->db->get('indicacao')->result();

	}

  public function bonusContador($page, $status)
	{
		$this->db->select("COUNT(`indicacao`.`indicacao_id`) AS total");
		if($status != 'todos' and $status != NULL){
		  $this->db->where('indicacao_status', $status);	
		}
		$this->db->join('`usuario` uf','uf.`usuario_id = `indicacao`.`usuario_indicacao` ');
		return $this->db->get('indicacao')->result();

	}

  public function bonusExcel($status)
	{
		$this->db->select("`indicacao`.`indicacao_id`,
		`indicacao`.`indicacao_data`,
		`indicacao`.`indicacao_valor`,
		`indicacao`.`indicacao_status`,
		(SELECT u.`usuario_nome` FROM usuario u WHERE u.`usuario_id` = uf.`usuario_indicacao` LIMIT 1) AS usuario_recebido,
		uf.`usuario_nome` AS usuario_indicacao");

		if($status != 'todos' and $status != NULL){
		  $this->db->where('indicacao_status', $status);	
		}
		
		$this->db->join('`usuario` uf','uf.`usuario_id = `indicacao`.`usuario_indicacao` ');
		return $this->db->get('indicacao')->result();
	}

######## FIM RELATORIO BONUS ########	

######## RELATORIO BONUS PIX ########	

 public function bonuspixListar($page, $itens_per_page, $status)
	{
		$this->db->select("`pix_id`,
		`pix_valor`,
		`pix_bonus`,
		`pix_status`,
		(SELECT u.`usuario_nome` FROM usuario u WHERE u.`usuario_id` = `pix_liberacao_usuario` LIMIT 1) AS usuario_liberado,
		uf.`usuario_nome` AS usuario_recebido");

		if($status != 'todos'){
		if ($status == 'NULL') {
			$this->db->where('`pix_status` IS NULL', NULL, FALSE);
		}else {
			$this->db->where('pix_status', $status);	
		}
		}
		
		$this->db->join('`usuario` uf','uf.usuario_id = `indicacao_pix`.`usuario_id` ');
		$this->db->limit($itens_per_page, (($page - 1) * $itens_per_page));
		return $this->db->get('indicacao_pix')->result();

	}

 public function bonuspixContador($page, $status)
	{
		$this->db->select("COUNT(`pix_id`) AS total");
		if($status != 'todos'){
			if ($status == 'NULL') {
			$this->db->where('`pix_status` IS NULL', NULL, FALSE);
		}else {
			$this->db->where('pix_status', $status);	
			}
		}
		$this->db->join('`usuario` uf','uf.usuario_id = `indicacao_pix`.`usuario_id` ');
		return $this->db->get('indicacao_pix')->result();

	}
 public function bonuspixExcel($status)
	{
		$this->db->select("`pix_id`,
		`pix_valor`,
		`pix_bonus`,
		`pix_status`,
		(SELECT u.`usuario_nome` FROM usuario u WHERE u.`usuario_id` = `pix_liberacao_usuario` LIMIT 1) AS usuario_liberado,
		uf.`usuario_nome` AS usuario_recebido");

		if($status != 'todos'){
		if ($status == 'NULL') {
			$this->db->where('`pix_status` IS NULL', NULL, FALSE);
		}else {
			$this->db->where('pix_status', $status);	
		}
		}
		
		$this->db->join('`usuario` uf','uf.usuario_id = `indicacao_pix`.`usuario_id` ');
		return $this->db->get('indicacao_pix')->result();
	}


}