<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicacao_model extends CI_Model {

public $tabela  = "usuario";
public $visivel = "usuario_visivel";
public $chave   = "usuario_id";



public function consultarIndicacao($usuario)
{	
    $this->db->select('*');
    $this->db->where('usuario.usuario_indicacao', $usuario);	
    $this->db->join('usuario','usuario.usuario_id = indicacao.usuario_indicacao ', 'left');
    return $this->db->get('indicacao')->result();

}

public function consultaBonus($usuario)
{	
    $this->db->select('SUM(`indicacao`.`indicacao_valor`) AS total');
    $this->db->where('usuario.usuario_indicacao', $usuario);	
    $this->db->where('indicacao_status', 1);
    $this->db->join('usuario','usuario.usuario_id = indicacao.usuario_indicacao ', 'left');
    return $this->db->get('indicacao')->result();

}

public function inserirValor($usuario, $valor)
{	
    $comissao = $this->consultaComissao();
    $this->db->set('usuario_indicacao', $usuario);
    $this->db->set('indicacao_valor', ($valor / 100 * $comissao[0]->indicacao_parametro));
    $this->db->insert('indicacao');
}

public function consultaComissao()
{	
    $this->db->select('*');
    return $this->db->get('indicacao_parametro')->result();

}

public function comissao($dados)
{
    $this->db->where('indicacao_parametro_id', '0');	
    $this->db->update('indicacao_parametro',$dados);
    
    if($this->db->affected_rows() == '1')
    {
        return true;
    }

    return false;
}

public function consultarExistePix($usuario)
{	
    $this->db->select('COUNT(`usuario`.`usuario_id`) AS total');
    $this->db->where('usuario.usuario_indicacao', $usuario);	
    $this->db->where('indicacao_status', 2);
    $this->db->join('usuario','usuario.usuario_id = indicacao.usuario_indicacao ', 'left');
    return $this->db->get('indicacao')->result();

}

public function tranformarSaldo($bonus, $id)
{
    $sqlUP = "UPDATE indicacao set indicacao_status = 0 WHERE indicacao_status = 1 AND usuario_indicacao IN (SELECT usuario_id FROM usuario WHERE usuario_indicacao = ?)";
    $this->db->query($sqlUP, array($id));
	
    if ($this->db->affected_rows() > 0){
        $this->db->set('usuario_id', $id);
        $this->db->set('historico_saldo_tipo', 0);
        $this->db->set('historico_saldo_status', 'CONCLUIDO');
        $this->db->set('historico_saldo_obsevacao', 'Saldo adicionado via Bônus');
        $this->db->set('historico_saldo_valor', $bonus);
        $this->db->insert('historico_saldo');

        $sqlAtualizar = "UPDATE saldo set saldo_valor = saldo_valor + ? WHERE usuario_id = ?";
        $this->db->query($sqlAtualizar, array($bonus, $id));
        return true;
    }

    return false;
}

public function tranformarSaldoPix($bonus, $id, $pix)
{
    $sqlUP = "UPDATE indicacao set indicacao_status = 2 WHERE indicacao_status = 1 AND usuario_indicacao IN (SELECT usuario_id FROM usuario WHERE usuario_indicacao = ?)";
    $this->db->query($sqlUP, array($id));
	
    if ($this->db->affected_rows() > 0){
        $this->db->set('usuario_id', $id);
        $this->db->set('pix_bonus', $bonus);
        $this->db->set('pix_valor', $pix);
        $this->db->insert('indicacao_pix');
        return true;
    }

    return false;
}

public function buscarDadosPix()
{	
    $this->db->select('*');
    $this->db->where('pix_visivel', 1);
    $this->db->where('pix_status IS NULL', NULL, FALSE);
    $this->db->join('usuario','usuario.usuario_id = indicacao_pix.usuario_id ', 'left');
    return $this->db->get('indicacao_pix')->result();

}

public function InvalidarPix($idUsuario, $idUsuarioadmin, $id)
{

    $sqlUP = "UPDATE indicacao set indicacao_status = 1 WHERE indicacao_status = 2 AND usuario_indicacao IN (SELECT usuario_id FROM usuario WHERE usuario_indicacao = ?)";
    $this->db->query($sqlUP, array($idUsuario));
	
    if ($this->db->affected_rows() > 0){
        $this->db->set('pix_status', 0);
        $this->db->set('pix_liberacao_usuario', $idUsuarioadmin);
        $this->db->where('pix_id',$id);	
        $this->db->update('indicacao_pix');
        return true;
    }

    return false;
}

public function buscarDadadosCliente($id)
{	
    $this->db->select('*');
    $this->db->where('usuario_id',$id);
    
    return $this->db->get('usuario')->result();

}

public function ValidarPix($idUsuario, $idUsuarioadmin, $id)
{

    $sqlUP = "UPDATE indicacao set indicacao_status = 0 WHERE indicacao_status = 2 AND usuario_indicacao IN (SELECT usuario_id FROM usuario WHERE usuario_indicacao = ?)";
    $this->db->query($sqlUP, array($idUsuario));
	
    if ($this->db->affected_rows() > 0){
        $this->db->set('pix_status', 1);
        $this->db->set('pix_liberacao_usuario', $idUsuarioadmin);
        $this->db->where('pix_id',$id);	
        $this->db->update('indicacao_pix');
        return true;
    }

    return false;
}


}