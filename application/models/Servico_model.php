<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico_model extends CI_Model {

public $tabela  = "servicos";
public $visivel = "servico_visivel";
public $chave   = "redes_id";

 public function buscarDadosGeral()
	{

	    $this->db->select("*");
		$this->db->where('redes_visivel', 1);
	    $redes = $this->db->get('redes')->result();
        

        $dados = array();

        foreach ($redes as $r) {
              
			$this->db->select("*");
			$this->db->where($this->visivel, 1);
			$this->db->where('servico_status', 1);
			$this->db->where($this->chave, $r->redes_id);
			$this->db->where('parametro_check', 1);
			$this->db->join('parametros','servicos.parametro_id = parametros.parametro_id ' );
		    $result = $this->db->get($this->tabela)->result();

	          if(count($result) > 0){
	            $dados[$r->redes_nome] = $result;
	          }else{
	          	$dados[$r->redes_nome] = array('redes_id' => $r->redes_id);
	          }   
        }

        return $dados;
	}

 public function buscarDadosGeralGestao()
	{

	    $this->db->select("*");
		$this->db->where('redes_visivel', 1);
	    $redes = $this->db->get('redes')->result();
        

        $dados = array();

        foreach ($redes as $r) {
              
			$this->db->select("*");
			$this->db->where($this->visivel, 1);
			$this->db->where($this->chave, $r->redes_id);
			$this->db->where('parametro_check', 1);
			$this->db->join('parametros','servicos.parametro_id = parametros.parametro_id ' );
		    $result = $this->db->get($this->tabela)->result();

	          if(count($result) > 0){
	            $dados[$r->redes_nome] = $result;
	          }else{
	          	$dados[$r->redes_nome] = array('redes_id' => $r->redes_id);
	          }   
        }

        return $dados;
	}

 public function buscarDados($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		$this->db->where('servico_status', 1);
		$this->db->where('parametro_check', 1);
		$this->db->join('parametros','servicos.parametro_id = parametros.parametro_id ' );
	    return $this->db->get($this->tabela)->result();
	}

 public function ajustarServico($dados, $id)
	{
		$this->db->where('servico_id',$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

 public function adicionar($dados, $tabela)
	{
		$this->db->insert($tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

 public function excluir($servico_visivel, $id)
	{
		$this->db->set($this->visivel, $servico_visivel);
		$this->db->where('servico_id',$id);		
		
		if($this->db->update($this->tabela))
		{
			return true;
		}

		return false;
    }

 public function status($servico_status, $id)
	{
		$this->db->set('servico_status', $servico_status);
		$this->db->where('servico_id',$id);		
		
		if($this->db->update($this->tabela))
		{
			return true;
		}

		return false;
    }
    
 public function excluirRede($redes_visivel, $id)
	{
		$this->db->set('redes_visivel', $redes_visivel);
		$this->db->where('redes_id',$id);		
		
		if($this->db->update('redes'))
		{
			return true;
		}

		return false;
    }  

 public function selecionarServico($id)
	{
		$this->db->select("*");
		$this->db->where('servico_id', $id);
		$this->db->where($this->visivel, 1);
	    return $this->db->get($this->tabela)->result();
	}


 public function buscarDadosServico($id)
	{
		$this->db->select("*");
		$this->db->where('servico_id', $id);
		$this->db->where($this->visivel, 1);
		$this->db->join('parametros','servicos.parametro_id = parametros.parametro_id ' );
	    return $this->db->get($this->tabela)->result();
	}


 public function editar($dados, $id, $tabela)
	{
		$this->db->where('servico_id',$id);

		if ($this->db->update($tabela, $dados))
		{
			return TRUE;
		}
		
		return FALSE; 
	}

  public function buscarDadosGeralGestaoManual()
	{
		$this->db->select("*");
		$this->db->where('ordemservico.parametro_id', 1);
		$this->db->where('ordem_status', 'PENDENTE');
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		$this->db->join('usuario','ordemservico.usuario_id = usuario.usuario_id ' );
	    return $this->db->get('ordemservico')->result();
	}

  public function ajustarOrdem($dados, $id)
	{
		$this->db->where('ordem_id',$id);		
		
		if($this->db->update('ordemservico',$dados))
		{
			return true;
		}

		return false;
	}

   public function buscarOdensCron()
	{
		$this->db->select("*");
		$this->db->where('ordemservico.parametro_id', 1);
		$this->db->where('ordem_status', 'EM PROGRESSO');
	    return $this->db->get('ordemservico')->result();
	}


}