<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comprovante_model extends CI_Model {

public $tabela  = "comprovante";
public $visivel = "comprovante_visivel";
public $status  = "comprovante_status";
public $chave   = "usuario_id";



 public function cadastrarComprovante($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

 public function buscarDados()
	{
		$this->db->select("usuario_nome, perfil_nome, comprovante.usuario_id as idUsuario, saldo_valor, comprovante, comprovante_id, comprovante_status");
		$this->db->join('usuario','usuario.usuario_id = comprovante.usuario_id');
		$this->db->join('saldo','usuario.usuario_id = saldo.usuario_id');
		$this->db->join('perfil','perfil.perfil_id = usuario.usuario_perfil');
		$this->db->where($this->visivel, 1);
		$this->db->where($this->status, 1);
	    return $this->db->get($this->tabela)->result();
	}

 public function ComprovanteStatusInvalidar($status, $id)
	{
		$this->db->set($this->status, $status);
		$this->db->set('comprovante_descricao', 'isso não é um comprovante');
		$this->db->where('comprovante_id',$id);		
		
		if($this->db->update($this->tabela))
		{
			return true;
		}

		return false;
    }

 public function ComprovanteStatusValidar($status, $id, $valor, $idUsuario)
	{
		$this->db->set($this->status, $status);
		$this->db->set('comprovante_descricao', 'Saldo de R$ '.$valor.' inserido.');
		$this->db->where('comprovante_id',$id);		
		
		if($this->db->update($this->tabela))
		{
			$this->db->set('usuario_id', $idUsuario);
			$this->db->set('comprovante_id', $id);
			$this->db->set('historico_saldo_tipo', 0);
			$this->db->set('historico_saldo_status', 'CONCLUIDO');
			$this->db->set('historico_saldo_obsevacao', 'Saldo adicionado via Comprovante');
			$this->db->set('historico_saldo_valor', $valor);
			$this->db->insert('historico_saldo');

			$sqlAtualizar = "UPDATE saldo set saldo_valor = saldo_valor + ? WHERE usuario_id = ?";
	        $this->db->query($sqlAtualizar, array($valor, $idUsuario));
			return true;
		}

		return false;
    }

  public function buscarDadadosCliente($id)
	{	
		$this->db->select('*');
		$this->db->where('usuario_id',$id);
		
		return $this->db->get('usuario')->result();

	}

}