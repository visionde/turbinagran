<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordem_model extends CI_Model {

public $tabela  = "ordemservico";
// public $visivel = "ordem_visi";
public $chave   = "ordem_id";

 public function adicionarOrdem($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{

			return TRUE;
		}
		
		return FALSE; 
	}

 public function consultaOrdem($id)
	{
		$this->db->where('usuario_id', $id);
	    $this->db->from($this->tabela);
		return $this->db->count_all_results();
	}

 public function buscarDadosTotal($id)
	{
		$this->db->where('usuario_id', $id);
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		return $this->db->get($this->tabela)->result();
	}

 public function buscarDadosTotalAtualizar($id)
	{   
		$this->db->select('* ,ordemservico.`parametro_id` AS parametros');
		$this->db->where('usuario_id', $id);
		$this->db->where_in('ordem_status', array('1' => 'PENDENTE', '2' => 'EM PROGRESSO'));
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		return $this->db->get($this->tabela)->result();
	}

 public function buscarDadosTotalAtualizarCron()
	{   
		$this->db->select('* ,ordemservico.`parametro_id` AS parametros');
		$this->db->where_in('ordem_status', array('1' => 'PENDENTE', '2' => 'EM PROGRESSO'));
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		return $this->db->get($this->tabela)->result();
	}

 public function buscarDadosPendentes($id)
	{
		$this->db->where('usuario_id', $id);
		$this->db->where_in('ordem_status', array('1' => 'PENDENTE', '2' => 'EM PROGRESSO'));
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		return $this->db->get($this->tabela)->result();
	}

 public function buscarDadosConcluidas($id)
	{
		$this->db->where('usuario_id', $id);
		$this->db->where_in('ordem_status', array('1' => 'CONCLUIDA', '2' => 'REEMBOLSO'));
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		return $this->db->get($this->tabela)->result();
	}

 public function buscarDadosCreditos($id)
	{
		$this->db->where('usuario_id', $id);
		return $this->db->get('historico_saldo')->result();
	}

 public function buscarDadosDebitos($id)
	{
		$this->db->where('usuario_id', $id);
		$this->db->join('servicos','servicos.servico_id = ordemservico.servico_id ' );
		return $this->db->get($this->tabela)->result();
	}

 public function atualizarOrdem($status, $id_externo){
   
	$this->db->set('ordem_status', $status);
	$this->db->where('ordem_externo_id', $id_externo);
	
	if($this->db->update($this->tabela))
	{
		return true;
	}
	return false;

    }

 public function consultaOrdemRefund($id_externo)
	{
		$this->db->select('*');
		$this->db->where('ordem_externo_id', $id_externo);
		return $this->db->get($this->tabela)->result();
	}

 public function atualizarOrdemRefund($status, $id_externo){
   
	$this->db->set('ordem_status', $status);
	$this->db->where('ordem_externo_id', $id_externo);
	
	if($this->db->update($this->tabela))
	{
		return true;
	}
	return false;

    }


  public function AtualizarRefund($valor, $idUsuario)
	{
		 $sqlAtualizar = "UPDATE saldo set saldo_valor = saldo_valor + ? WHERE usuario_id = ?";
	     $this->db->query($sqlAtualizar, array($valor, $idUsuario));
	}


   



}