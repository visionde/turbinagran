<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parametros_model extends CI_Model {

public $tabela  = "parametros";
public $chave   = "parametro_id";

 public function buscarDados()
	{
		$this->db->select("*");
	    return $this->db->get($this->tabela)->result();
	}

 public function buscarPararametroIdD($id)
	{
		if (count($id) >= 1) {
			$this->db->select("*");
			$this->db->where_in($this->chave, $id);
		    return $this->db->get($this->tabela)->result();
	    }
	}

 public function atualizarParametro($id)
	{
		$this->db->set('parametro_check', 0);			
		if($this->db->update($this->tabela))
		{
			if (count($id) >= 1) {
				$this->db->set('parametro_check', 1);
				$this->db->where_in($this->chave, $id);
			    $this->db->update($this->tabela);
			}
			
		}

	}

 public function salvaConfigs($dados, $id)
    {
	   	$this->db->where($this->chave, $id);
	    if($this->db->update($this->tabela,$dados))
		 {
			return true;
		 }

		 return false;
    }

  public function parametroAtivo()
	{
		$this->db->select("*");
		$this->db->where('parametro_check', 1);
		$this->db->where('parametro_id !=', 3);
	    return $this->db->get($this->tabela)->result();
	}


   public function dadadosParametros($id)
	{
		$this->db->select("*");
		$this->db->where('parametro_id', $id);
	    return $this->db->get($this->tabela)->result();
	}

	public function perfis()
	{
		$this->db->select("*");
		$this->db->where('perfil_id !=', '1');
		$this->db->where('perfil_id !=', '99');
	    return $this->db->get('perfil')->result();
	}

}