<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suporte_model extends CI_Model {

// public $tabela  = "comprovante";
// public $visivel = "comprovante_visivel";
// public $status  = "comprovante_status";
// public $chave   = "usuario_id";



 public function adicionar($dados)
	{
		$this->db->insert('ticket', $dados);
		$id = $this->db->insert_id('ticket');

		if ($this->db->affected_rows() == '1')
		{
			return $id;
		}
		
		return FALSE; 

	}

 public function dadosticket($id)
	{
		$this->db->select('*');
		$this->db->where('ticket_id',$id);
		
		return $this->db->get('ticket')->result();

	}

 public function abertos($id)
	{

		$this->db->select('*');
		$this->db->join('(SELECT chat.ticket_id, MAX(chat.chat_ticket_data) AS chat_ticket_data 
            FROM `chat_ticket` chat
            GROUP BY chat.ticket_id
           ) l3','l3.ticket_id = t.ticket_id', 'left');
		$this->db->where('usuario_id',$id);
		$this->db->where('ticket_status', 1);
		
		return $this->db->get('ticket t')->result();

	}

 public function dadosTicketChat($id)
	{
		$this->db->select('*');
		$this->db->where('ticket.ticket_id',$id);
		$this->db->join('chat_ticket','ticket.ticket_id = chat_ticket.ticket_id', 'left');
		$this->db->join('usuario','usuario.usuario_id = ticket.usuario_id', 'left');
		$this->db->join('perfil','perfil.perfil_id = usuario.usuario_perfil', 'left');
		$this->db->order_by('chat_ticket.chat_ticket_data');
		
		return $this->db->get('ticket')->result();

	}

 public function fechados($id)
	{
		$this->db->select('*');
		$this->db->join('(SELECT chat.ticket_id, MAX(chat.chat_ticket_data) AS chat_ticket_data 
            FROM `chat_ticket` chat
            GROUP BY chat.ticket_id
           ) l3','l3.ticket_id = t.ticket_id', 'left');
		$this->db->where('usuario_id',$id);
		$this->db->where('ticket_status', 2);
		
		return $this->db->get('ticket t')->result();

	}

 public function adicionarChat($dados)
	{
		$this->db->insert('chat_ticket', $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

 public function geral()
	{
		$this->db->select('*, t.ticket_id AS idTicket');
		$this->db->join('(SELECT chat.ticket_id, MAX(chat.chat_ticket_data) AS chat_ticket_data 
            FROM `chat_ticket` chat
            GROUP BY chat.ticket_id
           ) l3','l3.ticket_id = t.ticket_id', 'left');
		$this->db->join('usuario','usuario.usuario_id = t.usuario_id', 'left');
		return $this->db->get('ticket t')->result();

	}

 public function ticketFechar($dados, $id)
	{
	    $this->db->where('ticket_id', $id);
	    $this->db->set($dados);
	    $this->db->update('ticket');
    
    }

}
