<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

public $tabela  = "usuario";
public $visivel = "usuario_visivel";
public $chave   = "usuario_id";

 public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);
		$id = $this->db->insert_id($this->tabela);

		if ($this->db->affected_rows() == '1')
		{
			$this->db->set('saldo_valor', 0);
			$this->db->set('usuario_id', $id);
			$this->db->insert('saldo');
			return TRUE;
		}
		
		return FALSE; 
	}


 public function validarUsuario($email)
	{
		$this->db->select("*");
		$this->db->where('usuario_email', $email);
		$this->db->where($this->visivel, 1);
		$result =  $this->db->get($this->tabela)->result();

		if (count($result) > 0) {
			return TRUE;
		}

		return FALSE;
	}


 public function buscarDadados($email)
	{	
		$this->db->select('*');
		$this->db->where('usuario_email',$email);
		
		return $this->db->get('usuario')->result();

	}

 public function alterar($senha, $email){
   
	$this->db->set('usuario_senha', $senha);
	$this->db->where('usuario_email', $email);
	
	if($this->db->update($this->tabela))
	{
		return true;
	}
	return false;

    }

}