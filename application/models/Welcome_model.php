<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model {

  public function processarLogin($dados)
	{
		
		$this->db->select('*');
		$this->db->join('perfil','perfil.perfil_id = usuario_perfil');
		$this->db->where('usuario_email',$dados['usuario']);
		$this->db->where('usuario_senha',$dados['senha']);
		if (isset($dados['perfil'])) {
		   $this->db->where('usuario_perfil',$dados['perfil']);
		}
		
		return $this->db->get('usuario')->result();

	}	
}