<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldo_model extends CI_Model {

public $tabela  = "saldo";
public $chave   = "usuario_id";

 public function consultaSaldo($id)
	{
		$this->db->select("saldo_valor");
		$this->db->where($this->chave, $id);
	    return $this->db->get($this->tabela)->result();
	}


 public function retiradaSaldo($valor, $usuario){
   
	$this->db->set('saldo_valor', $valor);
	$this->db->where($this->chave, $usuario);
	
	if($this->db->update($this->tabela))
	{
		return true;
	}
	return false;

    }

}