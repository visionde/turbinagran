<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfis_model extends CI_Model {

public $tabela  = "usuario";
public $visivel = "usuario_visivel";
public $chave   = "usuario_id";


 public function buscarDadosGeral()
	{	
		$this->db->select('*');
		$this->db->where('usuario.usuario_id !=', 1);	
		$this->db->where($this->visivel, 1);
		$this->db->join('saldo','usuario.usuario_id = saldo.usuario_id');
		return $this->db->get($this->tabela)->result();

	}

 public function ajustarPerfil($dados, $id)
	{
		$this->db->where($this->chave, $id);	
		
		if($this->db->update($this->tabela, $dados))
		{
			return true;
		}

		return false;
	}


 public function ajustarSaldo($dados, $id)
	{
		$this->db->where($this->chave, $id);	
		
		if($this->db->update('saldo', $dados))
		{
			return true;
		}

		return false;
	}

 public function excluir($usuario_visivel, $id)
	{
		$this->db->set($this->visivel, $usuario_visivel);
		$this->db->where('usuario_id',$id);		
		
		if($this->db->update($this->tabela))
		{
			return true;
		}

		return false;
    }


}