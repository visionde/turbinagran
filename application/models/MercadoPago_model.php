<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MercadoPago_model extends CI_Model {

public $tabela  = "mercado_pago";
public $chave   = "mercadopago_id";


 public function atualizarDadosMercadoPago($dados, $status, $valor)
	{
		$this->db->insert($this->tabela, $dados);
		$id = $this->db->insert_id($this->tabela);

		if ($this->db->affected_rows() == '1')
		{
			$this->db->set('usuario_id', $this->session->userdata('usuario_id'));
			$this->db->set('mercadopago_id', $id);
			$this->db->set('historico_saldo_tipo', 0);
			$this->db->set('historico_saldo_status', $status);
			$this->db->set('historico_saldo_obsevacao', 'Saldo adicionado via MercadoPago');
			$this->db->set('historico_saldo_valor', $valor);
			$this->db->insert('historico_saldo');
			return TRUE;
		}
		
		return FALSE; 
	}



 public function validarPagamento($id)
	{
		$this->db->select("*");
		$this->db->where('mercadopago_payment_id', $id);
		$result =  $this->db->get($this->tabela)->result();

		if (count($result) > 0) {
			return TRUE;
		}

		return FALSE;
	}

  
  public function atualizarSaldo($idUsuario, $valor)
	{ 
        $sqlAtualizar = "UPDATE saldo set saldo_valor = saldo_valor + ? WHERE usuario_id = ?";
        $this->db->query($sqlAtualizar, array($valor, $idUsuario));
		if ($this->db->affected_rows() == '1'){
	      return TRUE;
	    }   
	    return FALSE;      
    }


   public function consultarMercado($idUsuario)
	{ 
		$this->db->select('`mercado_pago`.`mercadopago_payment_id`, mercado_pago`.`mercadopago_id`, historico_saldo.historico_saldo_valor');
		$this->db->join('mercado_pago ','`mercado_pago`.`mercadopago_id` = `historico_saldo`.`mercadopago_id` ' );
		$this->db->where('`historico_saldo`.`mercadopago_id` is NOT NULL', NULL, FALSE);
		$this->db->where('`historico_saldo`.`historico_saldo_status`!=' , 'CONCLUIDO');
		$this->db->where('`historico_saldo`.`usuario_id`' , $idUsuario);
		$this->db->where('`mercado_pago`.`mercadopago_payment_id` is NOT NULL', NULL, FALSE);
		
		return $this->db->get('historico_saldo')->result();   
    }



 public function atualizarHistoricoMercadoPago($status, $idMercado)
	{
		$this->db->set('historico_saldo_status', $status);
		$this->db->where($this->chave , $idMercado);
		$this->db->update('historico_saldo');

	}



}