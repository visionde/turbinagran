<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <title>TurbinaGran | Login & Cadastro</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

    <!-- Custom style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/login-register.css">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.png" type="image/x-icon">

</head>

<body>
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup">

                <form method="post" id="registerUsuario" action="<?php echo base_url(); ?>register/registerUsuario" class="sign-in-form">
                    <h2 class="title">Criar conta</h2>
                    <h2 class="text-muted">

                        <a href="<?php echo base_url(); ?>welcome"><i class="fa fa-fw fa-arrow-circle-left"></i></a>

                        <?php if ($perfil == '2') {
                            echo "<span>Entrar como Revendedor</span>";
                        } else {
                            echo "<span>Entrar como Cliente</span>";
                        } ?>

                    </h2>
                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input type="text" name="nome" placeholder="Nome" required="required" />
                        <input type="hidden" name="perfil" value="<?php echo $perfil ?>">
                    </div>
                    <div class="input-field">
                        <i class="fas fa-envelope"></i>
                        <input type="email" name="email" placeholder="E-mail" required="required" />
                    </div>
                    <div class="input-field">
                        <i class="fab fa-whatsapp"></i>
                        <input type="text" name="whatsapp" placeholder="Whatsapp" required="required" />
                        <input type="hidden" name="codigo" value="<?php echo $codigo ?>">
                    </div>
                    <div class="input-field">
                        <i class="fab fa-instagram"></i>
                        <input type="text" name="instagram" placeholder="Instagram"  />
                    </div>
                    <div class="input-field">
                        <i class="fab fa-facebook"></i>
                        <input type="text" name="facebook" placeholder="Facebook"  />
                    </div>
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" name="password" placeholder="Senha" required="required" />
                    </div>
                    <button type="submit" class="btn">Cadastrar</button>
                    <!--                     <p class="social-text">Ou faça o cadastro com plataformas sociais</p>
                    <div class="social-media">
                        <a href="#" class="social-icon">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#" class="social-icon">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#" class="social-icon">
                            <i class="fab fa-google"></i>
                        </a>
                        <a href="#" class="social-icon">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </div> -->
                </form>

            </div>
        </div>

        <div class="panels-container">

            <div class="panel left-panel">
                <div class="content">
                    <h3>Já é um de nós ?</h3>
                    <p>
                        Se você já possui uma conta em nossa plataforma, efetue o seu login.
                    </p>
                    <button class="btn transparent" onclick="location.href='<?php echo base_url(); ?>welcome/login'" type="button">Entrar</button>
                </div>

                <img src="<?php echo base_url(); ?>assets/dist/img/register.png" class="image" alt="" />
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.pt-br.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/actionRegister.js"></script>

    <!--     <script type="text/javascript">
        $(document).ready(function() {
            urlpg = $(location).attr('href'); //pega a url atual da página
            urllimpa = urlpg.split("/welcome")[0] //tira tudo o que estiver depois de

            window.history.replaceState(null, null, urllimpa); //subtitui a url atual pela url limpa
        })
    </script>
</body> -->

</html>