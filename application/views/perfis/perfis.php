  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Perfis</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Perfis</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Tratamento de mensagens do sitema -->
    <?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
      <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
        </div>
      </div>
    <?php } else if ($this->session->flashdata('success')) { ?>
      <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
        </div>
      </div>
    <?php } ?>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
           <div class="col-md-12">
             <div class="card">
              <div class="card-body">

               <table id="example1" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Perfil</th>
                    <th>Saldo</th>
                    <th>Ação</th>
                  </tr>
                  </thead>
                     <?php foreach ($dados as $v) { ?>
                       <tr>
                         <td><?php echo $v->usuario_nome ; ?></td>
                         <td><?php echo $v->usuario_email ; ?></td>
                          <td class="text-center"> 
                            <select class="form-control" name="status" onchange="ajustarNome(this)" idCampo="usuario_perfil" id="<?php echo $v->usuario_id ?> ">
                              <option value="1"<?php if($v->usuario_perfil == '1'){ echo 'selected';} ?>>ADMINISTRADOR</option>
                              <option value="2"<?php if($v->usuario_perfil == '2'){ echo 'selected';} ?>>REVENDEDOR</option>
                              <option value="3"<?php if($v->usuario_perfil == '3'){ echo 'selected';} ?>>CLIENTE</option>
                            </select>
                          </td>
                          <td> <input style="border:0; width:100px;" class="money" onblur="ajustarSaldo(this)" id="<?php echo $v->usuario_id ?>" type="text"  value="<?php echo $v->saldo_valor ?>" idCampo="saldo_valor"/></td>
                          <td class="text-center">
                            <a style="cursor:pointer;" title="Excluir" data-toggle="modal" data-target="#excluir" onclick="excluirModal('<?php echo base_url() . $this->uri->segment(1) . "/excluir/" . $v->usuario_id."/admin"; ?>');"><i class="fa fa-trash  text-danger"></i></a>
                          </td>
                       </tr>
                     <?php } ?> 

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


 <!-- Modal Excluir registros -->
  <div class="example-modal">
    <div class="modal" id="excluir">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-body">
            <p>Deseja realmente excluir este usuário ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
            <a href="" class="btn btn-danger" id="excluirId">Excluir</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Fim modal Excluir registros -->



  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <script type="text/javascript">

  var base_url = "<?php echo base_url(); ?>";
    
  function ajustarNome(input){
    let valor = $(input).val();
    let idUsuario = $(input).attr('id');
    let idCampo = $(input).attr('idCampo');

    $.ajax({
      url: base_url + "Perfis/ajustarPerfil",
      type: "POST",
      data: { valor: valor, idUsuario: idUsuario, idCampo: idCampo}
    }).done(function (resp) {
      // console.log(resp);
    })
  }

  function ajustarSaldo(input){
    let valor = $(input).val();
    let idUsuario = $(input).attr('id');
    let idCampo = $(input).attr('idCampo');

    $.ajax({
      url: base_url + "Perfis/ajustarSaldo",
      type: "POST",
      data: { valor: valor, idUsuario: idUsuario, idCampo: idCampo}
    }).done(function (resp) {
       location.reload();
    })
  }

  function excluirModal(url, idUsuario) {
      $('#excluirId').attr('href', url);
    }
  </script>