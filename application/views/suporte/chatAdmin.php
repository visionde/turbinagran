
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark" style="font-weight: 600;">Tickets de suporte</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tickets de suporte</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <!-- Timelime example  -->
        <div class="row">
          <div class="col-md-12">
            <!-- The time line -->
            <div class="timeline">
              <!-- timeline time label -->
              <div class="time-label">
                <span class="bg-red">Aberto <?php echo date("d M Y", strtotime(str_replace('/', '-',$ticket[0]->ticket_data)));; ?></span>
              </div>
              <!-- /.timeline-label -->
              <!-- timeline item -->
              <div>
                <i class="fas fa-clipboard-list bg-blue"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fas fa-clock"></i> <?php echo date("H:i", strtotime(str_replace('/', '-',$ticket[0]->ticket_data)));; ?></span>
                  <h3 class="timeline-header"><a href="#"><?php echo ucfirst($ticket[0]->ticket_assunto) ?></a></h3>

                  <div class="timeline-body">
                    <?php echo ucfirst($ticket[0]->ticket_messagem) ?>
                  </div>
                </div>
              </div>

             

                  <?php $date = 0; foreach ($dadosTickts as $k) { 

                    if (isset($k->chat_ticket_data)) {

                      if ($date != date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data)))) { 

                           echo '<div class="time-label">
                               <span class="bg-green">'.date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                            </div>'; 
                       }

                       $date = date("d M Y", strtotime(str_replace('/', '-',$k->chat_ticket_data)));

                      if ($k->chat_ticket_suporte != NULL) {
                        
                        echo '<div>
                            <i class="fas fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="fas fa-clock"></i> '.date("H:i", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                              <h3 class="timeline-header"><a href="#">Support Team</a></h3>

                              <div class="timeline-body">
                                '.ucfirst($k->chat_ticket_messagem).'
                              </div>
                            </div>
                          </div>';

                      } else {
                        
                        echo '<div>
                                <i class="fas fa-user bg-green"></i>
                                <div class="timeline-item">
                                  <span class="time"><i class="fas fa-clock"></i>'.date("H:i", strtotime(str_replace('/', '-',$k->chat_ticket_data))).'</span>
                                  <h3 class="timeline-header"><a href="#">'.$k->usuario_nome.'</a></h3>
                                  <div class="timeline-body">
                                    '.ucfirst($k->chat_ticket_messagem).'
                                  </div>
                                </div>
                              </div>';

                      }


                    }
     
                     
               }


                  $ultimo = end($dadosTickts); 


            if($ultimo->ticket_status == '2'){

               echo '<div class="time-label">
                 <span class="bg-red">Fechado</span>
              </div>'; 

             }else{  ?>


              <div>
                <i class="fas fa-envelope bg-blue"></i>
                <div class="timeline-item">
                  <h3 class="timeline-header"><a href="#">Support Team</a></h3>
                  <div class="timeline-body">
                      <div class="form-group">
                        <label for="message">Mensagem</label>
                        <textarea class="form-control" name="message" id='message' data='<?php echo $this->uri->segment(3)?>' status='<?php echo $ultimo->chat_ticket_suporte ?>' maxlength="512" class="input" placeholder="Descreva a sua resposta aqui"></textarea>
                      </div>
                  </div>
                </div>
              </div>

           <?php  } ?>

              
            </div>
          </div>
          <!-- /.col -->
        </div>
      </div>
      <!-- /.timeline -->

      <?php if($ultimo->ticket_status != '2'){ ?>

            <div class="card-footer">
              <button id="btnResponder" class="btn bg-info color-palette"></i>Responder<i class="fas fa-paper-plane pl-2"></i></button>
              <button id="btnFechar" class="btn bg-red color-palette"></i>Fechar<i class="fas fa-paper-plane pl-2"></i></button>
            </div>

      <?php  } ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">  
  var base_url = "<?php echo base_url(); ?>";
  </script>
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <script type="text/javascript">

    $('#btnResponder').click(function() {

      var message = $('#message').val();
      var status = $('#message').attr('status');
      var id = $('#message').attr('data');


        $.ajax({
          method: "POST",
          url: base_url+"suporte/ticketAdminchat/",
          dataType: "html",
          data: { id: id, message: message}
          })
          .done(function(data) {
              $('.timeline').html(data);    
        });

    });


    $('#btnFechar').click(function() {

      var id = $('#message').attr('data');


        $.ajax({
          method: "POST",
          url: base_url+"suporte/ticketFechar/",
          dataType: "json",
          data: { id: id},
          success: function(data){
             if(data.result == true){ 
                location.reload();   
             }
          }
      
       });

    });



  </script>