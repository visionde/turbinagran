  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark" style="font-weight: 600;">Tickets de suporte</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tickets de suporte</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-clipboard-list mr-1"></i>
              Meus Tickets
            </h3>
          </div>
        
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <div class="tab-content">

                <div class="card-body">

                   <table id="example2" class="table table-bordered table-hover">
                      <thead>
                      <tr>
                        <th>Ticket</th>
                        <th>Atualizado</th>
                        <th>Status</th>
                        <th>Ações</th>
                      </tr>
                      </thead>
                         <?php foreach ($dados as $p) { ?>
                           <tr>
                             <td><?php echo $p->ticket_assunto ; ?></td>
                             <td><?php echo isset($p->chat_ticket_data) ? date("d/m/Y H:i:s",  strtotime(str_replace('/', '-',$p->chat_ticket_data))) :  date("d/m/Y H:i:s",  strtotime(str_replace('/', '-',$p->ticket_data)))  ; ?></td>
                             <td><?php echo 'FECHADO'; ?></td>
                             <td><?php echo ' <a href="'.base_url().'suporte/ticket/'.$p->ticket_id.'" > Ver Ticket </a>'  ?></td>
                           </tr>
                         <?php } ?>
                    </table>

                </div>

            </div>
          </div>
        </div>     
      </div>
    </div>
  </div>
</section>
    <!-- /.content -->
 </div>
  <!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

