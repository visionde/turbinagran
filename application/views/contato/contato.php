  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Contato</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Contato</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">


  			<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
  				<div class="col-md-12"><br>
  					<div class="alert alert-danger alert-dismissible">
  						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  						<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
  					</div>
  				</div>
  			<?php } else if ($this->session->flashdata('success')) { ?>
  				<div class="col-md-12">
  					<div class="alert alert-success alert-dismissible">
  						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  						<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
  					</div>
  				</div>
  			<?php } ?>

  			<div class="row">
  				<div class="col-md-6 col-xl-6">
  					<div class="card card-primary">
  						<div class="card-header" style="background-color: #59277a; border-top-left-radius: 10px; border-top-right-radius: 10px;">
  							<h3 class="card-title">Preencha os campos abaixo e a nossa equipe de suporte estará entrando em contato com você.</h3>
  						</div>
  						<!-- /.card-header -->
  						<!-- form start -->
  						<form id="formContato" method="post" action="<?php echo base_url(); ?>contato/contatoMenu" role="form">
  							<div class="card-body">

  								<div class="form-group">
  									<label for="nome">Nome</label>
  									<input type="nome" class="form-control" name="nome" id="nome" required="required" placeholder="Informe o seu nome">
  								</div>
  								<div class="form-group">
  									<label for="email">E-mail</label>
  									<input type="email" class="form-control" name="email" id="email" required="required" placeholder="exemplo@email.com">
  								</div>
  								<div class="form-group">
  									<label for="tel">Telefone</label>
  									<input type="number" name="tel" class="form-control" id="tel" placeholder="999999999">
  								</div>
  								<div class="form-group">
  									<label for="message">Mensagem</label>
  									<textarea class="form-control" name="message" id='message' maxlength="512" class="input" placeholder="Descreva a sua dúvida ou sugestão aqui"></textarea>
  								</div>

  							</div>
  							<!-- /.card-body -->

  							<div class="card-footer">
  								<button type="submit" class="btn btn-primary btn-contato">Enviar <i class="fas fa-paper-plane pl-2"></i></button>
  							</div>
  						</form>
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/contato.png" alt="">
  				</div>

  			</div>

  			<style type="text/css" scoped>
  				.card {
  					border-radius: 10px;
  				}

  				.form-control {
  					border-radius: 10px;
  				}

  				.btn-contato {
  					background-color: #59277a;
  					border: none;
  					border-radius: 10px;
  					padding: 5px 25px;
  					font-size: 1.1rem;
  				}

  				.btn-contato:hover {
  					background-color: #6a2c87;
  				}

  				.card-footer {
  					background-color: #fff;
  					border-radius: 10px;
  				}

  				@media (max-width: 600px) {
  					.btn-contato {
  						width: 100%;
  					}
  				}
  			</style>


  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.pt-br.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/actionDashboard.js"></script>