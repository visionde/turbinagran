<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <title>TurbinaGran | Redefinição de senha</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

    <!-- Custom style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/login-register.css">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.png" type="image/x-icon">

</head>

<body>
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup recover">

                <form method="post" id="recuperarSenhaNova" action="<?php echo base_url(); ?>/register/recuperarSenhaNova">
                    <h2 class="title">Redefinir senha</h2>
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" name="password" id="password" placeholder="Nova senha" required="required" />
                    </div>
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" name="password2" id="password2" placeholder="Confirmar senha" required="required" />
                    </div>
                    <input type="hidden" name="tipo" value="<?php echo $this->uri->segment(3); ?>">
                    <button type="submit" class="btn">Salvar</button>
                </form>

            </div>
        </div>

        <div class="panels-container">

            <div class="panel left-panel">
                <div class="content">
                    <h3>Lembrou a senha da sua conta ?</h3>
                    <p>
                        Faça login na sua conta.
                    </p>
                    <a href="<?php echo base_url(); ?>welcome/login">
                        <button class="btn transparent">
                            Entrar
                        </button>
                    </a>
                </div>
                <img src="<?php echo base_url(); ?>assets/dist/img/password.png" class="image" alt="" />
            </div>

        </div>
    </div>

</body>

</html>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.pt-br.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/actionRegister.js"></script>

<!--     <script type="text/javascript">
        $(document).ready(function() {
            urlpg = $(location).attr('href'); //pega a url atual da página
            urllimpa = urlpg.split("/welcome")[0] //tira tudo o que estiver depois de

            window.history.replaceState(null, null, urllimpa); //subtitui a url atual pela url limpa
        })
    </script> -->