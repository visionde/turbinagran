  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark" style="font-weight: 600;">Youtube</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Youtube</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-xl-6">
            <div class="card card-red">
              <div class="card-header">
                <h3 class="card-title">Escolha seu Serviço</h3>
                <i class="fas fa-clipboard-list" style="position: absolute; right: 2%; font-size: 25px;"></i>
              </div>

              <?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
                <div class="col-md-12"><br>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
                  </div>
                </div>
              <?php } else if ($this->session->flashdata('success')) { ?>
                <div class="col-md-12">
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
                  </div>
                </div>
              <?php } ?>
              <form method="post" class="send-order" id="formTubeCut" action="<?php echo base_url(); ?>youtube/ordemServico">
                <div class="card-body">

                  <label class="filed-form"><span class="filed">Serviços:</span></label>
                  <select id="servicotube" class="form-control" data-placeholder="Selecione o grupo de produtos" name="servicotube">
                    <option value="">Selecione</option>
                    <?php foreach ($youtube as $i) { ?>
                      <option value='<?php echo $i->servico_id; ?>'><?php echo $i->servico_nome; ?> </option>
                    <?php } ?>
                  </select><br>

                  <label class="filed-form"><span class="filed">Link:</span></label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">@</span>
                    </div>
                    <input type="text" name="idtube" class="form-control" placeholder="Insira o link ou id da Pagina do Youtube">
                  </div>

                  <label class="filed-form"><span class="filed">Quantidade:</span></label>
                  <input type="number" name="quantidade" id="quantidade" class="form-control">

                  <div id="tubeMini" class="spinner">
                    <span class="spin minimo">mínimo </span>
                    <span class="spin maximo">máxmio </span>
                  </div><br>

                  <label class="filed-form"><span class="filed">Valor:</span></label>
                  <input type="text" name="valor" id="valorTube" class="form-control" disabled="disabled">
                  <br>
                </div>

                <div class="card-footer">
                  <button type="submit" class="btn bg-red color-palette"><i class="nav-icon fab fa-youtube"></i> Enviar Ordem</button>
                </div>

              </form>
              <!-- /.card-body -->
            </div>
          </div>

          <div class="col-md-6 col-xl-6">
            <div class="card card-red">
              <div class="card-header">
                <h3 class="card-title">Informações</h3>
                <i class="fas fa-info-circle" style="position: absolute; right: 2%; font-size: 25px;"></i>
              </div>
              <div class="card-body p-0">
                <table class="table">
                  <tbody id="tubetable">
                    <tr>
                      <td><strong>Link:</strong><br>Clique em enviar apenas 1 (uma vez)</td>
                    <tr>
                      <td><strong>Minimo:</strong><br></td>
                    </tr>
                    <tr>
                      <td><strong>Máximo:</strong><br></td>
                    </tr>
                    <tr>
                      <td><strong>Valor p/ 1000:<span id="spanValor"></span></strong><br></td>
                    </tr>
                    <tr>
                      <td><strong>Observações:</strong><br></td>
                    </tr>

                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>

      </div>
      <!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  <!-- jQuery -->
  <script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
  </script>
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.pt-br.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/actionDashboard.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/actionYoutube.js"></script>