  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark">Serviços Manual Gestão</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Serviços Manual Gestão</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="row">
  				<div class="col-md-12">
  					<div class="card">
  						<div class="card-body">

  							<table id="example1" class="table table-bordered table-hover">
  								<thead>
  									<tr>
  										<th>Usuário</th>
  										<th>Serviço</th>
  										<th>Data</th>
  										<th>QTD/Valor</th>
  										<th>Link</th>
  										<th>Status</th>
  									</tr>
  								</thead>
  								<?php foreach ($dados as $v) { ?>
  									<tr>
  										<td><?php echo $v->usuario_nome; ?></td>
  										<td><?php echo $v->servico_nome; ?></td>
  										<td><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $v->ordem_data))); ?></td>
  										<td><?php echo $v->ordem_quantidade . ' / R$ ' . str_replace('.', ',', $v->ordem_valor); ?></td>
  										<td><?php echo $v->ordem_link; ?></td>
  										<td class="text-center">
  											<select class="form-control" name="status" onchange="ajustarNome(this)" idCampo="ordem_status" id="<?php echo $v->ordem_id ?> ">
  												<option value="PENDENTE" <?php if ($v->ordem_status == 'PENDENTE') {
																				echo 'selected';
																			} ?>>PENDENTE</option>
  												<option value="EM PROGRESSO" <?php if ($v->ordem_status == 'EM PROGRESSO') {
																					echo 'selected';
																				} ?>>EM PROGRESSO</option>
  												<option value="CONCLUIDA" <?php if ($v->ordem_status == 'CONCLUIDA') {
																				echo 'selected';
																			} ?>>CONCLUIDO</option>
  											</select>
  										</td>
  									</tr>
  								<?php } ?>

  							</table>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	@media (max-width: 600px) {
  		td {
  			font-size: 12px;
  		}

  		.pagination {
  			display: inline-flex;
  			justify-content: center;
  			padding-top: 20px;
  		}
  	}
  </style>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <script type="text/javascript">
  	var base_url = "<?php echo base_url(); ?>";

  	function ajustarNome(input) {
  		let valor = $(input).val();
  		let idOrdem = $(input).attr('id');
  		let idCampo = $(input).attr('idCampo');

  		$.ajax({
  			url: base_url + "servico/ajustarOrdem",
  			type: "POST",
  			data: {
  				valor: valor,
  				idOrdem: idOrdem,
  				idCampo: idCampo
  			}
  		}).done(function(resp) {
  			// console.log(resp);
  		})
  	}
  </script>