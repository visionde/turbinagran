  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Serviços</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Serviços</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">

  			<?php foreach ($dados as $key => $value) { ?>
  				<div id="servico-desktop" class="row">
  					<div class="col-md-12 col-xl-12">
  						<div class="card card-info">
  							<div class="card-header">
  								<h3 class="card-title"><?php echo $key; ?></h3>
  							</div>
  							<div class="card-body p-0">
  								<table class="table">
  									<th class="text-center">Serviço</th>
  									<th class="text-center">Mínimo</th>
  									<th class="text-center">Máximo</th>
  									<?php if ($this->session->userdata('perfil_id') == '1') { ?>
  										<th class="text-center">Preço Por 1000 - Cliente</th>
  										<th class="text-center">Preço Por 1000 - Revendedor</th>
  									<?php } else { ?>
  										<th class="text-center">Preço Por 1000</th>
  									<?php } ?>

  									<tbody>
  										<?php if (!isset($dados[$key]['redes_id'])) {
												foreach ($value as $i) { ?>
  												<tr>
  													<td class="text-center"><?php echo $i->servico_nome ?></td>
  													<td class="text-center"><?php echo $i->servico_minimo ?></td>
  													<td class="text-center"><?php echo $i->servico_maximo; ?></td>
  													<?php if ($this->session->userdata('perfil_id') == '1') { ?>
  														<td class="text-center"><?php echo str_replace('.', ',', $i->servico_preco); ?></td>
  														<td class="text-center"><?php echo str_replace('.', ',', $i->servico_preco_rev); ?></td>
  													<?php } else { ?>
  														<td class="text-center"><?php echo $this->session->userdata('perfil_id') == '2' ? str_replace('.', ',', $i->servico_preco_rev) : str_replace('.', ',', $i->servico_preco) ?>
  														<?php } ?>
  												</tr>
  										<?php }
											} ?>
  									</tbody>
  								</table>
  							</div>
  						</div>
  					</div>
  				</div>

  				<div id="servico-mobile" class="card border-light mb-3">
  					<div class="card-header text-center">
  						<h3 class="card-title"><?php echo $key; ?></h3>
  					</div>
  					<div class="card-body">
  						<?php if (!isset($dados[$key]['redes_id'])) {
								foreach ($value as $i) { ?>
  								<!--<h5 class="card-title">Light card title</h5>-->
  								<p class="card-text">
  									<span class="text-center"><span class="nome-servico">Serviço:</span> <?php echo $i->servico_nome ?></span><br>
  									<span class="text-center"><span class="nome-servico">Mínimo: </span><?php echo $i->servico_minimo ?></span><br>
  									<span class="text-center"><span class="nome-servico">Máximo: </span><?php echo $i->servico_maximo; ?></span><br>
  									<?php if ($this->session->userdata('perfil_id') == '1') { ?>
  										<span class="text-center"><span class="nome-servico">Preço para 1000 - Cliente : </span><?php echo str_replace('.', ',', $i->servico_preco); ?></span><br>
  										<span class="text-center"><span class="nome-servico">Preço para 1000 - Revendedor: </span> <?php echo str_replace('.', ',', $i->servico_preco_rev); ?></span><br>
  									<?php } else { ?>
  										<span class="text-center"><span class="nome-servico">Preço para 1000: </span><?php echo $this->session->userdata('perfil_id') == '2' ? str_replace('.', ',', $i->servico_preco_rev) : str_replace('.', ',', $i->servico_preco) ?></span><br>
  									<?php } ?>
  								</p>
  						<?php }
							} ?>
  					</div>
  				</div>
  			<?php } ?>

  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	#servico-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {
  		#servico-desktop {
  			display: none;
  		}

  		#servico-mobile {
  			display: block;
  		}

  		#servico-mobile.card {
  			border-radius: 10px;
  		}

  		#servico-mobile .card-header {
  			border-radius: 10px;
  			background-color: #59277A;
  			color: #fff;
  			font-size: 1.2rem;
  			font-weight: 600;
  		}
  	}

  	.nome-servico {
  		font-weight: 600;
  	}
  </style>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->