  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Gestão de Serviços</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Gestão de Serviços</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Tratamento de mensagens do sitema -->
  	<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
  		<div class="col-md-12">
  			<div class="alert alert-danger alert-dismissible">
  				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
  			</div>
  		</div>
  	<?php } else if ($this->session->flashdata('success')) { ?>
  		<div class="col-md-12">
  			<div class="alert alert-success alert-dismissible">
  				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
  			</div>
  		</div>
  	<?php } ?>

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<!--<div class="col-md-2">-->
  			<a href="#" type="button" data-toggle="modal" data-target="#addRedes" class="btn btn-block btn-success" style="max-width: 200px; background: #B92786; border: none; border-radius: 10px;">Adicionar rede social <i class="fas fa-plus-circle pl-2"></i></a>
  			<!--</div>--><br><br>

  			<?php foreach ($dados as $key => $value) {

					isset($dados[$key]['redes_id']) ? $id = $dados[$key]['redes_id'] : $id = $value[0]->redes_id; ?>

  				<div class="row">
  					<div class="col-md-12 col-xl-12">
  						<div id="gestao-servico-desktop" class="card card-info">
  							<div class="card-header">
  								<h3 class="card-title"><a style="cursor:pointer;" title="Adicionar" data-toggle="modal" data-target="#addServico" onclick="addServicoModal('<?php echo $id; ?>');"><i class="fas fa-plus-circle pr-2"></i> <?php echo $key; ?></a></h3>
  								<div class="card-tools">
  									<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluirServico" onclick="excluirServicoModal('<?php echo base_url() . $this->uri->segment(1) . "/excluirRede/" . $id; ?>', '<?php echo $key; ?>');"><i class="fa fa-trash  text-danger"></i></a>
  								</div>
  							</div>
  							<div class="card-body p-0">
  								<table class="table table-condensed">
  									<thead>
  										<tr>
  											<th class="text-center">Serviço</th>
  											<th class="text-center">Obeservação</th>
  											<th class="text-center">Sistema</th>
  											<th class="text-center">Status</th>
  											<th class="text-center">Ação</th>
  										</tr>
  									</thead>
  									<tbody>

  										<?php

											if (!isset($dados[$key]['redes_id'])) {

												foreach ($value as $v) {

													$v->servico_status == '1' ? $status = 'Ativo' : $status = 'Inativo';

													echo '<tr>';

													echo '<td class="text-center"> <input style="border:0; width:400px;" onblur="ajustarNome(this)" id="' . $v->servico_id . '" type="text"  value="' . $v->servico_nome . '" idCampo="servico_nome"/></td>';

													echo '<td class="text-center"> <input style="border:0; width:400px;" onblur="ajustarNome(this)" id="' . $v->servico_id . '" type="text"  value="' . $v->servico_observacao . '" idCampo="servico_observacao"/></td>';

													echo '<td class="text-center">' . $v->parametro_nome . '</td>';

													echo '<td class="text-center">' . $status . '</td>';  ?>

  												<td class="text-center">

  													<a style="cursor:pointer;" title="Editar" data-toggle="modal" data-target="#editarServico" onclick="editarServicoModal('<?php echo $v->servico_id; ?>');"><i class="fa fa-edit  text-success
                              "></i></a>

  													<a style="cursor:pointer;" title="Mudar" data-toggle="modal" data-target="#statusServico" onclick="statusServicoModal('<?php echo base_url() . $this->uri->segment(1) . "/status/" . $v->servico_id . "/" . $v->servico_status; ?>', '<?php echo $v->servico_nome; ?>');"><i class="fas fa-times  text-danger"></i></a>

  													<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluirServico" onclick="excluirServicoModal('<?php echo base_url() . $this->uri->segment(1) . "/excluir/" . $v->servico_id; ?>', '<?php echo $v->servico_nome; ?>');"><i class="fa fa-trash  text-danger"></i></a>
  												</td>

  										<?php echo '</tr>';
												}
											} ?>

  									</tbody>
  								</table>
  							</div>
  						</div>

  						<div id="gestao-servico-mobile" class="card card-info">
  							<div class="card-header">
  								<h3 class="card-title"><a style="cursor:pointer;" title="Adicionar" data-toggle="modal" data-target="#addServico" onclick="addServicoModal('<?php echo $id; ?>');"><i class="fas fa-plus-circle pr-2"></i> <?php echo $key; ?></a></h3>
  								<div class="card-tools">
  									<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluirServico" onclick="excluirServicoModal('<?php echo base_url() . $this->uri->segment(1) . "/excluirRede/" . $id; ?>', '<?php echo $key; ?>');"><i class="fa fa-trash  text-danger"></i></a>
  								</div>
  							</div>
  							<div class="card-body px-3 py-3">
  								<p class="card-text">
  									</tr>

  									<?php

										if (!isset($dados[$key]['redes_id'])) {

											foreach ($value as $v) {

												$v->servico_status == '1' ? $status = 'Ativo' : $status = 'Inativo';

												echo '<p> <span class="text-service">Serviço: </span> <input style="border:0; " onblur="ajustarNome(this)" id="' . $v->servico_id . '" type="text"  value="' . $v->servico_nome . '" idCampo="servico_nome"/></p>';

												echo '<p> <span class="text-service">Observação: </span><input style="border:0;" onblur="ajustarNome(this)" id="' . $v->servico_id . '" type="text"  value="' . $v->servico_observacao . '" idCampo="servico_observacao"/></p>';

												echo '<p> <span class="text-service">Sistema: </span>' . $v->parametro_nome . '</p>';

												echo '<p> <span class="text-service">Status: </span>' . $status . '</p>';

										?>

  								<p>

  									<a style="cursor:pointer;" title="Editar" data-toggle="modal" data-target="#editarServico" onclick="editarServicoModal('<?php echo $v->servico_id; ?>');"><i class="fa fa-edit text-success icones-gestao"></i></a>

  									<a style="cursor:pointer;" title="Mudar" data-toggle="modal" data-target="#statusServico" onclick="statusServicoModal('<?php echo base_url() . $this->uri->segment(1) . "/status/" . $v->servico_id . "/" . $v->servico_status; ?>', '<?php echo $v->servico_nome; ?>');"><i class="fas fa-times text-danger icones-gestao"></i></a>

  									<a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluirServico" onclick="excluirServicoModal('<?php echo base_url() . $this->uri->segment(1) . "/excluir/" . $v->servico_id; ?>', '<?php echo $v->servico_nome; ?>');"><i class="fa fa-trash text-danger icones-gestao"></i></a>
  								</p>

  						<?php echo '<hr>';
											}
										} ?>
  						</p>
  							</div>
  						</div>
  					</div>
  				</div>

  			<?php } ?>

  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	#gestao-servico-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {
  		#gestao-servico-mobile p {
  			font-size: 14px;
  			display: inline-flex;
  			flex-wrap: wrap;
  			width: 100%;
  		}

  		.icones-gestao {
  			font-size: 25px;
  			margin-right: 15px;
  		}

  		#gestao-servico-desktop {
  			display: none;
  		}

  		#gestao-servico-mobile {
  			display: block;
  		}

  		.text-service {
  			font-weight: bold;
  			padding-right: 5px;
  		}

  		input {
  			width: 100%;
  		}
  	}
  </style>

  <!-- Modal Excluir registros -->
  <div class="example-modal">
  	<div class="modal" id="excluirServico">
  		<div class="modal-dialog modal-dialog-centered">
  			<div class="modal-content">
  				<div class="modal-body">
  					<p id="servicoExcluir">Deseja realmente excluir este serviço ?</p>
  				</div>
  				<div class="modal-footer">
  					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  					<a href="" class="btn btn-danger" id="excluirIdServico">Excluir</a>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <!-- Fim modal Excluir registros -->

  <!-- Modal Excluir registros -->
  <div class="example-modal">
  	<div class="modal" id="statusServico">
  		<div class="modal-dialog">
  			<div class="modal-content">
  				<div class="modal-body">
  					<p id="servicostatus">Deseja mudar o status do serviço ?</p>
  				</div>
  				<div class="modal-footer">
  					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  					<a href="" class="btn btn-danger" id="statusIdServico">Alterar</a>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <!-- Fim modal Excluir registros -->

  <!-- Modal -->
  <div class="modal fade" id="addServico">
  	<div class="modal-dialog modal-dialog-centered">
  		<div class="modal-content">
  			<div class="modal-body">
  				<div class="content">

  					<form role="form" id="addServicoForm" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/addServico" method="post" enctype="multipart/form-data">

  						<div class="form-group">
  							<label>Sistema</label>
  							<select id="modal_serviço_sistema" class="form-control" data-placeholder="Selecione o Sistema" name="modal_serviço_sistema" required="required">
  								<option value="">Selecione</option>
  								<?php foreach ($parametro as $p) { ?>
  									<option value='<?php echo $p->parametro_id; ?>'><?php echo $p->parametro_nome; ?> </option>
  								<?php } ?>
  							</select>
  						</div>

  						<div class="form-group">
  							<label>Serviço</label>
  							<input class="col-md-12 form-control" name="modal_serviço" id="modal_serviço" required="required"></input>
  							<input type="hidden" class="col-md-12 form-control" name="modal_redes" id="modal_redes"></input>
  						</div>
  						<div class="form-group">
  							<label>Observação</label>
  							<textarea class="col-md-12 form-control" name="modal_serviço_observacao" id="modal_serviço_observacao"></textarea>
  						</div>
  						<div class="form-group">
  							<label>ID Externo</label>
  							<input type="number" class="col-md-9 form-control" name="modal_serviço_id_externo" id="modal_serviço_id_externo"></input>
  						</div>
  						<div class="form-group">
  							<label>Mínimo</label>
  							<input type="number" class="col-md-9 form-control" name="modal_serviço_minimo" id="modal_serviço_minimo"></input>
  						</div>
  						<div class="form-group">
  							<label>Máximo</label>
  							<input type="number" class="col-md-9 form-control" name="modal_serviço_maximo" id="modal_serviço_maximo"></input>
  						</div>
  						<div class="form-group">
  							<label>Preço Por 1000 - Cliente</label>
  							<input class="col-md-9 form-control money" name="modal_serviço_preço" id="modal_serviço_preço"></input>
  						</div>

  						<div class="form-group">
  							<label>Preço Por 1000 - Revendedor</label>
  							<input class="col-md-9 form-control money" name="modal_serviço_preço_rev" id="modal_serviço_preço_rev"></input>
  						</div>

  				</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  				<button type="submit" class="btn btn-primary">Adicionar</button>
  			</div>
  			</form>
  		</div>
  		<!-- /.modal-content -->
  	</div>
  	<!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Modal -->
  <div class="modal fade" id="editarServico">
  	<div class="modal-dialog modal-dialog-centered">
  		<div class="modal-content">
  			<div class="modal-body">
  				<div class="content">

  					<form role="form" id="editarServicoForm" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/editarServico" method="post" enctype="multipart/form-data">

  						<div class="form-group edi_modal_serviço_sistema">
  							<label>Sistema</label>
  							<select id="edi_modal_serviço_sistema" class="form-control" data-placeholder="Selecione o Sistema" name="edi_modal_serviço_sistema" required="required">
  								<option value="">Selecione</option>
  								<?php foreach ($parametro as $p) { ?>
  									<option value='<?php echo $p->parametro_id; ?>'><?php echo $p->parametro_nome; ?> </option>
  								<?php } ?>
  							</select>
  						</div>

  						<div class="form-group">
  							<label>Serviço</label>
  							<input class="col-md-12 form-control" name="edi_modal_serviço" id="edi_modal_serviço" required="required"></input>
  							<input type="hidden" class="col-md-12 form-control" name="edi_modal_redes" id="edi_modal_redes"></input>
  							<input type="hidden" class="col-md-12 form-control" name="edi_modal_serviço_id" id="edi_modal_serviço_id"></input>
  						</div>
  						<div class="form-group">
  							<label>Observação</label>
  							<textarea class="col-md-12 form-control" name="edi_modal_serviço_observacao" id="edi_modal_serviço_observacao"></textarea>
  						</div>
  						<div class="form-group">
  							<label>ID Externo</label>
  							<input type="number" class="col-md-9 form-control" name="edi_modal_serviço_id_externo" id="edi_modal_serviço_id_externo"></input>
  						</div>
  						<div class="form-group">
  							<label>Mínimo</label>
  							<input type="number" class="col-md-9 form-control" name="edi_modal_serviço_minimo" id="edi_modal_serviço_minimo"></input>
  						</div>
  						<div class="form-group">
  							<label>Máximo</label>
  							<input type="number" class="col-md-9 form-control" name="edi_modal_serviço_maximo" id="edi_modal_serviço_maximo"></input>
  						</div>
  						<div class="form-group">
  							<label>Preço Por 1000 - Cliente</label>
  							<input class="col-md-9 form-control money" name="edi_modal_serviço_preço" id="edi_modal_serviço_preço"></input>
  						</div>

  						<div class="form-group">
  							<label>Preço Por 1000 - Revendedor</label>
  							<input class="col-md-9 form-control money" name="edi_modal_serviço_preço_rev" id="edi_modal_serviço_preço_rev"></input>
  						</div>

  				</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  				<button type="submit" class="btn btn-primary">Editar</button>
  			</div>
  			</form>
  		</div>
  		<!-- /.modal-content -->
  	</div>
  	<!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <!-- Modal -->
  <div class="modal fade" id="addRedes">
  	<div class="modal-dialog modal-dialog-centered">
  		<div class="modal-content">
  			<div class="modal-body">
  				<div class="content">

  					<form role="form" id="addRedesForm" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/addRedes" method="post" enctype="multipart/form-data">

  						<div class="form-group">
  							<label>Rede Social</label>
  							<input class="col-md-12 form-control" name="modal_redes" id="modal_redes" required="required"></input>
  						</div>

  				</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  				<button type="submit" class="btn btn-primary">Adicionar</button>
  			</div>
  			</form>
  		</div>
  		<!-- /.modal-content -->
  	</div>
  	<!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <script type="text/javascript">
  	var base_url = "<?php echo base_url(); ?>";
  </script>

  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.pt-br.js"></script>


  <script type="text/javascript">
  	$('.selectTudo').click(function(event) {
  		$(this).select();
  	});


  	function ajustarCampo(input) {
  		let valor = parseFloat($(input).val());
  		let idServico = $(input).attr('id');
  		let idCampo = $(input).attr('idCampo');

  		if (isNaN(valor) && idCampo == 'servico_preco') {
  			valor = 0;
  			$(input).val('0.00');
  		}

  		$.ajax({
  			url: base_url + "servico/ajustarServico",
  			type: "POST",
  			data: {
  				valor: valor,
  				idServico: idServico,
  				idCampo: idCampo
  			}
  		}).done(function(resp) {
  			// console.log(resp);
  		})
  	}

  	function ajustarNome(input) {
  		let valor = $(input).val();
  		let idServico = $(input).attr('id');
  		let idCampo = $(input).attr('idCampo');

  		if (isNaN(valor) && idCampo == 'servico_preco') {
  			valor = 0;
  			$(input).val('0.00');
  		}

  		$.ajax({
  			url: base_url + "servico/ajustarServico",
  			type: "POST",
  			data: {
  				valor: valor,
  				idServico: idServico,
  				idCampo: idCampo
  			}
  		}).done(function(resp) {
  			// console.log(resp);
  		})
  	}

  	function excluirServicoModal(url, idServico) {
  		$('#servicoExcluir').html('<p>Deseja realmente excluir este serviço "' + idServico + '"?</p>');
  		$('#excluirIdServico').attr('href', url);
  	}

  	function statusServicoModal(url, idServico) {
  		$('#servicostatus').html('<p>Deseja mudar o status do serviço "' + idServico + '"?</p>');
  		$('#statusIdServico').attr('href', url);
  	}

  	function addServicoModal(idRedes) {
  		$('#modal_redes').val(idRedes);
  	}

  	function editarServicoModal(id) {

  		$.ajax({
  			method: "POST",
  			url: base_url + "servico/ajaxservico",
  			dataType: "JSON",
  			data: {
  				id: id
  			}
  		}).done(function(response) {

  			$("div.edi_modal_serviço_sistema select").val(response[0].parametro_id);
  			$("#edi_modal_redes").val(response[0].redes_id);
  			$("#edi_modal_serviço").val(response[0].servico_nome);
  			$("#edi_modal_serviço_id").val(id);
  			$("#edi_modal_serviço_observacao").val(response[0].servico_observacao);
  			$("#edi_modal_serviço_id_externo").val(response[0].externo_id);
  			$("#edi_modal_serviço_minimo").val(response[0].servico_minimo);
  			$("#edi_modal_serviço_maximo").val(response[0].servico_maximo);
  			$("#edi_modal_serviço_preço").val(response[0].servico_preco);
  			$("#edi_modal_serviço_preço_rev").val(response[0].servico_preco_rev);
  		})
  	}

  	$("#addServicoForm").validate({
  		rules: {
  			modal_serviço: "required"
  		}
  	});

  	$("#addRedesForm").validate({
  		rules: {
  			modal_redes: "required"
  		}
  	});
  </script>