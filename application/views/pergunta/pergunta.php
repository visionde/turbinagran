  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Dúvidas Frequentes</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Dúvidas Frequentes</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="row">

  				<div class="col-md-6 col-xl-6">
  					<div class="accordion" id="QuestionsLeft">
  						<div class="card">
  							<div class="card-header" id="headingOne">
  								<h2 class="mb-0">
  									<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
  										Como adicionar saldo?<i class="fas fa-plus"></i>
  									</button>
  								</h2>
  							</div>

  							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#QuestionsLeft">
  								<div class="card-body">
  									Para adicionar saldo basta fazer depósito em uma de nossas contas bancárias e enviar o comprovante para ne seção “Recarga” na opção gerenciar comprovantes.
  								</div>
  							</div>
  						</div>

  						<div class="card">
  							<div class="card-header" id="headingTwo">
  								<h2 class="mb-0">
  									<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
  										Em quanto tempo recebo meu pedido?<i class="fas fa-plus"></i>
  									</button>
  								</h2>
  							</div>
  							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#QuestionsLeft">
  								<div class="card-body">
  									Após a solicitação, o envio do serviço começará em no máximo 24h.
  								</div>
  							</div>
  						</div>

  						<div class="card">
  							<div class="card-header" id="headingThree">
  								<h2 class="mb-0">
  									<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
  										Cancelamento do pedido<i class="fas fa-plus"></i>
  									</button>
  								</h2>
  							</div>
  							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#QuestionsLeft">
  								<div class="card-body">
  									Após o pagamento dos serviços o envio é programado, NÃO havendo meios de cancelamento ou estorno.
  								</div>
  							</div>
  						</div>

  						<div class="card">
  							<div class="card-header" id="headingFour">
  								<h2 class="mb-0">
  									<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
  										Posso utilizar o serviço em qualquer conta?<i class="fas fa-plus"></i>
  									</button>
  								</h2>
  							</div>
  							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#QuestionsLeft">
  								<div class="card-body">
  									Sim! mas para a entrega do serviço é essencial o usuário manter o seu perfil "aberto" (modo público), do contrário o pedido será cancelado. Após a entrega, o usuário poderá colocar seu perfil em modo privado se preferir.
  								</div>
  							</div>
  						</div>

  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<div class="accordion" id="QuestionsRight">
  						<div class="card">
  							<div class="card-header" id="headingFive">
  								<h2 class="mb-0">
  									<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
  										É normal eu perder seguidores?<i class="fas fa-plus"></i>
  									</button>
  								</h2>
  							</div>

  							<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#QuestionsRight">
  								<div class="card-body">
  									Sim. É normal perder alguns seguidores no início, sabendo disso, sempre adicionamos uma quantidade maior do que a comprada, na opção seguidores no Instagram.
  								</div>
  							</div>
  						</div>
  						<div class="card">
  							<div class="card-header" id="headingSix">
  								<h2 class="mb-0">
  									<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
  										Da entrega dos serviços:<i class="fas fa-plus"></i>
  									</button>
  								</h2>
  							</div>
  							<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#QuestionsRight">
  								<div class="card-body">
  									A entrega será considerada concluída após contagem inicial mais valor comprado ser atingido. NÃO compre simultaneamente conosco e outras empresas pois isso irá atrapalhar a contagem. NÃO nos responsabilizamos se você fizer isso e a sua compra conosco será considerada concluída quando atingir o valor da soma entre contagem inicial mais valor comprado. Só compre se concordar com essa questão.
  								</div>
  							</div>
  						</div>
  						<div class="card">
  							<div class="card-header" id="headingSeven">
  								<h2 class="mb-0">
  									<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
  										Políticas de privacidade:<i class="fas fa-plus"></i>
  									</button>
  								</h2>
  							</div>
  							<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#QuestionsRight">
  								<div class="card-body">
  									<p>Nossa empresa valoriza sua privacidade. Seguimos uma política de confidencialidade para lhe dizer como coletamos e usamos informações sobre você e o que fazemos para manter essa informação confidencial.</p>
  									<p>No site, não compartilhamos informações confidenciais que você nos forneceu. Além disso, nunca compartilhamos essas informações com terceiros, incluindo suas ordens.</p>Nós não divulgamos informações sobre sua revenda - todas as informações permanecem conosco.
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>

  				<div class="col-md-12 text-center my-4">
  					<img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/duvidas.png" style="max-width: 150px;" alt="">
  				</div>

  				<!-- <div class="col-md-6 col-xl-6">
  					<div class="card card-gray-dark collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
  						<div class="card-header">
  							<h3 class="card-title">Como adicionar saldo?</h3>

  							<div class="card-tools">
  								<button type="button" class="btn btn-tool" data-card-widget="collapse">
  									<i class="fas fa-plus" aria-hidden="true"></i>
  								</button>
  							</div>
  						</div>
  						<div class="card-body p-0">

  							<ul class="products-list product-list-in-card pl-2 pr-2">
  								<li class="item">
  									<span>Para adicionar saldo basta fazer depósito em uma de nossas contas bancárias e enviar o comprovante para ne seção “Recarga” na opção gerenciar comprovantes.</span>
  								</li>
  							</ul>

  						</div>
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<div class="card card-gray-dark collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
  						<div class="card-header">
  							<h3 class="card-title">Em quanto tempo recebo meu pedido?</h3>

  							<div class="card-tools">
  								<button type="button" class="btn btn-tool" data-card-widget="collapse">
  									<i class="fas fa-plus" aria-hidden="true"></i>
  								</button>
  							</div>
						  </div>
						  
  						<div class="card-body p-0">

  							<ul class="products-list product-list-in-card pl-2 pr-2">
  								<li class="item">
  									<span>Após a solicitação, o envio do serviço começará em no máximo 24h.</span>
  								</li>
  							</ul>

  						</div>
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<div class="card card-gray-dark collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
  						<div class="card-header">
  							<h3 class="card-title">Cancelamento do pedido</h3>

  							<div class="card-tools">
  								<button type="button" class="btn btn-tool" data-card-widget="collapse">
  									<i class="fas fa-plus" aria-hidden="true"></i>
  								</button>
  							</div>
						  </div>
						  
  						<div class="card-body p-0">

  							<ul class="products-list product-list-in-card pl-2 pr-2">
  								<li class="item">
  									<span>Após o pagamento dos serviços o envio é programado, NÃO havendo meios de cancelamento ou estorno.</span>
  								</li>
  							</ul>
  						</div>
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<div class="card card-gray-dark collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
  						<div class="card-header">
  							<h3 class="card-title">Posso utilizar o serviço em qualquer conta?</h3>

  							<div class="card-tools">
  								<button type="button" class="btn btn-tool" data-card-widget="collapse">
  									<i class="fas fa-plus" aria-hidden="true"></i>
  								</button>
  							</div>
						  </div>
						  
  						<div class="card-body p-0">

  							<ul class="products-list product-list-in-card pl-2 pr-2">
  								<li class="item">
  									<span>Sim! mas para a entrega do serviço é essencial o usuário manter o seu perfil "aberto" (modo público), do contrário o pedido será cancelado. Após a entrega, o usuário poderá colocar seu perfil em modo privado se preferir.</span>
  								</li>
  							</ul>
  						</div>
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<div class="card card-gray-dark collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
  						<div class="card-header">
  							<h3 class="card-title">É normal eu perder seguidores?</h3>

  							<div class="card-tools">
  								<button type="button" class="btn btn-tool" data-card-widget="collapse">
  									<i class="fas fa-plus" aria-hidden="true"></i>
  								</button>
  							</div>
						  </div>
						  
  						<div class="card-body p-0">

  							<ul class="products-list product-list-in-card pl-2 pr-2">
  								<li class="item">
  									<span>Sim. É normal perder alguns seguidores no início, sabendo disso, sempre adicionamos uma quantidade maior do que a comprada, na opção seguidores no Instagram.</span>
  								</li>
  							</ul>

  						</div>
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<div class="card card-gray-dark collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
  						<div class="card-header">
  							<h3 class="card-title">Da entrega dos serviços:</h3>

  							<div class="card-tools">
  								<button type="button" class="btn btn-tool" data-card-widget="collapse">
  									<i class="fas fa-plus" aria-hidden="true"></i>
  								</button>
  							</div>
						  </div>
						  
  						<div class="card-body p-0">

  							<ul class="products-list product-list-in-card pl-2 pr-2">
  								<li class="item">
  									<span>A entrega será considerada concluída após contagem inicial mais valor comprado ser atingido. NÃO compre simultaneamente conosco e outras empresas pois isso irá atrapalhar a contagem. NÃO nos responsabilizamos se você fizer isso e a sua compra conosco será considerada concluída quando atingir o valor da soma entre contagem inicial mais valor comprado. Só compre se concordar com essa questão.</span>
  								</li>
  							</ul>
  						</div>
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6">
  					<div class="card card-gray-dark collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
  						<div class="card-header">
  							<h3 class="card-title">Políticas de privacidade:</h3>

  							<div class="card-tools">
  								<button type="button" class="btn btn-tool" data-card-widget="collapse">
  									<i class="fas fa-plus" aria-hidden="true"></i>
  								</button>
  							</div>
						  </div>
						  
  						<div class="card-body p-0">

  							<ul class="products-list product-list-in-card pl-2 pr-2">
  								<li class="item">
  									<span>Nossa empresa valoriza sua privacidade. Seguimos uma política de confidencialidade para lhe dizer como coletamos e usamos informações sobre você e o que fazemos para manter essa informação confidencial.<br>No site, não compartilhamos informações confidenciais que você nos forneceu. Além disso, nunca compartilhamos essas informações com terceiros, incluindo suas ordens.<br>Nós não divulgamos informações sobre sua revenda - todas as informações permanecem conosco.</span>
  								</li>
  							</ul>
  						</div>
  					</div>
  				</div> -->

  			</div>

  		</div>

  		<style type="text/css" scoped>
  			.card {
  				border-radius: 10px !important;
  			}

  			.card-header {
  				padding: 5px !important;
  				background-color: #59277a;
  			}

  			.card-header button {
  				color: #fff;
  			}

  			.card-header button:hover {
  				color: #fff;
  			}

  			i.fas.fa-plus,
  			i.fas.fa-minus {
  				right: 0;
  				position: absolute;
  				padding: 5px;
  				margin: 0 0.5rem;
  			}

  			@media (max-width: 600px) {
  				.card-header button {
  					font-size: 0.9rem;
  				}
  			}
  		</style>

  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->