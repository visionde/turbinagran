<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <title>TurbinaGran | Recuperação de conta</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

    <!-- Custom style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/login-register.css">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.png" type="image/x-icon">

</head>

<body>
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup recover">

                <form method="post" id="recuperarSenha" action="<?php echo base_url(); ?>/register/recuperarSenha">
                    <h2 class="title">Recuperar conta</h2>
                    <div class="input-field">
                        <i class="fas fa-envelope"></i>
                        <input type="email" id="email" name="email" placeholder="E-mail" required="required" />
                    </div>
                    <button type="submit" class="btn">Enviar e-mail</button>
                </form>

            </div>
        </div>

        <div class="panels-container">

            <div class="panel left-panel">
                <div class="content">
                    <h3>Já recuperou a conta ?</h3>
                    <p>
                        Siga o passo-a-passo que estaremos enviando para você por e-mail e efetue o login com a nova
                        senha.
                    </p>
                    <a href="<?php echo base_url(); ?>welcome/login">
                        <button class="btn transparent">
                            Entrar
                        </button>
                    </a>
                </div>
                <img src="<?php echo base_url(); ?>assets/dist/img/recover.png" class="image" alt="" />
            </div>

        </div>
    </div>

</body>

</html>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.pt-br.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/actionRegister.js"></script>

<!--     <script type="text/javascript">
        $(document).ready(function() {
            urlpg = $(location).attr('href'); //pega a url atual da página
            urllimpa = urlpg.split("/welcome")[0] //tira tudo o que estiver depois de

            window.history.replaceState(null, null, urllimpa); //subtitui a url atual pela url limpa
        })
    </script> -->