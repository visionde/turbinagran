<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark" style="font-weight: 600;">Sistema de Afiliados</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Contas</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<style>

		.mb-3 {
			width: 45%;
		}

		@media (max-width: 600px) {

		.mb-3 {
			width: 100%;
		}
	}
	</style>

	<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
		<div class="col-md-12"><br>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
			</div>
		</div>
	<?php } else if ($this->session->flashdata('success')) { ?>
		<div class="col-md-12">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
			</div>
		</div>
	<?php } else if ($this->session->flashdata('warning')) { ?>
		<div class="col-md-12">
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5></i> <?php echo $this->session->flashdata('warning'); ?></h5>
			</div>
		</div>
	<?php } ?>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">

			<div class="card card-pink">
				<div class="card-header">
					<h3 class="card-title">Como Funciona</h3>
					<i class="fas fa-bullhorn" style="position: absolute; right: 2%; font-size: 25px;"></i>
				</div>

				<div class="col-md-12" style=" border-radius: 25px;">
					<hr><p><h6><strong> IMPORTANTE:</strong> Indique nosso site para seus amigos e ganhe créditos.</h6></p>
                     - Você ganhará <?php echo $comissao[0]->indicacao_parametro ?>% de todo o valor adicionado por seus amigos.</br>
                     - Você estará ajudando nosso site a crescer e oferecer um melhor serviço e menor preço.</br>
                     - Todo processo é realizado de forma automática, basta indicar e ganhar.
					<hr>
                    <div class="product-info">
                        <p><h6><strong> SEU LINK DE INDICAÇÃO</h6></p></strong>
                    </div>
					<div class="input-group mb-3">
						<input class="form-control" id="link_input" type="text" value="<?php echo base_url().'welcome/register/?ref='.$this->session->userdata('usuario_id')?>" placeholder="">
						<div class="input-group-append">
						    <button id="btn-copiar" class="btn btn-success"> Copiar </button>
						</div>
					</div>
					<p>Basta enviar o link acima para seus amigos e começar a receber.</p>
				</div>

			</div>

			<div class="card card-pink">
				<div class="card-header">
					<h3 class="card-title">Liberação de Saldo</h3>
				</div>
				<div class="card-body p-0">
					<table class="table table-bordered table-hover">
						<th class="text-center">Saldo Atual</th>
						<th class="text-center">Saldo Bônus</th>
						<th class="text-center">Transferir para saldo TurbinaGran</th>
						<th class="text-center">Transferir Pix</th>
						<tbody>

						<td class="text-center"><?php echo $this->session->userdata('saldo') ?></td>
						<td class="text-center"><?php echo $this->session->userdata('bonus') ?></td>
						<td class="text-center">
						    <a style="cursor:pointer;" title="Validar" data-toggle="modal" data-target="#validarSaldo"><button class="btn btn-success">Transferir</button></a>
						</td>
						<td class="text-center">
					    	<a style="cursor:pointer;" title="Validar" data-toggle="modal" data-target="#validarSaldoPix"><button class="btn btn-success">Transferir</button></a>
						</td>
						</tbody>
					</table>
				</div>
			</div>

			<div class="card card-pink">
				<div class="card-header">
					<h3 class="card-title">Suas indicações</h3>
					<i class="fas fa-file-alt" style="position: absolute; right: 2%; font-size: 25px;"></i>
				</div>

                <div class="col-md-12">
  							<div class="nav-tabs-custom">
  								<div class="tab-content">

  									<div class="tab-pane active" id="tab_1">

  										<div class="card-body">

  											<table id="example1" class="table table-bordered table-hover">
  												<thead>
  													<tr>
  														<th>Usuário</th>
  														<th>Data</th>
  														<th>Comissão</th>
														<th>Status</th>
  													</tr>
  												</thead>
  		
  												<tbody>
												<?php foreach ($indicacoes as $i) { ?>
													<tr>
														<td class="text-center"><?php echo $i->usuario_nome ?></td>
														<td class="text-center"><?php echo date("d/m/Y", strtotime($i->indicacao_data)); ?></td>
														<td class="text-center"><?php echo $i->indicacao_valor ?></td>
														<?php if ($i->indicacao_status == "0") { ?>
															<td class="text-center">Transferido</td>
														<?php } elseif ($i->indicacao_status == "1") { ?>
															<td class="text-center">Aberto</td>
														<?php }else { ?>
															<td class="text-center">Em Análise</td>
														<?php } ?>
													</tr>
												<?php } ?>
  											</table>

  										</div>

  									</div>

  								</div>
  							</div>
  						</div>
            
            </div>
				

					

		</div>
		<!--/. container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div class="modal fade" id="validarSaldo">
  	<div class="modal-dialog modal-dialog-centered">
  		<div class="modal-content">
  			<div class="modal-body">
  				<div class="content">

  					<form role="form" id="validarSaldoForm" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/tranformarSaldo" method="post" enctype="multipart/form-data">


  						<div class="form-group">
  							<label>Tem certeza que deseja transferir o bônus em saldo ?</label>
  						</div>

  				</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  				<button type="submit" class="btn btn-success">Transferir</button>
  			</div>
  			</form>
  		</div>
  		<!-- /.modal-content -->
  	</div>
  	<!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <div class="modal fade" id="validarSaldoPix">
  	<div class="modal-dialog modal-dialog-centered">
  		<div class="modal-content">
  			<div class="modal-body">
  				<div class="content">

  					<form role="form" id="validarSaldoPixForm" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/tranformarSaldoPix" method="post" enctype="multipart/form-data">


  						<div class="form-group">
  							<label>Tem certeza que deseja receber o bônus via pix ?</label>
  						</div>
						<div class="form-group">
  							<center>
								<label style="font-size: 28px;">Chave PIX:</label>
								<p style="color: green;">Valor mínimo para saque R$ 10,00</p>
								<input type="text" class="col-md-9 form-control" name="modal_valor" id="modal_valor" required="required"></input>
							</center>
  						</div>

  				</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  				<button type="submit" class="btn btn-success">Transferir</button>
  			</div>
  			</form>
  		</div>
  		<!-- /.modal-content -->
  	</div>
  	<!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->



<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";
	var perfil = "<?php echo $this->session->userdata('perfil_id'); ?>";

	// FUNCAO COPIAR
	function copiar () {
		const
		ele = document.querySelector ('input'),
		ele_id = document.getElementById ('link_input');
		ele.select();
		document.execCommand ('copy');
	};
	const
	btn_copiar = document.getElementById ('btn-copiar');
	btn_copiar.addEventListener ('click', copiar);
</script>
