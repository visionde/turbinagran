  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Gestão de Pix</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Gestão de Pix</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->


  	<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
  		<div class="col-md-12">
  			<div class="alert alert-danger alert-dismissible">
  				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
  			</div>
  		</div>
  	<?php } else if ($this->session->flashdata('success')) { ?>
  		<div class="col-md-12">
  			<div class="alert alert-success alert-dismissible">
  				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
  			</div>
  		</div>
  	<?php } ?>

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="row">
  				<div class="col-md-12 col-xl-12">
  					<div class="card card-info">
  						<div class="card-header">
  							<h3 class="card-title">Liberação de Pix</h3>
  						</div>
  						<div class="card-body p-0">
  							<table id="gestao-saldo-desktop" class="table table-striped">
  								<th class="text-center">Usuario</th>
  								<th class="text-center">Pix</th>
  								<th class="text-center">Bônus</th>
  								<th class="text-center">Ação</th>
  								<tbody>
  									<?php foreach ($dados as $i) { ?>
  										<tr>
  											<td class="text-center"><?php echo $i->usuario_nome ?></td>
  											<td class="text-center"><?php echo $i->pix_valor ?></td>
  											<td class="text-center"><?php echo $i->pix_bonus ?></td>
  											<td class="text-center">
  												<a style="cursor:pointer;" title="Invalidar" data-toggle="modal" data-target="#invalidarPixModal" onclick="invalidarModal('<?php echo base_url() . $this->uri->segment(1) . "/InvalidarPix/" . $i->pix_id . "/" . $i->usuario_id ?>')"><i class="fas fa-times  text-danger"></i></a>&nbsp;&nbsp;
  												<a style="cursor:pointer;" title="Validar" data-toggle="modal" data-target="#validarPixModal" onclick="validarModal('<?php echo base_url() . $this->uri->segment(1) . "/ValidarPix/" . $i->pix_id . "/" . $i->usuario_id ?>');"><i class="fas fa-check  text-success"></i></a>
  											</td>
  										</tr>
  									<?php } ?>
  								</tbody>
  							</table>

  							<table id="gestao-saldo-mobile" class="table table-striped">
  								<tbody>
  									<?php foreach ($dados as $i) { ?>
  										<tr class="lista-gestao">
  											<td class="text-center"><span>Usuário</span><?php echo $i->usuario_nome ?></td>
  											<td class="text-center"><span>Pix</span><?php echo $i->pix_valor ?></td>
  											<td class="text-center"><span>Bônus</span><span class="saldo-result">R$ <?php echo $i->pix_bonus ?></span></td>
  											<td class="text-center"><span>Ação</span>
  												<span>
  													<a class="pr-2" style="cursor:pointer;" title="Invalidar" data-toggle="modal" data-target="#invalidarPixModal" onclick="invalidarModal('<?php echo base_url() . $this->uri->segment(1) . "/InvalidarPix/" . $i->pix_id . "/" . $i->usuario_id ?>')"><i class="fas fa-times text-danger"></i></a>&nbsp;&nbsp;
  													<a style="cursor:pointer;" title="Validar" data-toggle="modal" data-target="#validarPixModal" onclick="validarModal('<?php echo base_url() . $this->uri->segment(1) . "/ValidarPix/" . $i->pix_id . "/" . $i->usuario_id ?>');"><i class="fas fa-check  text-success"></i></a>
  												</span>
  											</td>
  										</tr>
  									<?php } ?>
  								</tbody>
  							</table>
  						</div>
  					</div>
  				</div>
  			</div>

  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	#gestao-saldo-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {
  		#gestao-saldo-mobile {
  			display: block;
  		}

  		#gestao-saldo-desktop {
  			display: none;
  		}

  		#gestao-saldo-mobile {
  			display: flex;
  			flex-direction: column;
  		}

  		#gestao-saldo-mobile .lista-gestao {
  			display: flex;
  			flex-direction: column;
  			margin-bottom: 20px;
  		}

  		#gestao-saldo-mobile .lista-gestao .text-center {
  			display: flex;
  			justify-content: space-between;
  		}

  		#gestao-saldo-mobile .lista-gestao .text-center span {
  			font-weight: bold;
  		}

  		.lista-gestao .saldo-result {
  			font-weight: 500 !important;
  		}

  		.card-body.p-0 .table tbody>tr>td:first-of-type,
  		.card-body.p-0 .table tbody>tr>th:first-of-type,
  		.card-body.p-0 .table thead>tr>td:first-of-type,
  		.card-body.p-0 .table thead>tr>th:first-of-type {
  			padding-left: 12px;
  		}
  	}
  </style>


  <!-- Modal Excluir registros -->
  <div class="example-modal">
  	<div class="modal" id="invalidarPixModal">
  		<div class="modal-dialog modal-dialog-centered">
  			<div class="modal-content">
  				<div class="modal-body">
  					<p id="pixInvalidar">Deseja realmente invalidar este Pix ?</p>
  				</div>
  				<div class="modal-footer">
  					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  					<a href="" class="btn btn-danger" id="invalidarIdPix">Invalidar</a>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <!-- Fim modal Excluir registros -->


    <!-- Modal Excluir registros -->
 <div class="example-modal">
  	<div class="modal" id="validarPixModal">
  		<div class="modal-dialog modal-dialog-centered">
  			<div class="modal-content">
  				<div class="modal-body">
  					<p id="pixValidar">Deseja realmente validar este Pix ?</p>
  				</div>
  				<div class="modal-footer">
  					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  					<a href="" class="btn btn-success" id="validarIdPix">Validar</a>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <!-- Fim modal Excluir registros -->





  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <script type="text/javascript">
  	function invalidarModal(url) {
  		$('#pixInvalidar').html('<p>Deseja realmente invalidar este Pix ?</p>');
  		$('#invalidarIdPix').attr('href', url);
  	}

	  function validarModal(url) {
  		$('#pixValidar').html('<p>Deseja realmente validar este Pix ?</p>');
  		$('#validarIdPix').attr('href', url);
  	}
  </script>