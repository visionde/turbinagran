<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark" style="font-weight: 600;">Parâmetro Indicação</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Parâmetro Indicação</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
		<div class="col-md-12"><br>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
			</div>
		</div>
	<?php } else if ($this->session->flashdata('success')) { ?>
		<div class="col-md-12">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
			</div>
		</div>
	<?php } else if ($this->session->flashdata('warning')) { ?>
		<div class="col-md-12">
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5></i> <?php echo $this->session->flashdata('warning'); ?></h5>
			</div>
		</div>
	<?php } ?>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
            <div class="card card-pink">
                <div class="card-header">
                    <h3 class="card-title">Comissão</h3>
                    <i class="fas fa-wallet" style="position: absolute; right: 2%; font-size: 25px;"></i>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0"><br>
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                            <div class="product-img banco">
                                <i class="mercadopago"></i>
                            </div>
                            <div  class="product-info">
                                <form class="form-inline" method="post" action="<?php echo base_url(); ?>indicacao/comissao">
                                    <div class="form-group mr-2">
                                        <input type="text" class="form-control" name="comissao" value="<?php echo $comissao[0]->indicacao_parametro ?>" autocomplete="off" required="required">
                                    </div>
                                    <input class="btn btn-primary" type="submit"  value="Atualizar">
                                </form>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
         </div>
    </section>
</div>