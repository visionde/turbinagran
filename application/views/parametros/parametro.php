  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Parâmetros</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Parâmetros</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">

  			<div class="row">

  				<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
  					<div class="col-md-12"><br>
  						<div class="alert alert-danger alert-dismissible">
  							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  							<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
  						</div>
  					</div>
  				<?php } else if ($this->session->flashdata('success')) { ?>
  					<div class="col-md-12">
  						<div class="alert alert-success alert-dismissible">
  							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  							<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
  						</div>
  					</div>
  				<?php } ?>

  				<div class="col-md-6 col-xl-6">
  					<div class="card card-success">
  						<div class="card-header">
  							<h3 class="card-title">Escolher Parâmetro do sistema</h3>
  							<i class="fas fa-sliders-h" style="position: absolute; right: 2%; font-size: 25px;"></i>
  						</div>
  						<div class="card-body">
  							<!-- Minimal style -->
  							<div class="row">

  								<?php foreach ($parametros as $p) {

										if ($p->parametro_check == '1' and $p->parametro_id != '3') { ?>

  										<div class="col-sm-6">
  											<div class="form-group clearfix">
  												<div class="icheck-success d-inline">
  													<input type="checkbox" checked name="sistema" data="<?php echo $p->parametro_id ?>" id="checkboxSuccess<?php echo $p->parametro_id ?>">
  													<label for="checkboxSuccess<?php echo $p->parametro_id ?>"><?php echo $p->parametro_nome ?>
  													</label>
  												</div>
  											</div>
  										</div>

  									<?php } elseif ($p->parametro_id == '3') { ?>

  										<div class="col-sm-6">
  											<div class="form-group clearfix">
  												<div class="icheck-success d-inline">
  													<input type="checkbox" checked disabled="disabled" name="sistema" data="<?php echo $p->parametro_id ?>" id="checkboxSuccess<?php echo $p->parametro_id ?>">
  													<label for="checkboxSuccess<?php echo $p->parametro_id ?>"><?php echo $p->parametro_nome ?>
  													</label>
  												</div>
  											</div>
  										</div>

  									<?php } else { ?>

  										<div class="col-sm-6">
  											<div class="form-group clearfix">
  												<div class="icheck-success d-inline">
  													<input type="checkbox" name="sistema" data="<?php echo $p->parametro_id ?>" id="checkboxSuccess<?php echo $p->parametro_id ?>">
  													<label for="checkboxSuccess<?php echo $p->parametro_id ?>"><?php echo $p->parametro_nome ?>
  													</label>
  												</div>
  											</div>
  										</div>

  								<?php }
									} ?>

  							</div>
  						</div>
  					</div>
  					<div>
  						<img class="img-fluid parametros" src="<?php echo base_url(); ?>assets/dist/img/parametros.png" alt="Parâmetros">
  					</div>
  				</div>

  				<div class="col-md-6 col-xl-6" id="configSistema">
  				</div>
  			</div>

  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	@media (max-width: 768px) {
  		.parametros {
  			display: none;
  		}
  	}
  </style>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  <!-- jQuery -->
  <script type="text/javascript">
  	var base_url = "<?php echo base_url(); ?>";
  </script>
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>

  <script type="text/javascript">
  	$(document).ready(function() {
  		ids = [];

  		$("input[name=sistema]:checked").each(function() {
  			ids.push($(this).attr("data"));
  		});

  		$.ajax({
  				method: "POST",
  				url: base_url + "parametros/buscarPararametroIdD/",
  				dataType: "html",
  				data: {
  					id: ids
  				}
  			})
  			.done(function(data) {
  				$('#configSistema').html(data);
  			});
  	});
  </script>

  <script type="text/javascript">
  	$("[name=sistema]").change(function() {

  		ids = [];

  		$("input[name=sistema]:checked").each(function() {
  			ids.push($(this).attr("data"));
  		});

  		$.ajax({
  				method: "POST",
  				url: base_url + "parametros/atualizarParametro/",
  				dataType: "JSON",
  				data: {
  					id: ids
  				}
  			})
  			.done(function(response) {

  			});


  		$.ajax({
  				method: "POST",
  				url: base_url + "parametros/buscarPararametroIdD/",
  				dataType: "html",
  				data: {
  					id: ids
  				}
  			})
  			.done(function(data) {
  				$('#configSistema').html(data);
  			});

  	});
  </script>