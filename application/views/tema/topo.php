<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta https-equiv="X-UA-Compatible" content="IE=edge">
	<title>TurbinaGran | <?php echo ucfirst($this->uri->segment(1)); ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/styles.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.png" type="image/x-icon">
	<!-- Facebook Pixel Code -->
    <script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init', '166101931768882');fbq('track', 'PageView');</script>
    <noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=166101931768882&ev=PageView&noscript=1"/></noscript>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<!-- Site wrapper -->
	<div class="wrapper">
		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand navbar-dark navbar-pink">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a id="menu-icon" class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-chevron-left" style="font-size: 1.2rem;"></i></a>
					<a id="menu-icon-mobile" class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars" style="font-size: 1.2rem;"></i></a>
				</li>
			</ul>
			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">
				<!-- Messages Dropdown Menu -->
				<!--  <li class="nav-item dropdown">
				<a class="nav-link" data-toggle="dropdown" href="#">
				<i class="far fa-comments"></i>
				<span class="badge badge-danger navbar-badge">3</span>
				</a>
				<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
				<a href="#" class="dropdown-item">
					<div class="media">
					<img src="<?php echo base_url(); ?>assets/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
					<div class="media-body">
						<h3 class="dropdown-item-title">
						Brad Diesel
						<span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
						</h3>
						<p class="text-sm">Call me whenever you can...</p>
						<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
					</div>
					</div>
				</a>
				<div class="dropdown-divider"></div>
				<a href="#" class="dropdown-item">

					<div class="media">
					<img src="<?php echo base_url(); ?>assets/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
					<div class="media-body">
						<h3 class="dropdown-item-title">
						John Pierce
						<span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
						</h3>
						<p class="text-sm">I got your message bro</p>
						<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
					</div>
					</div>

				</a>
				<div class="dropdown-divider"></div>
				<a href="#" class="dropdown-item">

					<div class="media">
					<img src="<?php echo base_url(); ?>assets/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
					<div class="media-body">
						<h3 class="dropdown-item-title">
						Nora Silvester
						<span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
						</h3>
						<p class="text-sm">The subject goes here</p>
						<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
					</div>
					</div>

				</a>
				<div class="dropdown-divider"></div>
				<a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
				</div>
				</li> -->
				<!-- Notifications Dropdown Menu -->
				<!-- <li class="dropdown user user-menu">

				<a class="nav-link" href="https://wa.me/5581981469522" target="_blank">
					<img style="border-radius: 0" src="<?php echo base_url(); ?>assets/dist/img/zapicon.png" class="user-image" alt="User Image">
				</a>
				</li> -->
				<li id="profile-desktop" class="dropdown user user-menu">

					<a class="nav-link" data-toggle="dropdown" href="#">
						<span class="hidden-xs pr-2"><?php echo strtoupper($this->session->userdata('usuario_nome')); ?></span>
						<img style="border-radius: 0" src="<?php echo base_url(); ?>assets/dist/img/favicon.png" class="user-image" alt="User Image">
					</a>

					<!--
					<ul class="dropdown-menu">
						<!-- User image -->
					<!-- <li class="user-header"> -->
					<!-- <img src="<?php echo base_url(); ?>assets/dist/img/logo-quadrado.png" class="img-circle" alt="User Image"> -->

					<!--
						<p>
							<small><?php echo $this->session->userdata('perfil_nome'); ?></small>
							<?php echo strtoupper($this->session->userdata('usuario_nome')); ?>
						</p>
					

				</li>
				<li class="user-footer">
					<div class="pull-right">
						<!--<a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-default btn-flat">Sair</a>-->
					<!--
						<a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-danger" style=" border-radius: 10px; background: #B92786; border: none;">Sair
							<i class="nav-icon fas fa-sign-out-alt"></i>
						</a>
					</div>
				</li>
				-->
			</ul>

			</li>

			<div class="logout-desktop">
				<a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-danger" style=" border-radius: 10px; background: #59277a; border: none;">Sair
					<i class="nav-icon fas fa-sign-out-alt"></i>
				</a>
			</div>

			<div id="profile-mobile" class="dropdown">
				<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img class="profile dropdown-toggle" style="width: 40px;" role="button" id="dropdownMenuLink" src="<?php echo base_url(); ?>assets/dist/img/favicon.png">
					<i class="fas fa-sort-down" style="color: #fff"></i>
				</a>

				<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					<a class="dropdown-item" href="#"><small class=" mr-2"><?php echo $this->session->userdata('perfil_nome'); ?></small><?php echo strtoupper($this->session->userdata('usuario_nome')); ?></a>
					<a href="<?php echo base_url(); ?>welcome/logout" class="dropdown-item btn btn-danger text-center" style=" border-radius: 10px; background: #B92786; border: none; color: #fff; max-width: 100px; margin: 0 auto;">Sair
						<i class="nav-icon fas fa-sign-out-alt"></i>
					</a>
				</div>
			</div>
			<!--       <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li> -->
			</ul>
		</nav>
		<!-- /.navbar -->

		<style>
			#menu-icon-mobile {
				display: none;
			}

			#profile-mobile {
				display: none;
				align-items: center;
			}

			@media (max-width: 576px) {
				#menu-icon {
					display: none;
				}

				#menu-icon-mobile {
					display: block;
				}

				#profile-desktop {
					display: none;
				}

				.logout-desktop {
					display: none;
				}

				#profile-mobile {
					display: flex;
				}

				.dropdown-menu {
					padding: 10px;
				}
			}
		</style>