  <!-- Main Sidebar Container -->
  <script src="https://kit.fontawesome.com/044e5ae942.js" crossorigin="anonymous"></script>
  <aside class="main-sidebar sidebar-dark-pink elevation-4">
  	<!-- Brand Logo -->
  	<a href="<?php echo base_url(); ?>welcome" class="brand-link">
  		<img src="<?php echo base_url(); ?>assets/dist/img/favicon.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
  		<span class="brand-text font-weight-bold">TurbinaGran</span>
  	</a>

  	<!-- Sidebar -->
  	<div class="sidebar">
  		<!-- Sidebar user (optional) -->
  		<!--       <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>assets/dist/img/AdminLTELogo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo strtoupper($this->session->userdata('usuario_nome')); ?></a>
        </div>
      </div> -->

  		<!-- Sidebar Menu -->
  		<nav class="mt-2">
  			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
  				<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>welcome/dashboard" class="nav-link ts">
  						<!--<i class="nav-icon fas fa-tachometer-alt"></i>-->
  						<i class="nav-icon fas fa-home"></i>
  						<p>
  							Início
  						</p>
  					</a>
  				</li>
<!--   				<?php if ($this->session->userdata('perfil_id') == '2' or $this->session->userdata('perfil_id') == '1') {  ?>
  					<li class="nav-item">
  						<a href="<?php echo base_url(); ?>curso" class="nav-link ts">
  							<i class="nav-icon fab fas fa-book"></i>
  							<p>
  								Curso
  							</p>
  						</a>
  					</li>
  				<?php } ?> -->
  				<li class="nav-item has-treeview contas">
  					<a href="#" class="nav-link tscontas">
  						<i class="nav-icon fas fa-search-dollar"></i>
  						<p>
  							Recarga R$ <?php echo $this->session->userdata('saldo'); ?>
  							<!--<i class="fas fa-angle-left right"></i>-->
  							<i class="fas fa-caret-down right"></i>
  						</p>
  					</a>
  					<ul class="nav nav-treeview">
  						<li class="nav-item">
  							<a href="<?php echo base_url(); ?>contas" class="nav-link ts">
  								<!-- <i class="far fa-circle nav-icon"></i> -->
  								<i class="fas fa-hand-holding-usd nav-icon"></i>
  								<p>Efetuar Recarga</p>
  							</a>
  						</li>
  						<?php if ($this->session->userdata('perfil_id') == '1') {  ?>
  							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>contas/gestao" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-cash-register nav-icon"></i>
  									<p>Gestão de saldo</p>
  								</a>
  							</li>
  						<?php } ?>
  					</ul>
  				</li>
				<?php if ($this->session->userdata('perfil_id') == '1') {  ?>

					<li class="nav-item has-treeview indicacao">
						<a href="#" class="nav-link tsindicacao">
							<i class="nav-icon fas fa-comments-dollar"></i>
							<p>
							    Indicação R$ <?php echo $this->session->userdata('bonus'); ?>
								<i class="fas fa-caret-down right"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>indicacao" class="nav-link ts">
									<i class="fas fa-comments-dollar nav-icon"></i>
									<p>Minhas Indicações</p>
								</a>
							</li>
							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>indicacao/gestaopix" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-cash-register nav-icon"></i>
  									<p>Gestão de Pix</p>
  								</a>
  							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>indicacao/parametro" class="nav-link ts">
									<i class="fas fa-cogs nav-icon"></i>
									<p>Parâmetros</p>
								</a>
							</li>
						</ul>
					</li>

				<?php }else{ ?>	
					<li class="nav-item">
						<a href="<?php echo base_url(); ?>indicacao" class="nav-link ts">
							<i class="nav-icon fas fa-comments-dollar"></i>
							<p>
								Indicação R$ <?php echo $this->session->userdata('bonus'); ?>
							</p>
						</a>
					</li>
				<?php } ?>
  				<li class="nav-item has-treeview servico">
  					<a href="#" class="nav-link tsservico">
  						<i class="nav-icon fas fa-paste"></i>
  						<p>
  							Serviços
  							<!--<i class="fas fa-angle-left right"></i>-->
  							<i class="fas fa-caret-down right"></i>
  						</p>
  					</a>
  					<ul class="nav nav-treeview">
  						<li class="nav-item">
  							<a href="<?php echo base_url(); ?>servico" class="nav-link ts">
  								<!-- <i class="far fa-circle nav-icon"></i> -->
  								<i class="fas fa-list-ul nav-icon"></i>
  								<p>Lista de serviços </p>
  							</a>
  						</li>
  						<?php if ($this->session->userdata('perfil_id') == '1') {  ?>
  							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>servico/gestao" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-users-cog nav-icon"></i>
  									<p>Gestão de serviços</p>
  								</a>
  							</li>
  							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>servico/gestaoManual" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-user-cog nav-icon"></i>
  									<p>Serviços Manual Gestão</p>
  								</a>
  							</li>
  						<?php } ?>
  					</ul>
  				</li>
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>instagram" class="nav-link ts">
  						<i class="nav-icon fab fa-instagram"></i>
  						<p>
  							Instagram
  						</p>
  					</a>
  				</li>
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>facebook" class="nav-link ts">
  						<!--<i class="nav-icon fab fa-facebook"></i>-->
  						<i class="nav-icon fab fa-facebook-f"></i>
  						<p>
  							Facebook
  						</p>
  					</a>
  				</li>
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>tiktok" class="nav-link ts">
  						<i class="nav-icon fab fa-tiktok"></i>
  						<p>
  							Tiktok
  						</p>
  					</a>
  				</li>
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>youtube" class="nav-link ts">
  						<i class="nav-icon fab fa-youtube"></i>
  						<p>
  							Youtube
  						</p>
  					</a>
  				</li>
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>twitter" class="nav-link ts">
  						<i class="nav-icon fab fa-twitter"></i>
  						<p>
  							Twitter
  						</p>
  					</a>
  				</li>
  				<li class="nav-item has-treeview ordem">
  					<a href="#" class="nav-link tsordem">
  						<i class="nav-icon fas fa-boxes"></i>
  						<p>
  							Histórico
  							<!--<i class="fas fa-angle-left right"></i>-->
  							<i class="fas fa-caret-down right"></i>
  						</p>
  					</a>
  					<ul class="nav nav-treeview ordem">
  						<li class="nav-item">
  							<a href="<?php echo base_url(); ?>ordem" class="nav-link ts">
  								<!-- <i class="far fa-circle nav-icon"></i> -->
  								<i class="far fa-list-alt nav-icon"></i>
  								<p>Ordens</p>
  							</a>
  						</li>
  						<li class="nav-item">
  							<a href="<?php echo base_url(); ?>ordem/saldos" class="nav-link ts">
  								<!-- <i class="far fa-circle nav-icon"></i> -->
  								<i class="fas fa-coins nav-icon"></i>
  								<p>Saldos</p>
  							</a>
  						</li>
  					</ul>
  				</li>
  				<li class="nav-item has-treeview suporte">
  					<a href="#" class="nav-link tssuporte">
  						<!-- <i class="nav-icon fas fa-clipboard-list"></i> -->
  						<i class="fas fa-headset nav-icon"></i>
  						<p>
  							Suporte
  							<i class="fas fa-caret-down right"></i>
  						</p>
  					</a>
  					<ul class="nav nav-treeview">
  						<li class="nav-item">
  							<a href="<?php echo base_url(); ?>suporte" class="nav-link ts">
  								<!-- <i class="far fa-circle nav-icon"></i> -->
  								<i class="fas fa-ticket-alt nav-icon"></i>
  								<p>Abrir Ticket</p>
  							</a>
  						</li>
  						<li class="nav-item">
  							<a href="<?php echo base_url(); ?>suporte/abertos" class="nav-link ts">
  								<!-- <i class="far fa-circle nav-icon"></i> -->
  								<i class="fas fa-check-circle nav-icon"></i>
  								<p>Abertos</p>
  							</a>
  						</li>
  						<li class="nav-item">
  							<a href="<?php echo base_url(); ?>suporte/fechados" class="nav-link ts">
  								<!-- <i class="far fa-circle nav-icon"></i> -->
  								<i class="fas fa-times-circle nav-icon"></i>
  								<p>Fechados</p>
  							</a>
  						</li>
  						<?php if ($this->session->userdata('perfil_id') == '1') {  ?>
  							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>suporte/gestaoticket" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-clipboard-list nav-icon"></i>
  									<p>Gestão de Tickets</p>
  								</a>
  							</li>
  						<?php } ?>
  					</ul>
  				</li>
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>perguntas" class="nav-link ts">
  						<i class="nav-icon fas fa-hands-helping"></i>
  						<p>
  							Ajuda
  						</p>
  					</a>
  				</li>
  				<li class="nav-item">
  					<a href="<?php echo base_url(); ?>contato" class="nav-link ts">
  						<!--<i class="nav-icon fas fa-phone-square-alt"></i>-->
  						<i class="nav-icon fas fa-phone-alt"></i>
  						<p>
  							Contato
  						</p>
  					</a>
  				</li>
  				<?php if ($this->session->userdata('perfil_id') == '1') {  ?>
  					<li class="nav-item">
  						<a href="<?php echo base_url(); ?>perfis" class="nav-link ts">
  							<i class="nav-icon fas fa-users"></i>
  							<p>
  								Perfis
  							</p>
  						</a>
  					</li>
  				<?php } ?>
  				<?php if ($this->session->userdata('perfil_id') == '1') {  ?>
  					<li class="nav-item">
  						<a href="<?php echo base_url(); ?>parametros" class="nav-link ts">
  							<i class="nav-icon fas fa-cogs"></i>
  							<p>
  								Parâmetros
  							</p>
  						</a>
  					</li>
  				<?php } ?>
  				<?php if ($this->session->userdata('perfil_id') == '1') {  ?>
  					<li class="nav-item has-treeview relatorios">
  						<a href="#" class="nav-link tsrelatorios">
  							<i class="nav-icon fa fa-files-o"></i>
  							<p>
  								Relatórios
  								<i class="fas fa-caret-down right"></i>
  							</p>
  						</a>
  						<ul class="nav nav-treeview">
  							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>relatorios/usuario" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-users nav-icon"></i>
  									<p>Usuários Ativos</p>
  								</a>
  							</li>
  							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>relatorios/financeiro" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-file-invoice-dollar nav-icon"></i>
  									<p>Financeiro</p>
  								</a>
  							</li>
  							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>relatorios/ordens" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="fas fa-boxes nav-icon"></i>
  									<p>Ordens</p>
  								</a>
  							</li>
							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>relatorios/bonus" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="nav-icon fas fa-comments-dollar"></i>
  									<p>Indicação</p>
  								</a>
  							</li>
							<li class="nav-item">
  								<a href="<?php echo base_url(); ?>relatorios/bonuspix" class="nav-link ts">
  									<!-- <i class="far fa-circle nav-icon"></i> -->
  									<i class="nav-icon fas fa-comments-dollar"></i>
  									<p>Indicação Pix</p>
  								</a>
  							</li>
  						</ul>
  					</li>
  				<?php } ?>

  			</ul>
  		</nav>

  		<!--
      <nav class="mt-2" style="display:flex; justify-content: center;">
        <ul id="sidebar-out" class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>welcome/logout" class="nav-link btn btn-danger" style="border-radius: 10px; background: #B92786; border: none; padding: 5px 20px;">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Sair
              </p>
            </a>
          </li>

        </ul>
      </nav>
          -->
  		<!-- /.sidebar-menu -->
  	</div>
  	<!-- /.sidebar -->
  </aside>

  <style>
  	#sidebar-out {
  		position: absolute;
  		bottom: 5%;
  	}

  	@media (max-width: 600px) {
  		#sidebar-out {
  			bottom: 10% !important;
  		}
  	}

  	@media (min-width: 992px) {

  		.sidebar-mini.sidebar-collapse .main-sidebar.sidebar-focused .brand-text,
  		.sidebar-mini.sidebar-collapse .main-sidebar.sidebar-focused .logo-xl,
  		.sidebar-mini.sidebar-collapse .main-sidebar.sidebar-focused .nav-sidebar .nav-link p,
  		.sidebar-mini.sidebar-collapse .main-sidebar.sidebar-focused .user-panel>.info,
  		.sidebar-mini.sidebar-collapse .main-sidebar:hover .brand-text,
  		.sidebar-mini.sidebar-collapse .main-sidebar:hover .logo-xl,
  		.sidebar-mini.sidebar-collapse .main-sidebar:hover .nav-sidebar .nav-link p,
  		.sidebar-mini.sidebar-collapse .main-sidebar:hover .user-panel>.info {
  			margin-right: 25px;
  		}
  	}
  </style>