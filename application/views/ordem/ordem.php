  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Minhas Ordens</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Minhas Ordens</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="row">
  			<div class="col-md-12">
  				<!-- Horizontal Form -->
  				<div class="box box-primary">
  					<div class="card">
  						<div class="card-header">
  							<h3 class="card-title">
  								<i class="fas fa-boxes mr-1"></i>
  								Ordens
  							</h3>
  							<div class="card-tools">
  								<ul class="nav nav-pills ml-auto">
  									<li class="nav-item">
  										<a class="nav-link active" href="#tab_1" data-toggle="tab">Total</a>
  									</li>
  									<li class="nav-item">
  										<a class="nav-link" href="#tab_2" data-toggle="tab">Pendentes</a>
  									</li>
  									<li class="nav-item">
  										<a class="nav-link" href="#tab_3" data-toggle="tab">Concluidas</a>
  									</li>
  								</ul>
  							</div>
  						</div>

  						<div class="col-md-12">
  							<div class="nav-tabs-custom">
  								<div class="tab-content">

  									<div class="tab-pane active" id="tab_1">

  										<div class="card-body">

  											<table id="example1" class="table table-bordered table-hover">
  												<thead>
  													<tr>
  														<th>Serviço</th>
  														<th>Data</th>
  														<th>QTD/Valor</th>
  														<th>Link</th>
  														<th>Status</th>
  													</tr>
  												</thead>
  												<?php foreach ($total as $v) { ?>
  													<tr>
  														<td><?php echo $v->servico_nome; ?></td>
  														<td><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-',$v->ordem_data))); ?></td>
  														<td><?php echo $v->ordem_quantidade . ' / R$ ' . str_replace('.', ',', $v->ordem_valor); ?></td>
  														<td><?php echo $v->ordem_link; ?></td>
  														<td><?php echo $v->ordem_status; ?></td>
  													</tr>
  												<?php } ?>
  												<tbody>
  													</tbfoot>
  											</table>

  										</div>

  									</div>

  									<div class="tab-pane" id="tab_2">
  										<div class="card-body">

  											<table id="example2" class="table table-bordered table-hover">
  												<thead>
  													<tr>
  														<th>Serviço</th>
  														<th>Data</th>
  														<th>QTD/Valor</th>
  														<th>Link</th>
  														<th>Status</th>
  													</tr>
  												</thead>
  												<?php foreach ($pendentes as $p) { ?>
  													<tr>
  														<td><?php echo $p->servico_nome; ?></td>
  														<td><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-',$v->ordem_data))); ?></td>
  														<td><?php echo $p->ordem_quantidade . ' / ' . $p->ordem_valor; ?></td>
  														<td><?php echo $p->ordem_link; ?></td>
  														<td><?php echo $p->ordem_status; ?></td>
  													</tr>
  												<?php } ?>
  												<tbody>
  													</tbfoot>
  											</table>

  										</div>
  									</div>
  									<!-- /.tab-pane -->
  									<div class="tab-pane" id="tab_3">

  										<div class="card-body">

  											<table id="example3" class="table table-bordered table-hover">
  												<thead>
  													<tr>
  														<th>Serviço</th>
  														<th>Data</th>
  														<th>QTD/Valor</th>
  														<th>Link</th>
  														<th>Status</th>
  													</tr>
  												</thead>
  												<?php foreach ($concluidas as $c) { ?>
  													<tr>
  														<td><?php echo $c->servico_nome; ?></td>
  														<td><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-',$v->ordem_data))); ?></td>
  														<td><?php echo $c->ordem_quantidade . ' / ' . $c->ordem_valor; ?></td>
  														<td><?php echo $c->ordem_link; ?></td>
  														<td><?php echo $c->ordem_status; ?></td>
  													</tr>
  												<?php } ?>
  												<tbody>
  													</tbfoot>
  											</table>

  										</div>

  									</div>

  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	@media (max-width: 600px) {
  		.card-tools {
  			padding-top: 25px;
  		}

  		.card-header>.card-tools {
  			float: left;
  		}
  	}
  </style>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->