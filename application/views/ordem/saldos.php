  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Detalhamento de Saldo</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Detalhamento de Saldo</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="row">
  			<div class="col-md-12">
  				<!-- Horizontal Form -->
  				<div class="box box-primary">
  					<div class="card">
  						<div class="card-header">
  							<h3 class="card-title">
  								<i class="fas fa-boxes mr-1"></i>
  								Saldos
  							</h3>
  							<div class="card-tools">
  								<ul class="nav nav-pills ml-auto">
  									<li class="nav-item">
  										<a class="nav-link active" href="#tab_1" data-toggle="tab">Creditos</a>
  									</li>
  									<li class="nav-item">
  										<a class="nav-link" href="#tab_2" data-toggle="tab">Debitos</a>
  									</li>
  								</ul>
  							</div>
  						</div>

  						<div class="col-md-12">
  							<div class="nav-tabs-custom">
  								<div class="tab-content">

  									<div class="tab-pane active" id="tab_1">

  										<div class="card-body">

  											<table id="example1" class="table table-bordered table-hover">
  												<thead>
  													<tr>
  														<th>Observação</th>
  														<th>Data</th>
  														<th>Valor</th>
  													</tr>
  												</thead>
  												<?php foreach ($creditos as $v) { ?>
  													<tr>
  														<td><?php echo $v->historico_saldo_obsevacao; ?></td>
  														<td><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-',$v->historico_saldo_data))); ?></td>
  														<td><?php echo '(+) R$ ' . str_replace('.', ',', $v->historico_saldo_valor); ?></td>
  													</tr>
  												<?php } ?>
  												<tbody>
  													</tbfoot>
  											</table>

  										</div>

  									</div>

  									<div class="tab-pane" id="tab_2">
  										<div class="card-body">

  											<table id="example2" class="table table-bordered table-hover">
  												<thead>
  													<tr>
  														<th>Serviço</th>
  														<th>Data</th>
  														<th>Valor</th>
  													</tr>
  												</thead>
  												<?php foreach ($debitos as $p) { ?>
  													<tr>
  														<td><?php echo $p->servico_nome; ?></td>
  														<td><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-',$p->ordem_data))); ?></td>
                              <td><?php echo '(-) R$ ' . str_replace('.', ',', $p->ordem_valor); ?></td>	
  													</tr>
  												<?php } ?>
  												<tbody>
  													</tbfoot>
  											</table>

  										</div>
  									</div>

  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	@media (max-width: 600px) {
  		.card-tools {
  			padding-top: 25px;
  		}

  		.card-header>.card-tools {
  			float: left;
  		}
  	}
  </style>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->