<style type="text/css">
	.card {
		border-radius: 10px;
	}

	.card-header {
		border-radius: 10px;
	}

	.card-footer {
		border-radius: 10px;
	}

	.banco i.mercadopago {
		background-position: 0 center;
	}

	.banco i.nubank {
		background-position: -70px center;
	}

	.banco i.caixa {
		background-position: -140px center;
	}

	.banco i.brasil {
		background-position: -210px center;
	}

	.banco i.itau {
		background-position: -280px center;
	}

	.banco i.bradesco {
		background-position: -350px center;
	}

	.banco i {
		float: left;
		width: 70px;
		height: 70px;
		background-image: url(<?php echo base_url(); ?>assets/dist/img/credit/bancos.png);
		margin-right: 15px;
		border-radius: 10px;
	}

	.custom-file-input:hover::before {
		border-color: black;
	}

	.alert.amarelo {
		border-color: #cc9d06;
		background: #fff3cd;
		color: #cc9d06;
	}

	.form-control {
		width: 15%;
	}

	#itemPagar {
		display: flex;
		align-items: center;
	}

	#formPagar.product-info {
		margin: 0;
	}

	@media (max-width: 600px) {
		.form-control {
			width: 100%;
		}

		#formMercado {
			width: 98%;
		}

		#formPagar .form-group {
			width: 100%;
		}
	}

	.card-bancos:hover {
		box-shadow: 0px 0px 5px 5px #ccc;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark" style="font-weight: 600;">Contas</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Contas</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
		<div class="col-md-12"><br>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
			</div>
		</div>
	<?php } else if ($this->session->flashdata('success')) { ?>
		<div class="col-md-12">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
			</div>
		</div>
	<?php } else if ($this->session->flashdata('warning')) { ?>
		<div class="col-md-12">
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5></i> <?php echo $this->session->flashdata('warning'); ?></h5>
			</div>
		</div>
	<?php } ?>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">

			<div class="card card-success">
				<div class="card-header">
					<h3 class="card-title">Gerenciar Comprovantes</h3>
					<i class="fas fa-file-alt" style="position: absolute; right: 2%; font-size: 25px;"></i>
				</div>

				<div class="col-md-12" style=" border-radius: 25px;">
					<hr>Utilize essa página para enviar novos comprovantes.<br>
					Depósito mínimo 10 reais comprador 20 reais revendedor.
					<hr>
					<h4>Enviar Comprovante</h4>
					<form method="post" action="<?php echo base_url(); ?>contas/cadastrarComprovante" enctype="multipart/form-data">Aceito apenas arquivos de imagem (JPG e PNG) e PDF.
						<br>
						<div class="custom-file">
							<input type="file" required="required" name="fileComprovante" accept="image/jpeg, image/jpg, image/png, image/jpeg, application/pdf" class="custom-file-input" id="customFile">
							<label class="custom-file-label" data-browse="Escolha o Arquivo" for="customFile">Selecione o Arquivo</label>
						</div><br><br>
						<button class="btn btn-success">Enviar Comprovante</button>
					</form><br><br>
				</div>

			</div>

			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">Mercado Pago</h3>
					<i class="fas fa-wallet" style="position: absolute; right: 2%; font-size: 25px;"></i>
				</div>
				<!-- /.card-header -->
				<div class="card-body p-0"><br>
<!-- 
					<div class="alert amarelo">Pagamentos via boleto bancário podem levar de 1 à 2 dias para serem aprovados, por cartão de crédito geralmente são aprovados imediatamente.</div> -->

					<div class="alert amarelo">Pagamentos via cartão de crédito geralmente são aprovados imediatamente.</div>

					<ul class="products-list product-list-in-card pl-2 pr-2">
						<li id="itemPagar" class="item">
							<div class="product-img banco">
								<i class="mercadopago"></i>
							</div>
							<div id="formPagar" class="product-info">
								<form class="form-inline">
									<div class="form-group mr-2">
										<input type="text" class="form-control money" id="amount" placeholder="20.00" value="<?php echo $this->session->userdata('perfil_id') == '2' ? '20.00' : '10.00' ?>" autocomplete="off" required="required">
									</div>
									<input class="btn btn-primary" type="submit" id="formMercado" value="Pagar agora">
								</form>
							</div>
						</li>
					</ul>

				</div>
			</div>

			<div class="card-deck mb-4">

				<div class="card card-bancos" style="border: 1px solid #fed000;">
					<div class="product-img banco mx-auto pt-4">
						<i class="brasil"></i>
					</div>
					<div class="card-body">
						<h5 class="card-title font-weight-bold">001 - Banco do Brasil</h5>
						<p class="card-text">
							<span><strong>Titular:</strong> Rafael da Costa Melo</span><br>
							<span><strong>Agência:</strong> 4235-8</span><br>
							<span><strong>Conta Corrente:</strong> 11230-5</span><br>
							<span><strong>CPF:</strong> 849.027.364-20</span></p>
					</div>
				</div>

				<div class="card card-bancos" style="border: 1px solid #8a05be;">
					<div class="product-img banco mx-auto pt-4">
						<i class="nubank"></i>
					</div>
					<div class="card-body">
						<h5 class="card-title font-weight-bold">260 - Nubank</h5>
						<p class="card-text">
							<span><strong>Titular:</strong> Rafael da Costa Melo</span><br>
							<span><strong>Agência:</strong> 0001</span><br>
							<span><strong>Conta:</strong> 140162-1</span><br>
							<span><strong>CPF:</strong> 849.027.364-20</span></p>
					</div>
				</div>

				<div class="card card-bancos" style="border: 1px solid #006ab4;">
					<div class="product-img banco mx-auto pt-4">
						<i class="caixa"></i>
					</div>
					<div class="card-body">
						<h5 class="card-title font-weight-bold">104 - Caixa Econômica Federal</h5>
						<p class="card-text">
							<span><strong>Titular:</strong> Leandro H Cavalcanti de Souza</span><br>
							<span><strong>Agência:</strong> 0048 </span><br>
							<span><strong>Conta Poupança:</strong> 27183-0</span><br>
							<span><strong>CPF:</strong> 104.457.874-22</span></p>
					</div>
				</div>

				<div class="card card-bancos" style="border: 1px solid #da251c;">
					<div class="product-img banco mx-auto pt-4">
						<i class="bradesco"></i>
					</div>
					<div class="card-body">
						<h5 class="card-title font-weight-bold">327 - Bradesco</h5>
						<p class="card-text">
							<span><strong>Titular:</strong> Leandro H Cavalcanti de Souza</span><br>
							<span><strong>Agência:</strong> 4343</span><br>
							<span><strong>Conta Poupança:</strong> 3399-5</span><br>
							<span><strong>CPF:</strong> 104.457.874-22</span></p>
					</div>
				</div>

			</div>
			<!--
			<div class="card card-warning collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
				<div class="card-header">
					<h3 class="card-title">001 - Banco do Brasil</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
							<i class="fas fa-plus" aria-hidden="true"></i>
						</button>
					</div>
				</div>
			
				<div class="card-body p-0">

					<ul class="products-list product-list-in-card pl-2 pr-2">
						<li class="item">
							<div class="product-img banco">
								<i class="brasil"></i>
							</div>
							<div class="product-info">
								<span><strong>Titular:</strong> Rafael da Costa Melo</span><br>
								<span><strong>Agencia:</strong> 4235-8 <strong>Conta Corrente:</strong> 11230-5</span><br>
								<span><strong>CPF:</strong> 849.027.364-20</span>
							</div>
						</li>
					</ul>

				</div>
			</div>

			<div class="card card-pink collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
				<div class="card-header">
					<h3 class="card-title">260 - Nubank</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
							<i class="fas fa-plus" aria-hidden="true"></i>
						</button>
					</div>
				</div>
				<div class="card-body p-0">

					<ul class="products-list product-list-in-card pl-2 pr-2">
						<li class="item">
							<div class="product-img banco">
								<i class="nubank"></i>
							</div>
							<div class="product-info">
								<span><strong>Titular:</strong> Rafael da Costa Melo</span><br>
								<span><strong>Agencia:</strong> 0001 <strong>Conta:</strong> 140162-1</span><br>
								<span><strong>CPF:</strong> 849.027.364-20</span>
							</div>
						</li>
					</ul>

				</div>
			</div>

			<div class="card card-blue collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
				<div class="card-header">
					<h3 class="card-title">104 - Caixa Economica Federal</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
							<i class="fas fa-plus" aria-hidden="true"></i>
						</button>
					</div>
				</div>
				<div class="card-body p-0">

					<ul class="products-list product-list-in-card pl-2 pr-2">
						<li class="item">
							<div class="product-img banco">
								<i class="caixa"></i>
							</div>
							<div class="product-info">
								<span><strong>Titular:</strong> Leandro H Cavalcanti de Souza</span><br>
								<span><strong>Agencia:</strong> 0048 <strong>Conta Poupança:</strong> 27183-0</span><br>
								<span><strong>CPF:</strong> 104.457.874-22</span>
							</div>
						</li>
					</ul>

				</div>
			</div>

			<div class="card card-danger collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
				<div class="card-header">
					<h3 class="card-title">327 - Bradesco</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
							<i class="fas fa-plus" aria-hidden="true"></i>
						</button>
					</div>
				</div>
				<div class="card-body p-0">

					<ul class="products-list product-list-in-card pl-2 pr-2">
						<li class="item">
							<div class="product-img banco">
								<i class="bradesco"></i>
							</div>
							<div class="product-info">
								<span><strong>Titular:</strong> Leandro H Cavalcanti de Souza</span><br>
								<span><strong>Agencia:</strong> 4343 <strong>Conta Poupança:</strong> 3399-5</span><br>
								<span><strong>CPF:</strong> 104.457.874-22</span>
							</div>
						</li>
					</ul>

				</div>
			</div>
		-->

			<!-- 
        <div class="card card-info collapsed-card" style="cursor:pointer;" data-card-widget="collapse">
            <div class="card-header">
              <h3 class="card-title">Mercado Pago</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-plus" aria-hidden="true"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">

              <ul class="products-list product-list-in-card pl-2 pr-2">
                <li class="item">
                  <div class="product-img banco">
                    <i class="mercadopago"></i>
                  </div>
                  <div class="product-info">
                    <span><strong>Titular:</strong> Rafael da Costa Melo</span><br>
                    <span><strong>Agencia:</strong>  0001 <strong>Conta:</strong> 1841141441-7</span><br>
                    <span><strong>CPF:</strong> 849.027.364-20</span>
                  </div>
                </li>
              </ul>

            </div>
        </div>
 	-->

		</div>
		<!--/. container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";
	var perfil = "<?php echo $this->session->userdata('perfil_id'); ?>";
</script>
<script>
	$(function() {

		var submit_btn = $('#formMercado');

		submit_btn.click(function(e) {
			e.preventDefault();

			var amount = $('#amount').val();

			if (perfil === '1' || perfil === '3') {

				if (amount.length === 0 || amount === '0.00' || parseFloat(amount) < 10) {

					amount = '10.00'

				}

			}else{

				if (amount.length === 0 || amount === '0.00' || parseFloat(amount) < 20) {

					amount = '20.00'

				}

			}



			$.ajax({
				method: "POST",
				url: base_url + "contas/mercadoExe/",
				dataType: "JSON",
				data: {
					valor: amount
				},
				success: function(data) {
					if (data != 'error') {
						var novaURL = data;
						location.href = novaURL;
					} else {
						alert('Ocorreu um erro tente novamente!');
					}
				}

			});

		});
	});
</script>