  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Gestão de Saldo</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Gestão de Saldo</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->


  	<?php if ($this->session->flashdata('error') or $this->session->flashdata('erro')) { ?>
  		<div class="col-md-12">
  			<div class="alert alert-danger alert-dismissible">
  				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				<h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error'); ?></h5>
  			</div>
  		</div>
  	<?php } else if ($this->session->flashdata('success')) { ?>
  		<div class="col-md-12">
  			<div class="alert alert-success alert-dismissible">
  				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				<h5></i> <?php echo $this->session->flashdata('success'); ?></h5>
  			</div>
  		</div>
  	<?php } ?>

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="row">
  				<div class="col-md-12 col-xl-12">
  					<div class="card card-info">
  						<div class="card-header">
  							<h3 class="card-title">Liberação de Saldo</h3>
  						</div>
  						<div class="card-body p-0">
  							<table id="gestao-saldo-desktop" class="table table-striped">
  								<th class="text-center">Usuario</th>
  								<th class="text-center">Perfil</th>
  								<th class="text-center">Saldo Atual</th>
  								<th class="text-center">Comprovante</th>
  								<th class="text-center">Ação</th>
  								<tbody>
  									<?php foreach ($dados as $i) { ?>
  										<tr>
  											<td class="text-center"><?php echo $i->usuario_nome ?></td>
  											<td class="text-center"><?php echo $i->perfil_nome ?></td>
  											<td class="text-center"><?php echo $i->saldo_valor ?></td>
  											<td class="text-center"> <a href='<?php echo base_url(); ?>assets/arquivos/comprovantes/<?php echo $i->comprovante ?>' target='_blank'>Ver Comprovante</a></td>
  											<td class="text-center">
  												<a style="cursor:pointer;" title="Invalidar" data-toggle="modal" data-target="#invalidarComprovanteModal" onclick="invalidarModal('<?php echo base_url() . $this->uri->segment(1) . "/ComprovanteStatusInvalidar/" . $i->comprovante_id . "/0/" . $i->idUsuario ?>')"><i class="fas fa-times  text-danger"></i></a>&nbsp;&nbsp;
  												<a style="cursor:pointer;" title="Validar" data-toggle="modal" data-target="#validarComprovante" onclick="validarComprovanteModal('<?php echo $i->comprovante_id; ?>', '<?php echo $i->idUsuario; ?>');"><i class="fas fa-check  text-success"></i></a>
  											</td>
  										</tr>
  									<?php } ?>
  								</tbody>
  							</table>

  							<table id="gestao-saldo-mobile" class="table table-striped">
  								<!-- <th class="text-center">Usuario</th>
  								<th class="text-center">Perfil</th>
  								<th class="text-center">Saldo Atual</th>
  								<th class="text-center">Comprovante</th>
  								<th class="text-center">Ação</th> -->
  								<tbody>
  									<?php foreach ($dados as $i) { ?>
  										<tr class="lista-gestao">
  											<td class="text-center"><span>Usuário</span><?php echo $i->usuario_nome ?></td>
  											<td class="text-center"><span>Perfil</span><?php echo $i->perfil_nome ?></td>
  											<td class="text-center"><span>Saldo Atual</span><span class="saldo-result">R$ <?php echo $i->saldo_valor ?></span></td>
  											<td class="text-center"><span>Comprovante</span><a href='<?php echo base_url(); ?>assets/arquivos/comprovantes/<?php echo $i->comprovante ?>' target='_blank'>Ver Comprovante</a></td>
  											<td class="text-center"><span>Ação</span>
  												<span>
  													<a class="pr-2" style="cursor:pointer;" title="Invalidar" data-toggle="modal" data-target="#invalidarComprovanteModal" onclick="invalidarModal('<?php echo base_url() . $this->uri->segment(1) . "/ComprovanteStatusInvalidar/" . $i->comprovante_id . "/0/" . $i->idUsuario ?>')"><i class="fas fa-times text-danger"></i></a>&nbsp;&nbsp;
  													<a style="cursor:pointer;" title="Validar" data-toggle="modal" data-target="#validarComprovante" onclick="validarComprovanteModal('<?php echo $i->comprovante_id; ?>', '<?php echo $i->idUsuario; ?>');"><i class="fas fa-check  text-success"></i></a>
  												</span>
  											</td>
  										</tr>
  									<?php } ?>
  								</tbody>
  							</table>
  						</div>
  					</div>
  				</div>
  			</div>

  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	#gestao-saldo-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {
  		#gestao-saldo-mobile {
  			display: block;
  		}

  		#gestao-saldo-desktop {
  			display: none;
  		}

  		#gestao-saldo-mobile {
  			display: flex;
  			flex-direction: column;
  		}

  		#gestao-saldo-mobile .lista-gestao {
  			display: flex;
  			flex-direction: column;
  			margin-bottom: 20px;
  		}

  		#gestao-saldo-mobile .lista-gestao .text-center {
  			display: flex;
  			justify-content: space-between;
  		}

  		#gestao-saldo-mobile .lista-gestao .text-center span {
  			font-weight: bold;
  		}

  		.lista-gestao .saldo-result {
  			font-weight: 500 !important;
  		}

  		.card-body.p-0 .table tbody>tr>td:first-of-type,
  		.card-body.p-0 .table tbody>tr>th:first-of-type,
  		.card-body.p-0 .table thead>tr>td:first-of-type,
  		.card-body.p-0 .table thead>tr>th:first-of-type {
  			padding-left: 12px;
  		}
  	}
  </style>


  <!-- Modal Excluir registros -->
  <div class="example-modal">
  	<div class="modal" id="invalidarComprovanteModal">
  		<div class="modal-dialog modal-dialog-centered">
  			<div class="modal-content">
  				<div class="modal-body">
  					<p id="comprovanteInvalidar">Deseja realmente invalidar este comprovante ?</p>
  				</div>
  				<div class="modal-footer">
  					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  					<a href="" class="btn btn-danger" id="invalidarIdComprovante">Invalidar</a>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <!-- Fim modal Excluir registros -->


  <!-- Modal -->
  <div class="modal fade" id="validarComprovante">
  	<div class="modal-dialog modal-dialog-centered">
  		<div class="modal-content">
  			<div class="modal-body">
  				<div class="content">

  					<form role="form" id="validarComprovanteForm" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/ComprovanteStatusValidar" method="post" enctype="multipart/form-data">


  						<div class="form-group">
  							<label>Saldo á Adicionar</label>
  							<input type="text" class="col-md-9 form-control money" name="modal_valor" id="modal_valor" required="required"></input>
  							<input type="hidden" class="col-md-12 form-control" name="modal_comprovante" id="modal_comprovante"></input>
  							<input type="hidden" class="col-md-12 form-control" name="modal_usuario" id="modal_usuario"></input>
  						</div>

  				</div>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
  				<button type="submit" class="btn btn-success">Validar</button>
  			</div>
  			</form>
  		</div>
  		<!-- /.modal-content -->
  	</div>
  	<!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <script type="text/javascript">
  	function invalidarModal(url) {
  		$('#comprovanteInvalidar').html('<p>Deseja realmente invalidar este comprovante ?</p>');
  		$('#invalidarIdComprovante').attr('href', url);
  	}

  	function validarComprovanteModal(idComprovante, idUsuario) {
  		$('#modal_comprovante').val(idComprovante);
  		$('#modal_usuario').val(idUsuario);
  	}
  </script>