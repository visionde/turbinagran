  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Relatório Indicação Pix</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Relatório Indicação Pix</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="col-md-12">
  				<div class="card card-primary" style="padding: 20px 10px;">
  					<!--
              <div class="card-header">
              </div>
              -->
  					<form action="<?php echo base_url(); ?>relatorios/bonuspix">
  						<div class="box-body">
  							<div class="col-md-12">
  								<div class="row">
  									<div class="col-md-4">
  										<div class="form-group">
  											<label>Status</label>
  											<select class="form-control" name="status">
  												<option value="todos" <?php if ($status == 'todos') {
																		echo 'selected';
																	} ?>>TODOS</option>
  												<option value="NULL" <?php if ($status == 'NULL') {
																				echo 'selected';
																			} ?>>AGUARDANDO APROVAÇÃO</option>
  												<option value="0" <?php if ($status == '0') {
																				echo 'selected';
																			} ?>>INVÁLIDO</option>
                                                <option value="1" <?php if ($status == '1') {
																				echo 'selected';
																			} ?>>APROVADO</option>
  											</select>
  										</div>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<button style="margin-top: 7px; border-radius: 10px;" type="submit" class="btn btn-block btn-primary btn-flat">Filtrar <i class="fas fa-filter pl-2"></i></button>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<a style="margin-top: 7px; border-radius: 10px;" href="<?php echo base_url() . 'relatorios/bonuspixExcel/' . $status ?>" class="btn btn-block btn-success btn-flat">Exportar <i class="fas fa-file-export pl-2"></i></a>
  									</div>
  								</div>
  							</div>
  						</div>
  					</form>
  					<hr>

  					<div id="lista-bonuspix-desktop" class="box-body">
  						<table class="table table-bordered table-striped">
  							<thead>
  								<tr>
  									<th class="columns01 col_default">#</th>
  									<th class="columns03 col_default">Usuário</th>
  									<th class="columns02 col_default">Usuário Admin</th>
                                    <th class="columns02 col_default">Status</th>
  									<th class="columns05 col_default">Pix</th>
  								</tr>
  							</thead>
  							<tbody>

                              <?php foreach ($dados as $f) { 

                                if ($f->pix_status == '0') {

                                $label = 'INVÁLIDO';

                                }elseif ($f->pix_status == '1') {

                                $label = 'APROVADO';

                                }else{

                                $label = 'AGUARDANDO APROVAÇÃO';

                                } ?>

                                <tr>
                                    <td class="text-left"><?php echo $f->pix_id; ?></td>
                                    <td class="text-left"><?php echo $f->usuario_recebido; ?></td>
                                    <td class="text-left"><?php echo $f->usuario_liberado; ?></td>
                                    <td class="text-left"><?php echo $label; ?></td>
                                    <td class="text-left"><?php echo $f->pix_bonus; ?></td>
                                </tr>

                                <?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div id="lista-bonuspix-mobile" class="box-body">
  						<table class="table table-bordered table-striped">
  							<tbody>

                              <?php foreach ($dados as $f) { 

                                if ($f->pix_status == '0') {

                                    $label = 'INVÁLIDO';

                                    }elseif ($f->pix_status == '1') {

                                    $label = 'APROVADO';

                                    }else{

                                    $label = 'AGUARDANDO APROVAÇÃO';

                                } ?>

                                <tr class="lista-bonus">
                                    <td class="text-left"><span>ID</span><?php echo $f->pix_id; ?></td>
                                    <td class="text-left"><span>Usuário</span><?php echo $f->usuario_recebido; ?></td>
                                    <td class="text-left"><span>Usuário Admin</span><?php echo $f->usuario_liberado; ?></td>
                                    <td class="text-left"><span>Status</span><?php echo $label ?></span></td>
                                    <td class="text-left"><span>Pix</span><span class="result-saldo"><?php echo$f->pix_valor ?></span></td>
                                </tr>

                                <?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div class="card-footer clearfix">
  						<ul class="pagination pagination-sm m-0 float-right">
  							<?php
								if ($total_bonuspix <> 0) {

									for ($p = 1, $i = 0; $i < $total_bonuspix; $p++, $i += $itens_per_page) :
										if ($page == $p) :
											$tmp[] = "<li class='page-item active'><a class='page-link' href='javascript:;'>" . $p . "</a></li>";
										else :
											$tmp[] = "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/bonuspix/" . $p . "/" . $status . "'>" . $p . "</a></li>";
										endif;
									endfor;

									for ($i = count($tmp) - 3; $i > 1; $i--) :
										if (abs($page - $i - 1) > 2) :
											unset($tmp[$i]);
										endif;
									endfor;

									if (count($tmp) > 1) :
										if ($page > 1) :
											echo "<li ><a class='page-link' href='" . base_url() . "relatorios/bonuspix/" . ($page - 1) . "/" . $status . "'>«</a></li>";
										endif;

										$lastlink = 0;
										foreach ($tmp as $i => $link) :
											if ($i > $lastlink + 1) :
												echo "<li class='page-item'><a class='page-link' href='javascript:;'>...</a></li>";
											endif;
											echo $link;
											$lastlink = $i;
										endforeach;

										if ($page <= $lastlink) :
											echo "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/bonuspix/" . ($page + 1) . "/" . $status . "'>«</a></li>";
										endif;

									endif;
								} else {

									echo "Dados não foi encontrado!";
								}

								?>

  						</ul>
  					</div>

  				</div>

  			</div>


  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	#lista-bonuspix-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {

  		#lista-bonuspix-desktop {
  			display: none;
  		}

  		#lista-bonuspix-mobile {
  			display: block;
  		}

  		thead {
  			display: inline-block;
  		}

  		.lista-bonuspix {
  			display: flex;
  			flex-direction: column;
  			margin-bottom: 20px;
  		}

  		.lista-bonuspix span {
  			font-weight: bold;
  		}

  		.lista-bonuspix .result-saldo {
  			font-weight: 500;
  		}

  		.lista-bonuspix .text-left {
  			display: flex;
  			justify-content: space-between;
  			align-items: center;
  			font-size: 14px;
  		}
  	}
  </style>


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->