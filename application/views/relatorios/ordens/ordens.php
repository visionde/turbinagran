  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Relatório Ordens</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Relatório Ordens</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="col-md-12">
  				<div class="card card-primary" style="padding: 20px 5px;">
  					<!--
              <div class="card-header">
              </div>
              -->
  					<form action="<?php echo base_url(); ?>relatorios/ordens">
  						<div class="box-body">
  							<div class="col-md-12">
  								<div class="row">
  									<div class="col-md-4">
  										<div class="form-group">
  											<label>Status</label>
  											<select class="form-control" name="status">
  												<option value="1" <?php if ($status == '1') {
																		echo 'selected';
																	} ?>>TODOS</option>
  												<option value="PENDENTE" <?php if ($status == 'PENDENTE') {
																				echo 'selected';
																			} ?>>PENDENTE</option>
  												<option value="REEMBOLSO" <?php if ($status == 'REEMBOLSO') {
																				echo 'selected';
																			} ?>>REEMBOLSO</option>
  												<option value="EM PROGRESSO" <?php if ($status == 'EM PROGRESSO') {
																					echo 'selected';
																				} ?>>EM PROGRESSO</option>

  												<option value="CONCLUIDA" <?php if ($status == 'CONCLUIDA') {
																				echo 'selected';
																			} ?>>CONCLUIDA</option>
  											</select>
  										</div>
  									</div>
  									<div class="col-md-4">
  										<div class="form-group">
  											<label>Usuário</label>
  											<input class="form-control" type="text" name="usuario" id="usuario">
  										</div>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<button style="margin-top: 7px; border-radius: 10px;" type="submit" class="btn btn-block btn-primary btn-flat">Filtrar <i class="fas fa-filter pl-2"></i></button>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<a style="margin-top: 7px; border-radius: 10px;" href="<?php echo base_url() . 'relatorios/ordensExcel/' . $status . '/' . $usuario ?>" class="btn btn-block btn-success btn-flat">Exportar <i class="fas fa-file-export pl-2"></i></a>
  									</div>
  								</div>
  							</div>
  						</div>
  					</form>
  					<hr>

  					<div id="ordens-desktop" class="box-body">
  						<table class="table table-bordered table-striped">
  							<thead>
  								<tr>
  									<th class="columns01 col_default">Usuário</th>
  									<th class="columns01 col_default">Serviço</th>
  									<th class="columns03 col_default">Data</th>
  									<th class="columns02 col_default">QTD/Valor</th>
  									<th class="columns05 col_default">Link</th>
  									<th class="columns05 col_default">Status</th>
  								</tr>
  							</thead>
  							<tbody>

  								<?php foreach ($dados as $v) {

                    if ($v->ordem_status == 'PENDENTE') {

                      $label = 'warning';

                    }elseif ($v->ordem_status == 'REEMBOLSO') {

                      $label = 'success';

                    }elseif ($v->ordem_status == 'CONCLUIDA') {

                      $label = 'success';

                    }else{

                      $label = 'info';

                    } ?>

  									<tr>
  										<td class="text-left"><?php echo $v->usuario_nome; ?></td>
  										<td class="text-left"><?php echo $v->servico_nome; ?></td>
  										<td class="text-left"><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $v->ordem_data))); ?></td>
  										<td class="text-left"><?php echo $v->ordem_quantidade . ' / R$ ' . str_replace('.', ',', $v->ordem_valor); ?></td>
  										<td class="text-left"><?php echo $v->ordem_link; ?></td>
  										<td class="text-left"><span class="badge badge-pill badge-<?php echo $label ?>"><?php echo $v->ordem_status; ?></span></td>
  									</tr>

  								<?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div id="ordens-mobile" class="box-body">
  						<table class="table table-bordered table-striped">
  							<!-- <thead>
  								<tr>
  									<th class="columns01 col_default">Usuário</th>
  									<th class="columns01 col_default">Serviço</th>
  									<th class="columns03 col_default">Data</th>
  									<th class="columns02 col_default">QTD/Valor</th>
  									<th class="columns05 col_default">Link</th>
  									<th class="columns05 col_default">Status</th>
  								</tr>
  							</thead> -->
  							<tbody>

  								<?php foreach ($dados as $v) { 

                    if ($v->ordem_status == 'PENDENTE') {

                      $label = 'warning';

                    }elseif ($v->ordem_status == 'REEMBOLSO') {

                      $label = 'success';

                    }elseif ($v->ordem_status == 'CONCLUIDA') {

                      $label = 'success';

                    }else{

                      $label = 'info';

                    } ?>

  									<tr class="lista-ordens">
  										<td class="text-left"><span>Usuário</span><?php echo $v->usuario_nome; ?></td>
  										<td class="text-left"><span>Serviço</span><?php echo $v->servico_nome; ?></td>
  										<td class="text-left"><span>Data</span><?php echo date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $v->ordem_data))); ?></td>
  										<td class="text-left"><span>QTD / Valor</span><?php echo $v->ordem_quantidade . ' / R$ ' . str_replace('.', ',', $v->ordem_valor); ?></td>
  										<td class="text-left"><span>Link</span><?php echo $v->ordem_link; ?></td>
  										<td class="text-left"><span>Status</span><span class="badge badge-pill badge-<?php echo $label ?>"><?php echo $v->ordem_status; ?></span></td>
  									</tr>

  								<?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div class="card-footer clearfix">
  						<ul class="pagination pagination-sm m-0 float-right">
  							<?php
								if ($total_ordens <> 0) {

									for ($p = 1, $i = 0; $i < $total_ordens; $p++, $i += $itens_per_page) :
										if ($page == $p) :
											$tmp[] = "<li class='page-item active'><a class='page-link' href='javascript:;'>" . $p . "</a></li>";
										else :
											$tmp[] = "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/ordens/" . $p . "/" . $status . "/" . $usuario . "'>" . $p . "</a></li>";
										endif;
									endfor;

									for ($i = count($tmp) - 3; $i > 1; $i--) :
										if (abs($page - $i - 1) > 2) :
											unset($tmp[$i]);
										endif;
									endfor;

									if (count($tmp) > 1) :
										if ($page > 1) :
											echo "<li ><a class='page-link' href='" . base_url() . "relatorios/ordens/" . ($page - 1) . "/" . $status . "/" . $usuario . "'>«</a></li>";
										endif;

										$lastlink = 0;
										foreach ($tmp as $i => $link) :
											if ($i > $lastlink + 1) :
												echo "<li class='page-item'><a class='page-link' href='javascript:;'>...</a></li>";
											endif;
											echo $link;
											$lastlink = $i;
										endforeach;

										if ($page <= $lastlink) :
											echo "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/ordens/" . ($page + 1) . "/" . $status . "/" . $usuario . "'>«</a></li>";
										endif;

									endif;
								} else {

									echo "Dados não foi encontrado!";
								}
								?>

  						</ul>
  					</div>

  				</div>

  			</div>


  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	#ordens-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {

  		#ordens-desktop {
  			display: none;
  		}

  		#ordens-mobile {
  			display: block;
  		}

  		.lista-ordens {
  			display: flex;
  			flex-direction: column;
  			margin-bottom: 20px;
  		}

  		.lista-ordens span {
  			font-weight: bold;
  		}

  		.lista-ordens .text-left {
  			display: flex;
  			justify-content: space-between;
  			align-items: center;
  			font-size: 12px;
  		}
  	}
  </style>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->