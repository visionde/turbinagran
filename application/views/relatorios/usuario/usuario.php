  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Relatório Usuários Ativos</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Relatório Usuários Ativos</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="col-md-12">
  				<div class="card card-primary" style="padding: 20px 10px;">
  					<!--
              <div class="card-header">
              </div>
              -->
  					<form action="<?php echo base_url(); ?>relatorios/usuario">
  						<div class="box-body">
  							<div class="col-md-12">
  								<div class="row">
  									<div class="col-md-4">
  										<div class="form-group">
  											<label>Status</label>
  											<select class="form-control" name="status">
  												<option value="1" <?php if ($status == '1') {
																		echo 'selected';
																	} ?>>TODOS</option>
  												<option value="POSITIVO" <?php if ($status == 'POSITIVO') {
																				echo 'selected';
																			} ?>>POSITIVO</option>
  												<option value="ZERADO" <?php if ($status == 'ZERADO') {
																				echo 'selected';
																			} ?>>ZERADO</option>
  											</select>
  										</div>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<button style="margin-top: 7px; border-radius: 10px;" type="submit" class="btn btn-block btn-primary btn-flat">Filtrar <i class="fas fa-filter pl-2"></i></button>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<a style="margin-top: 7px; border-radius: 10px;" href="<?php echo base_url() . 'relatorios/usuarioExcel/' . $status ?>" class="btn btn-block btn-success btn-flat">Exportar <i class="fas fa-file-export pl-2"></i></a>
  									</div>
  								</div>
  							</div>
  						</div>
  					</form>
  					<hr>

  					<div id="lista-usuarios-desktop" class="box-body">
  						<table class="table table-bordered table-striped">
  							<thead>
  								<tr>
  									<th class="columns01 col_default">#</th>
  									<th class="columns03 col_default">Nome</th>
  									<th class="columns02 col_default">Email</th>
  									<th class="columns05 col_default">Perfil</th>
  									<th class="columns05 col_default">Saldo</th>
  								</tr>
  							</thead>
  							<tbody>

  								<?php foreach ($dados as $f) { ?>

  									<tr>
  										<td class="text-left"><?php echo $f->usuario_id; ?></td>
  										<td class="text-left"><?php echo $f->usuario_nome; ?></td>
  										<td class="text-left"><?php echo $f->usuario_email; ?></td>
  										<td class="text-left"><?php echo ucfirst($f->perfil_nome) ?></span></td>
  										<td class="text-left"><?php echo str_replace('.', ',', $f->saldo_valor); ?></td>
  									</tr>

  								<?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div id="lista-usuarios-mobile" class="box-body">
  						<table class="table table-bordered table-striped">
  							<!-- <thead>
  								<tr>
  									<th class="columns01 col_default">#</th>
  									<th class="columns03 col_default">Nome</th>
  									<th class="columns02 col_default">Email</th>
  									<th class="columns05 col_default">Perfil</th>
  									<th class="columns05 col_default">Saldo</th>
  								</tr>
  							</thead> -->
  							<tbody>

  								<?php foreach ($dados as $f) { ?>

  									<tr class="lista-usuarios">
  										<td class="text-left"><span>ID</span><?php echo $f->usuario_id; ?></td>
  										<td class="text-left"><span>Nome</span><?php echo $f->usuario_nome; ?></td>
  										<td class="text-left"><span>E-mail</span><?php echo $f->usuario_email; ?></td>
  										<td class="text-left"><span>Perfil</span><?php echo ucfirst($f->perfil_nome) ?></span></td>
  										<td class="text-left"><span>Saldo</span><span class="result-saldo">R$ <?php echo str_replace('.', ',', $f->saldo_valor); ?></span></td>
  									</tr>

  								<?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div class="card-footer clearfix">
  						<ul class="pagination pagination-sm m-0 float-right">
  							<?php
								if ($total_usuario <> 0) {

									for ($p = 1, $i = 0; $i < $total_usuario; $p++, $i += $itens_per_page) :
										if ($page == $p) :
											$tmp[] = "<li class='page-item active'><a class='page-link' href='javascript:;'>" . $p . "</a></li>";
										else :
											$tmp[] = "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/usuario/" . $p . "/" . $status . "'>" . $p . "</a></li>";
										endif;
									endfor;

									for ($i = count($tmp) - 3; $i > 1; $i--) :
										if (abs($page - $i - 1) > 2) :
											unset($tmp[$i]);
										endif;
									endfor;

									if (count($tmp) > 1) :
										if ($page > 1) :
											echo "<li ><a class='page-link' href='" . base_url() . "relatorios/usuario/" . ($page - 1) . "/" . $status . "'>«</a></li>";
										endif;

										$lastlink = 0;
										foreach ($tmp as $i => $link) :
											if ($i > $lastlink + 1) :
												echo "<li class='page-item'><a class='page-link' href='javascript:;'>...</a></li>";
											endif;
											echo $link;
											$lastlink = $i;
										endforeach;

										if ($page <= $lastlink) :
											echo "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/usuario/" . ($page + 1) . "/" . $status . "'>«</a></li>";
										endif;

									endif;
								} else {

									echo "Dados não foi encontrado!";
								}

								?>

  						</ul>
  					</div>

  				</div>

  			</div>


  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>
  	#lista-usuarios-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {

  		#lista-usuarios-desktop {
  			display: none;
  		}

  		#lista-usuarios-mobile {
  			display: block;
  		}

  		thead {
  			display: inline-block;
  		}

  		.lista-usuarios {
  			display: flex;
  			flex-direction: column;
  			margin-bottom: 20px;
  		}

  		.lista-usuarios span {
  			font-weight: bold;
  		}

  		.lista-usuarios .result-saldo {
  			font-weight: 500;
  		}

  		.lista-usuarios .text-left {
  			display: flex;
  			justify-content: space-between;
  			align-items: center;
  			font-size: 14px;
  		}
  	}
  </style>


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->