  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<!-- Content Header (Page header) -->
  	<div class="content-header">
  		<div class="container-fluid">
  			<div class="row mb-2">
  				<div class="col-sm-6">
  					<h1 class="m-0 text-dark" style="font-weight: 600;">Relatório Financeiro</h1>
  				</div><!-- /.col -->
  				<div class="col-sm-6">
  					<ol class="breadcrumb float-sm-right">
  						<li class="breadcrumb-item"><a href="#">Home</a></li>
  						<li class="breadcrumb-item active">Relatório Financeiro</li>
  					</ol>
  				</div><!-- /.col -->
  			</div><!-- /.row -->
  		</div><!-- /.container-fluid -->
  	</div>
  	<!-- /.content-header -->

  	<!-- Main content -->
  	<section class="content">
  		<div class="container-fluid">
  			<div class="col-md-12">
  				<div class="card px-3 py-3">
  					<!--<div class="card-header"></div>-->
  					<form action="<?php echo base_url(); ?>relatorios/financeiro">
  						<div class="box-body">
  							<div class="col-md-12">
  								<div class="row">
  									<!-- 						  	   <div class="col-md-3">
								<div class="form-group">
				                  <label>Saldo Range:</label>
				                   <div class="input-group">
					                    <div class="input-group-prepend">
					                      <span class="input-group-text">
					                        <i class="far fa-calendar-alt"></i>
					                      </span>
					                    </div>
					                    <input type="text" class="form-control float-right" id="reservation" name="data">
				                   </div>
				                 </div>
				                </div> -->
  									<div class="col-md-4">
  										<div class="form-group">
  											<label>Forma Pagamento</label>
  											<select class="form-control" name="pagamento">
  												<option value="1" <?php if ($pagamento == '1') {
																		echo 'selected';
																	} ?>>AMBOS</option>
  												<option value="2" <?php if ($pagamento == '2') {
																		echo 'selected';
																	} ?>>COMPROVANTE</option>
  												<option value="3" <?php if ($pagamento == '3') {
																		echo 'selected';
																	} ?>>MERCADO PAGO</option>
												<option value="4" <?php if ($pagamento == '4') {
																		echo 'selected';
																	} ?>>INDICAÇÃO</option>
  											</select>
  										</div>
  									</div>
  									<div class="col-md-4">
  										<div class="form-group">
  											<label>Status</label>
  											<select class="form-control" name="status">
  												<option value="1" <?php if ($status == '1') {
																		echo 'selected';
																	} ?>>AMBOS</option>
  												<option value="CONCLUIDO" <?php if ($status == 'CONCLUIDO') {
																				echo 'selected';
																			} ?>>CONCLUIDO</option>
  												<option value="PENDENTE" <?php if ($status == 'PENDENTE') {
																				echo 'selected';
																			} ?>>PENDENTE</option>
  											</select>
  										</div>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<button style="margin-top: 7px; border-radius: 10px;" type="submit" class="btn btn-block btn-primary btn-flat">Filtrar <i class="fas fa-filter pl-2"></i></button>
  									</div>
  									<div class="col-md-2">
  										<br>
  										<a style="margin-top: 7px; border-radius: 10px;" href="<?php echo base_url() . 'relatorios/financeiroExcel/' . $status . '/' . $pagamento ?>" class="btn btn-block btn-success btn-flat">Exportar <i class="fas fa-file-export pl-2"></i></a>
  									</div>
  								</div>
  							</div>
  						</div>
  					</form>
  					<hr>

  					<div id="financeiro-desktop" class="box-body">
  						<table class="table table-bordered table-striped">
  							<thead>
  								<tr>
  									<th class="columns01 col_default">#</th>
  									<th class="columns03 col_default">Cliente / Revendedor</th>
  									<th class="columns02 col_default">Forma de Pagamento</th>
  									<th class="columns05 col_default">Status</th>
  									<th class="columns05 col_default">Link</th>
  									<th class="columns05 col_default">Valor</th>
  									<th class="columns05 col_default">Data</th>
  								</tr>
  							</thead>
  							<tbody>

  								<?php foreach ($dados as $f) {
										$historico_saldo_data  = date(('d/m/Y'), strtotime($f->historico_saldo_data));
										$f->historico_saldo_status == 'CONCLUIDO' ? $label = 'success' : $label = 'warning'; 
										if (empty($f->comprovante_id)) {
											if (empty($f->mercadopago_id)) {
												$formpg = 'INDICACÃO';
											} else {
												$formpg = 'MERCADO PAGO';
											}
										} else {
											$formpg = 'COMPROVANTE';
										}
										?>

  									<tr>
  										<td class="text-left"><?php echo $f->historico_saldo_id; ?></td>
  										<td class="text-left"><?php echo $f->usuario_nome; ?></td>
  										<td class="text-left"><?php echo $formpg; ?></td>
  										<td class="text-left"><span class="badge badge-pill badge-<?php echo $label ?>"><?php echo ucfirst($f->historico_saldo_status) ?></span></td>
  										<?php if (isset($f->comprovante)) { ?>
  											<td class="text-center"><a href='<?php echo base_url(); ?>assets/arquivos/comprovantes/<?php echo $f->comprovante ?>' target='_blank'>Ver Comprovante</a></td>
  										<?php } else { ?>
  											<td></td>
  										<?php }  ?>
  										<td class="text-left"><?php echo str_replace('.', ',', $f->historico_saldo_valor); ?></td>
  										<td class="text-left"><?php echo $historico_saldo_data; ?></td>
  									</tr>

  								<?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div id="financeiro-mobile" class="box-body">
  						<table class="table table-bordered table-striped">
  							<!-- <thead>
  								<tr>
  									<th class="columns01 col_default">#</th>
  									<th class="columns03 col_default">Cliente / Revendedor</th>
  									<th class="columns02 col_default">Forma de Pagamento</th>
  									<th class="columns05 col_default">Status</th>
  									<th class="columns05 col_default">Valor</th>
  									<th class="columns05 col_default">Data</th>
  								</tr>
  							</thead> -->
  							<tbody>

  								<?php foreach ($dados as $f) {
										$historico_saldo_data  = date(('d/m/Y'), strtotime($f->historico_saldo_data));
										$f->historico_saldo_status == 'CONCLUIDO' ? $label = 'success' : $label = 'warning'; 
										if (empty($f->comprovante_id)) {
											if (empty($f->mercadopago_id)) {
												$formpg = 'INDICACÃO';
											} else {
												$formpg = 'MERCADO PAGO';
											}
										} else {
											$formpg = 'COMPROVANTE';
										}?>

  									<tr class="lista-financeiro">
  										<td class="text-left"><span>ID</span><?php echo $f->historico_saldo_id; ?></td>
  										<td class="text-left"><span>Cliente / Revendedor</span><span class="badge badge-pill badge-info"><?php echo $f->usuario_nome; ?></span></td>
  										<td class="text-left"><span>Forma de pagamento</span><?php echo $formpg; ?></td>
  										<td class="text-left"><span>Status</span><span class="badge badge-pill badge-<?php echo $label ?>"><?php echo ucfirst($f->historico_saldo_status) ?></span></td>
  										<?php if (isset($f->comprovante)) { ?>
  											<td class="text-center"><a href='<?php echo base_url(); ?>assets/arquivos/comprovantes/<?php echo $f->comprovante ?>' target='_blank'>Ver Comprovante</a></td>
  										<?php } else { ?>
  											<td></td>
  										<?php }  ?>
  										<td class="text-left"><span>Valor</span><span class="result-saldo">R$ <?php echo str_replace('.', ',', $f->historico_saldo_valor); ?></span></td>
  										<td class="text-left"><span>Data</span><?php echo $historico_saldo_data; ?></td>
  									</tr>

  								<?php }  ?>

  							</tbody>
  						</table>
  					</div>

  					<div class="card-footer clearfix">
  						<ul class="pagination pagination-sm m-0 float-right">
  							<?php
								if ($total_financeiro <> 0) {

									for ($p = 1, $i = 0; $i < $total_financeiro; $p++, $i += $itens_per_page) :
										if ($page == $p) :
											$tmp[] = "<li class='page-item active'><a class='page-link' href='javascript:;'>" . $p . "</a></li>";
										else :
											$tmp[] = "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/financeiro/" . $p . "/" . $status . "/" . $pagamento . "'>" . $p . "</a></li>";
										endif;
									endfor;

									for ($i = count($tmp) - 3; $i > 1; $i--) :
										if (abs($page - $i - 1) > 2) :
											unset($tmp[$i]);
										endif;
									endfor;

									if (count($tmp) > 1) :
										if ($page > 1) :
											echo "<li ><a class='page-link' href='" . base_url() . "relatorios/financeiro/" . ($page - 1) . "/" . $status . "/" . $pagamento . "'>«</a></li>";
										endif;

										$lastlink = 0;
										foreach ($tmp as $i => $link) :
											if ($i > $lastlink + 1) :
												echo "<li class='page-item'><a class='page-link' href='javascript:;'>...</a></li>";
											endif;
											echo $link;
											$lastlink = $i;
										endforeach;

										if ($page <= $lastlink) :
											echo "<li class='page-item'><a class='page-link' href='" . base_url() . "relatorios/financeiro/" . ($page + 1) . "/" . $status . "/" . $pagamento . "'>«</a></li>";
										endif;

									endif;
								} else {

									echo "Dados não foi encontrado!";
								}

								?>

  						</ul>
  					</div>

  				</div>

  			</div>


  		</div>
  		<!--/. container-fluid -->
  	</section>
  	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <style type="text/css" scoped>
  	#financeiro-mobile {
  		display: none;
  	}

  	@media (max-width: 600px) {

  		#financeiro-desktop {
  			display: none;
  		}

  		#financeiro-mobile {
  			display: block;
  		}

  		.lista-financeiro {
  			display: flex;
  			flex-direction: column;
  		}

  		.lista-financeiro span {
  			font-weight: bold;
  		}

  		.lista-financeiro .badge {
  			font-size: 12px;
  		}

  		.lista-financeiro .result-saldo {
  			font-weight: 500;
  		}

  		.lista-financeiro .text-left {
  			display: flex;
  			justify-content: space-between;
  			align-items: center;
  			font-size: 14px;
  		}
  	}
  </style>


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
  	<!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->