  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark" style="font-weight: 600;">Olá, bem-vindo de volta !</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Início</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-xl-6">
            <div class="small-box bg-cyan">
              <div class="inner">
                <p>Saldo da conta</p>
                <h3>R$ <?php echo $saldo ?></h3>
                <a href="<?php echo base_url(); ?>contas" class="btn btn-primary btn-recarga">Efetuar recarga</a>
              </div>
              <div class="inner">
                <p>Saldo de indicação</p>
                <h3>R$ <?php echo $bonus ?></h3>
                <a href="<?php echo base_url(); ?>indicacao" class="btn btn-primary btn-recarga">Resgatar</a>
              </div>
              <div class="icon">
                <img class="img-fluid icons-dashboard icon-saldo" src="<?php echo base_url(); ?>assets/dist/img/saldo.png">
              </div>
            </div>
            </a>
          </div>

          <div class="col-md-6 col-xl-6">
            <!--<a href="<?php echo base_url(); ?>ordem" class="small-box-footer">-->
            <div class="small-box bg-dark">
              <div class="inner">
                <p>Minhas ordens</p>
                <h3><?php echo $ordem ?></h3>
                <a href="<?php echo base_url(); ?>ordem" class="btn btn-primary btn-pedidos">Visualizar ordens</a>
              </div>
              <div class="icon">
                <img class="img-fluid icons-dashboard icon-pedido" src="<?php echo base_url(); ?>assets/dist/img/pedidos.png">
              </div>
            </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-xl-6">
            <div class="card card-pink">
              <div class="card-header" style="display: flex; align-items: center;">
                <h3 class="card-title">Informações</h3>
                <i class="fas fa-info-circle" style="position: absolute; right: 2%; font-size: 25px;"></i>
              </div>
              <div class="card-body p-0">
                <table class="table">
                  <tbody id="instatable">
                    <tr>
                      <td>Para utilizar os nossos serviços você precisa efetuar uma recarga no sistema.</td>
                    <tr>
                      <td>Passo 01 - Clicar no menu recarga</td>
                    </tr>
                    <tr>
                      <td>Passo 02 - Clicar em Efetuar recarga</td>
                    </tr>
                    <tr>
                      <td>Passo 03 - Escolher a melhor conta para a transferência bancária</td>
                    </tr>
                    <tr>
                      <td>Passo 04 - Enviar o comprovante</td>
                    </tr>
                    <tr>
                      <td>Atenção: O pagamento é compensado dentro de <strong>4 a 12 horas</strong>, podendo ser compensado antes.<br></td>
                    </tr>
                    <tr>
                      <td><strong>Valor mínimo de recarga R$ 20,00</strong><br></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>

          <div id="contato-dashboard" class="col-md-6 col-xl-6 d-flex justify-content-center align-items-center">
            <img class="img-fluid img-contato" src="<?php echo base_url(); ?>assets/dist/img/atendimento.png">

            <div>
              <h2 class="text-center mb-4">Dúvidas ?</h2>
              <a href="https://wa.me/5581981469522" target="_blank" class="btn btn-success btn-contato"><i aria-hidden="true" class="fab fa-whatsapp pr-2"></i>Entre em contato</a>
            </div>
          </div>
        </div>

      </div>
      <!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <style type="text/css" scoped>

    .card {
      border-radius: 10px;
    }

    .card-header {
      border-top-left-radius: 10px;
      border-top-right-radius: 10px;
    }

    .inner .btn-recarga,
    .inner .btn-pedidos {
      background-color: #b92786;
      border: none;
      border-radius: 5px;
      font-size: 1rem;
      margin: 10px 0;
    }

    .inner .btn-recarga:hover,
    .inner .btn-pedidos:hover {
      background-color: #a3265f;
    }

    .small-box {
      display: flex;
      justify-content: space-between;
      align-items: center;
      min-height: 168px;
      background-color: #59277a !important;
      text-align: left;
      border-radius: 10px;
    }

    #contato-dashboard .img-contato {
      width: 200px;
    }

    #contato-dashboard h2 {
      font-weight: bold;
    }

    #contato-dashboard a {
      color: #ffff;
      border-radius: 5px;
      font-size: 1.2rem;
      background: #00BE3D;
      font-weight: bold;
    }

    @media (max-width: 600px) {
      #contato-dashboard .img-contato {
        width: 150px;
      }

      #contato-dashboard a {
        font-size: 1rem;
      }
    }

    .icon-saldo {
      width: 165px;
      margin-right: 10px;
    }

    .icon-pedido {
      width: 165px;
    }

    @media (max-width: 576px) {

      .icon-saldo,
      .icon-pedido {
        width: 115px;
      }
    }
  </style>

  <!-- Modal -->
  <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <style type="text/css" scoped>
        .modal-content {
          background-image: url('<?php echo base_url(); ?>assets/dist/img/banner-recarga.jpeg');
          background-size: 100%;
          border-radius: 16px;
          background-repeat: no-repeat;
          height: 400px;
        }

        .close {
          color: #fff;
          font-size: 30px;
          float: right;
        }

        .modal-header {
          border-bottom: transparent;
        }

        @media (max-width: 870px) {
          .modal-content {
            height: 200px;
          }
        }

        @media (max-width: 570px) {
          .modal-content {
            height: 168px;
          }
        }

        @media (max-width: 414px) {
          .modal-content {
            height: 195px;
          }
        }

        @media (max-width: 411px) {
          .modal-content {
            height: 190px;
          }
        }

        @media (max-width: 384px) {
          .modal-content {
            height: 185px;
          }
        }

        @media (max-width: 375px) {
          .modal-content {
            height: 180px;
          }
        }

        @media (max-width: 360px) {
          .modal-content {
            height: 173px;
          }
        }

        @media (max-width: 320px) {
          .modal-content {
            height: 150px;
          }
        }
      </style>
      <a class="modal-content" href="<?php echo base_url(); ?>contas">
        <div>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="img"></div>
        </div>
      </a>
    </div>
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.pt-br.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/actionDashboard.js"></script>
  <script>
    $(document).ready(function() {
      $('.close').click(function(ev) {
        ev.preventDefault();
        $("#modal").hide();
        $(".window").hide();
      });
    });
  </script>

  <script type="text/javascript">
    var saldo = "<?php echo $saldo ?>";
  </script>

  <script>
    $(document).ready(function() {
        if (saldo == '0,00') {
          $('#modal').modal('show');
        }
    })
  </script>