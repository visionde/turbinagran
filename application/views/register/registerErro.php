<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TurbinaGran | Erro de Cadastro</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

    <!-- Custom style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/login-register.css">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.png" type="image/x-icon">
</head>

<body>
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup confirmation">

                <div class="confirmErro">
                    <h2 class="title"><?php echo $this->session->flashdata('erro'); ?></h2>
                    <i class="fas fa-times"></i>
                </div>

            </div>
        </div>

        <div class="panels-container">

            <div class="panel left-panel">
                <div class="content">
                    <h3>Vish ! Ocorreu um erro</h3>
                    <p>
                        Por favor, tente novamente.
                    </p>
                    <button class="btn transparent" onclick="location.href='<?php echo base_url(); ?>welcome'" type="button">Início</button>
                </div>
                <img src="<?php echo base_url(); ?>assets/dist/img/erro.png" class="image rocket" alt="" />
            </div>

        </div>
    </div>

</body>

</html>