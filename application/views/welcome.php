<!doctype html>
<html lang="pt-br">

<head>
	<title>Turbinagran - Turbinando as suas redes sociais</title>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">

	<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<!-- Custom CSS styles-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/home.css">

	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.png" type="image/x-icon">
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-185100700-1">
	</script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-185100700-1');
	</script>
</head>

<body id="page-top">

	<nav class="navbar navbar-expand-lg navbar-light fixed-top py-2" id="mainNav">
		<div class="container">
			<a class="navbar-brand js-scroll-trigger" href="#page-top"><img class="img-fluid logo" src="<?php echo base_url(); ?>assets/dist/img/logo-retangulo.png" alt="Logo do TurbinaGran"></a>

			<a id="btn-login-mobile" onclick="login()" href="#" class="btn btn-primary btn-login hvr-sweep-to-right">Entrar</a>

			<!--
			<button class="navbar-toggler navbar-toggler-right btn-mobile animated-button" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<div class="animated-icon"><span></span><span></span><span></span><span></span></div>
			</button>
			-->
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0 menu">
					<li class="nav-item"><a class="nav-link js-scroll-trigger" href="#masterhead">Início</a></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger" href="#how-works">Como funciona</a></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger" href="#our-services">Serviços</a></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger" href="#testimonials">Depoimentos</a></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contato</a></li>
					<a onclick="login()" href="#" class="btn btn-primary btn-login hvr-sweep-to-right">Entrar</a>
				</ul>
			</div>
		</div>
	</nav>

	<header id="masterhead" class="masterhead">
		<div class="container">
			<div class="row d-flex jutify-content-center align-items-center row-header">

				<div class="col-md-6 col-sm-6 d-flex justify-content-center align-items-center">
					<img class="img-fluid img-header" src="<?php echo base_url(); ?>assets/dist/img/social.png" alt="O TurbinaGran tem o seu foco no crescimento de redes sociais">
				</div>

				<div class="col-md-6 col-sm-6 content-header">
					<p class="text-muted">O melhor fornecedor de serviços para redes sociais do Brasil</p>
					<h1 class="title-header">Turbine suas redes sociais</h1>
					<div class="benefits-desktop">
						<p class="text-muted"><i class="far fa-check-circle mr-2"></i>Cadastro Gratuito</p>
						<p class="text-muted"><i class="far fa-check-circle mr-2"></i>100% Seguro </p>
						<p class="text-muted"><i class="far fa-check-circle mr-2"></i>Revenda os nossos serviços e ganhe 100% de lucro</p>
						<p class="text-muted"><i class="far fa-check-circle mr-2"></i>Serviços de alta qualidade</p>
						<p class="text-muted"><i class="far fa-check-circle mr-2"></i>Melhor suporte do mercado</p>
					</div>
					<a href="#how-works" class="btn btn-primary btn-header hvr-sweep-to-right">Saiba mais</a>
				</div>

				<div class="col-md-12 d-none d-lg-block">
					<div class="icon-scroll"></div>
				</div>

				<div class="pulse-icon">
					<div class="pulse1"></div>
					<div class="pulse2"></div>
					<div class="icon"><a href="https://wa.me/5581981469522" target="_blank"><img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/whatsapp-icon.png" alt="Whatsapp"></a></div>
				</div>

			</div>
		</div>
	</header>

	<section class="page-section" id="how-works">
		<div class="container">
			<div class="text-center">
				<h2 class="section-heading">Como Funciona</h2>
				<h3 class="section-subheading text-muted">Descubra como utilizar a nossa plataforma e desfrutar das suas
					funcionalidades</h3>
			</div>
			<ul class="timeline">
				<li>
					<div class="timeline-image"><img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/register.png" alt="Realização de cadastro na plataforma TurbinaGran" />
					</div>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="subheading">Cadastre-se</h4>
						</div>
						<div class="timeline-body">
							<p class="text-muted">Se você se perguntou: "Afff, cadastro ?", Não precisa se preocupar ! O cadastro é super simples e rápido.</p>
						</div>
					</div>
				</li>
				<li class="timeline-inverted">
					<div class="timeline-image"><img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/send-money.png" alt="Recarga de conta para contratar os serviços do TurbinaGran" /></div>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="subheading">Recarregue sua conta</h4>
						</div>
						<div class="timeline-body">
							<p class="text-muted">Com sua conta carregada, você poderá contratar os serviços que mais se encaixam com o seu perfil, com um preço que cabe no seu bolso, lembrando que quanto mais você recarregar sua conta, maior serão os serviços que você poderá solicitar.</p>
						</div>
					</div>
				</li>
				<li>
					<div class="timeline-image"><img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/rocket-launch.png" alt="Crescimento acelerado de mídias sociais" /></div>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="subheading">Aproveite os serviços</h4>
						</div>
						<div class="timeline-body">
							<p class="text-muted">Você poderá contratar serviços para turbinar o seu Instagram ou Facebook, de maneira rápida e descomplicada</p>
						</div>
					</div>
				</li>
				<li class="timeline-inverted">
					<a href="#">
						<div class="timeline-image last-timeline hvr-radial-out">
							<h4>
								Comece agora mesmo
							</h4>
						</div>
					</a>
				</li>
			</ul>
		</div>
	</section>

	<section id="benefits" class="benefits d-md-none d-lg-none">
		<div class="container-fluid">

			<div class="text-center">
				<h2 class="section-heading">Benefícios</h2>
				<h3 class="section-subheading text-muted">Conheça os benefícios de utilizar os nossos serviços.</h3>
			</div>

			<div class="row text-center">
				<div class="col-md-1"></div>
				<div class="col-md-2 mb-5">
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/benefits-check.png" alt="">
					<h4 class="text-muted">Cadastro gratuíto</h4>
				</div>
				<div class="col-md-2 mb-5">
					<img class="img-fluid pb-2" src="<?php echo base_url(); ?>assets/dist/img/benefits-security.png" alt="">
					<h4 class="text-muted">100% seguro</h4>
				</div>
				<div class="col-md-2 mb-5">
					<img class="img-fluid pb-2" src="<?php echo base_url(); ?>assets/dist/img/benefits-money.png" alt="">
					<h4 class="text-muted">Revenda os nossos serviços e ganhe 100% de lucro</h4>
				</div>
				<div class="col-md-2 mb-5">
					<img class="img-fluid pb-2" src="<?php echo base_url(); ?>assets/dist/img/benefits-services.png" alt="">
					<h4 class="text-muted">Serviços de alta qualidade</h4>
				</div>
				<div class="col-md-2">
					<img class="img-fluid pb-2" src="<?php echo base_url(); ?>assets/dist/img/benefits-support.png" alt="">
					<h4 class="text-muted">Melhor suporte do mercado</h4>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>

	<section id="our-services" class="our-services">
		<div class="container">

			<div class="text-center">
				<h2 class="section-heading">Serviços</h2>
				<h3 class="section-subheading text-muted">Conheça os nossos serviços</h3>
			</div>

			<div class="pricing-table">

				<div class="pricing-card">
					<h3 class="pricing-card-header">Instagram</h3>
					<ul>
						<i class="fab fa-instagram"></i>
						<li>Ganho de seguidores : <span><strong>R$ 0,05 </strong><small>/ Unidade</small></span></li>
						<li>Curtidas nas publicações: <span><strong>R$ 0,10 </strong><small>/ Unidade</small></span></li>
						<li>Visualizações de stories: <span><strong>R$ 0,05 </strong><small>/ Unidade</small></span></li>
						<li>Pedido mínimo : <strong>100 unidades</strong></li>
					</ul>
					<!--<div class="price"><sup>R$</sup>0,05<span>/unidade</span></div>-->
					<a onclick="login()" href="#" class="order-btn">Comprar</a>
				</div>

				<!--
				<div class="pricing-card">
					<h3 class="pricing-card-header">Facebook</h3>
					<ul>
						<i class="fab fa-facebook-f"></i>
						<li><strong>Curtidas</strong> nas postagens</li>
						<li><strong>50 GB</strong> SSD</li>
						<li><strong>1</strong> Domain Name</li>
						<li><strong>20</strong> Email</li>
					</ul>
					<div class="price"><sup>R$</sup>0,05<span>/unidade</span></div>
					<a href="#" class="order-btn">Comprar</a>
				</div>
				-->

			</div>

			<!-- 
			<div class="row d-flex justify-content-center align-items-center">
				<div class="col-md-6">
					<p class="lead">Nossos serviços</p>
					<h2 class="font-weight-bold">Instagram</h2>
					<p class="text-muted">Escolha o serviço que mais se adequa ao seu propósito.</p>
					<div class="accordion" id="accordionOne">

						<div class="card">
							<div class="card-header card-heading hvr-sweep-to-right" id="headingOne">
								<h5 class="mb-0">
									<button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Ganho de seguidores <i class="fas fa-plus px-2"></i>
									</button>
								</h5>
							</div>

							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionOne">
								<div class="card-body text-muted">
									R$ 0,05 por cada seguidor
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header card-heading hvr-sweep-to-right" id="headingTwo">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed text-white" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Curtidas em publicações <i class="fas fa-plus px-2"></i>
									</button>
								</h5>
							</div>

							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionOne">
								<div class="card-body text-muted">
									R$ 0,05 por curtida
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header card-heading hvr-sweep-to-right" id="headingThree">
								<h5 class="mb-0">
									<button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
										Visualização de stories <i class="fas fa-plus px-2"></i>
									</button>
								</h5>
							</div>

							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionOne">
								<div class="card-body text-muted">
									R$ 0,05 por cada visualização
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header card-heading hvr-sweep-to-right" id="headingFour">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed text-white" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
										Pedido mínimo<i class="fas fa-plus px-2"></i>
									</button>
								</h5>
							</div>

							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionOne">
								<div class="card-body text-muted">
									Para cada serviço listado acima, o pedido mínimo é de 100 unidades por serviço.
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="col-md-6 mr-auto order-first order-md-last">
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/social-networks.png" alt="">
				</div>
			</div>
			-->
		</div>

		<!-- 
		<div class="container">
			<div class="row d-flex justify-content-center align-items-center">
				<div class="col-md-6">
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/chatting.png" alt="">
				</div>

				<div class="col-md-6">
					<h2 class="font-weight-bold">Facebook</h2>
					<p class="text-muted">Escolha o plano que mais se adequa ao seu propósito.</p>
					<div class="accordion" id="accordionTwo">

						<div class="card">
							<div class="card-header card-heading hvr-sweep-to-right" id="headingThree">
								<h5 class="mb-0">
									<button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
										Curtidas nas páginas <i class="fas fa-plus px-2"></i>
									</button>
								</h5>
							</div>

							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionTwo">
								<div class="card-body text-muted">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry.
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header card-heading hvr-sweep-to-right" id="headingFour">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed text-white" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
										Curtidas nas páginas com estrangeiros <i class="fas fa-plus px-2"></i>
									</button>
								</h5>
							</div>

							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionTwo">
								<div class="card-body text-muted">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry.
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>
			-->
		</div>
	</section>

	<!-- Carousel  
	<div class="container">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">

                <div class="card text-center card-carousel">
                    <img class="card-img-top mx-auto rounded-circle img-fluid" src="<?php echo base_url(); ?>assets/dist/img/people.jpg" alt="Imagem de capa do card">
                    <div class="card-body">
                        <h3 class="card-title">Título do card</h3>
                        <p class="card-text"><i class="fas fa-quote-left mr-2"></i>Este é um card maior com suporte a texto embaixo, que funciona como
                            uma introdução a um conteúdo adicional. Este card tem o conteúdo ainda maior que o
                            primeiro, para mostrar a altura igual, em ação. <i class="fas fa-quote-right ml-2"></i></p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </div>
                </div>
            </div>

            <div class="carousel-item">
                <div class="card text-center card-carousel">
                    <img class="card-img-top mx-auto rounded-circle" src="<?php echo base_url(); ?>assets/dist/img/people2.jpg" alt="Imagem de capa do card">
                    <div class="card-body">
                        <h3 class="card-title">Título do card</h3>
                        <p class="card-text"><i class="fas fa-quote-left mr-2"></i>Este é um card maior com suporte a texto embaixo, que funciona como
                            uma introdução a um conteúdo adicional. Este card tem o conteúdo ainda maior que o
                            primeiro, para mostrar a altura igual, em ação. <i class="fas fa-quote-right ml-2"></i></p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                    </div>
                </div>
            </div>

            <div class="carousel-item">
                <div class="card text-center card-carousel">
                    <img class="card-img-top mx-auto rounded-circle" src="<?php echo base_url(); ?>assets/dist/img/people3.jpg" alt="Imagem de capa do card">
                    <div class="card-body">
                        <h3 class="card-title">Título do card</h3>
                        <p class="card-text"><i class="fas fa-quote-left mr-2"></i>Este é um card maior com suporte a texto embaixo, que funciona como
                            uma introdução a um conteúdo adicional. Este card tem o conteúdo ainda maior que o
                            primeiro, para mostrar a altura igual, em ação. <i class="fas fa-quote-right ml-2"></i></p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Próximo</span>
        </a>
    </div>

</div>
	-->

	<section id="testimonials" class="testimonials">

		<div class="container">
			<div class="text-center">
				<h2 class="section-heading">Depoimentos</h2>
				<h3 class="section-subheading text-muted">Saiba o que os nossos clientes estão falando sobre a nossa plataforma.</h3>
			</div>

			<!-- Swiper -->
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="card text-center card-carousel">
							<!--<img class="card-img-top mx-auto rounded-circle img-fluid" src="<?php echo base_url(); ?>assets/dist/img/people.jpg" alt="Imagem de capa do card">-->
							<div class="card-body">
								<h3 class="card-title">Luka Cardoso</h3>
								<p class="card-text"><i class="fas fa-quote-left mr-2"></i>Ótimo marketing, vcs estão de parabéns!<i class="fas fa-quote-right ml-2"></i></p>
								<div class="ratings">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
								</div>
							</div>
						</div>
					</div>

					<div class="swiper-slide">
						<div class="card text-center card-carousel">
							<!--<img class="card-img-top mx-auto rounded-circle img-fluid" src="<?php echo base_url(); ?>assets/dist/img/people.jpg" alt="Imagem de capa do card">-->
							<div class="card-body">
								<h3 class="card-title">Erick Fontes</h3>
								<p class="card-text"><i class="fas fa-quote-left mr-2"></i>Vcs estão de parabéns, trabalho de vcs é muito sério e vale a pena.<i class="fas fa-quote-right ml-2"></i></p>
								<div class="ratings">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
								</div>
							</div>
						</div>
					</div>

					<div class="swiper-slide">
						<div class="card text-center card-carousel">
							<!--<img class="card-img-top mx-auto rounded-circle img-fluid" src="<?php echo base_url(); ?>assets/dist/img/people.jpg" alt="Imagem de capa do card">-->
							<div class="card-body">
								<h3 class="card-title">Exolclésia Esmelquina</h3>
								<p class="card-text"><i class="fas fa-quote-left mr-2"></i>Estão de parabéns!!!!!
									Trabalho sério e perfeito pra quem quer turbinar suas redes sociais.. 😁 passa toda confiança pro cliente e sem contar que tem o melhor atendimento vcs são 1.000000 parabéns, super indico.vale a pena é segurança total e agilidade.. grande bjo!!! <i class="fas fa-quote-right ml-2"></i></p>
								<div class="ratings">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
								</div>
							</div>
						</div>
					</div>

					<div class="swiper-slide">
						<div class="card text-center card-carousel">
							<!--<img class="card-img-top mx-auto rounded-circle img-fluid" src="<?php echo base_url(); ?>assets/dist/img/people.jpg" alt="Imagem de capa do card">-->
							<div class="card-body">
								<h3 class="card-title">Walmir Soares</h3>
								<p class="card-text"><i class="fas fa-quote-left mr-2"></i>Muito bom e muito rápido
									<i class="fas fa-quote-right ml-2"></i>
								</p>
								<div class="ratings">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- Add Pagination -->
				<div class="swiper-pagination"></div>
			</div>
		</div>

	</section>

	<section id="contact">
		<div class="container-contact">
			<div class="form">
				<div class="contact-info">
					<h3 class="title">Vamos conversar ?</h3>
					<p class="text">
						Tem algum problema, dúvidas ou sugestões ? Entre em contato com o nosso suporte.
					</p>
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/dist/img/uniting-the-world.png" alt="Pessoas conectadas através das mídias sociais">

				</div>

				<div class="contact-form">
					<span class="circle one"></span>
					<span class="circle two"></span>

					<form method="post" action="<?php echo base_url(); ?>contato/contatoWelcome" autocomplete="off">
						<h3 class="title">Fale conosco</h3>
						<div class="input-container">
							<input type="text" name="name" id="name" class="input" required="required" />
							<label for="">Nome</label>
							<span>Nome</span>
						</div>
						<div class="input-container">
							<input type="email" name="email" id="email" class="input" required="required" />
							<label for="">E-mail</label>
							<span>E-mail</span>
						</div>
						<div class="input-container">
							<input type="tel" name="tel" id="tel" class="input" />
							<label for="">Telefone</label>
							<span>Telefone</span>
						</div>
						<div class="input-container textarea">
							<textarea name="message" class="input" id="message" maxlength="150"></textarea>
							<label for="">Mensagem</label>
							<span>Mensagem</span>
						</div>
						<input type="submit" value="Enviar" class="btn-contact" />
					</form>
				</div>
			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="container">
			<div class="row d-flex justify-content-center align-items-start align-content-center">
				<div class="col-md-3 d-flex flex-column footer-item">
					<h3>Links úteis</h3>
					<span><i class="fas fa-user-shield pr-2 icons-s"></i><a href="#">Políticas de privacidade</span></a>
					<span><i class="far fa-file-alt pr-2 icons-s"></i><a href="#">Termos de serviço</span></a>
				</div>
				<div class="col-md-3 d-flex flex-column footer-item">
					<h3>Sobre nós</h3>
					<span>Extremamente motivados em oferecer os melhores serviços para os nossos clientes.</span>
				</div>
				<div class="col-md-3 d-flex flex-column footer-item">
					<h3>Contato</h3>
					<!--<span><i class="fas fa-map-marked-alt pr-2 icons-s"></i>Av. conde da boa vista</span>-->
					<span><i class="far fa-envelope pr-2 icons-s"></i><a href="#">turbinagran@gmail.com</a></span>
					<span><i class="fab fa-whatsapp pr-2 icons-s"></i><a href="https://wa.me/5581981469522" target="_blank">+55 (81) 8146-9522</a></span>
				</div>
				<div class="col-md-3 footer-icons">
					<h3>Social</h3>
					<div class="social-icons">
						<!--
						<div class="icons">
							<a href="#"><i class="fab fa-instagram fa-2x"></i></a>
						</div>
						-->
						<div class="icons">
							<a href="https://www.facebook.com/turbinagran" target="_blank"><i class="fab fa-facebook-f fa-2x"></i></a>
						</div>
						<div class="icons">
							<a href="https://www.instagram.com/turbinagran" target="_blank"><i class="fab fa-instagram fa-2x"></i></a>
						</div>
						<!--
						<div class="icons">
							<a href="#"><i class="fab fa-twitter fa-2x"></i></a>
						</div>
						-->
					</div>
				</div>
				<div class="col-md-12 pt-5 text-center">
					<span class="copyright">Copyright &copy; 2020 TurbinaGran - Todos os direitos reservados.</span>
				</div>
			</div>
		</div>
	</footer>

	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>assets/dist/js/actionRegister.js"></script>

	<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

	<!-- Initialize Swiper -->
	<script>
		var swiper = new Swiper('.swiper-container', {
			grabCursor: true,
			centeredSlides: true,
			slidesPerView: 1,
			grabCursor: true,
			autoplay: {
				delay: 3500,
				disableOnInteraction: false,
			},
			coverflowEffect: {
				rotate: 0,
				stretch: 0,
				depth: 0,
				modifier: 1,
				slideShadows: true,
			},
			pagination: {
				el: '.swiper-pagination',
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			loop: true
		});
	</script>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
	</script>

	<script src="<?php echo base_url(); ?>assets/dist/js/home.js"></script>
	<script src="<?php echo base_url(); ?>assets/dist/js/contact.js"></script>

	<script type="text/javascript">
		var base_url = "<?php echo base_url(); ?>";
	</script>

</body>

</html>